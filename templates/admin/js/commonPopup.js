/**
 * 공통 팝업 function
 */

/*
 * 회원검색 공통 팝업
 * 사용법 : customerSearchPopup();
 * 리턴값 : Object
 *     obj.custId : 고객번호
 *     obj.memberId : 회원아이디     
 *     obj.custName : 고객명
 * 부모창 함수 : opener.fnCustomerChoice(obj);
 */
function customerSearchPopup(singleYn){
	// 단일선택여부 (Y:단일선택 / N:다중선택)
	if (singleYn == "" || typeof singleYn == "undefined")
	{   
		singleYn ="N";    
    }
	var popupObj = new Object();
    popupObj.url = "/denied/member/customer/customerMgtListPopup.do?singleYn="+singleYn;
    popupObj.popupName = "customerSearchPopup"; 
    popupObj.width = "1000";
    popupObj.height = "600";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);	
}

/*
 * 업체 공통 팝업
 * 사용법 : vendorSearchPopup();
 * 리턴값 : Object
 *       obj.vendorSeq : 업체seq
 *       obj.vendorName : 업체명
 * 부모창 함수 : opener.fnVendorChoice(obj);
 */
function vendorSearchPopup(singleYn){
	// 단일선택여부 (Y:단일선택 / N:다중선택)
	if (singleYn == "" || typeof singleYn == "undefined")
	{   
		singleYn ="N";    
    }
	var popupObj = new Object();
    popupObj.url = "/denied/vendor/vendorMgtListPopup.do?singleYn="+singleYn;
    popupObj.popupName = "vendorSearchPopup"; 
    popupObj.width = "550";
    popupObj.height = "500";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);	
}

/*
 * 브랜드검색 공통 팝업
 * 사용법 : brandSearchPopup();
 * 리턴값 : Object
 *     obj.brandSeq : 브랜드코드
 *     obj.brandName : 브랜드명
 * 부모창 함수 : opener.fnbrandChoice(obj);
 */
function brandSearchPopup(singleYn){
	// 단일선택여부 (Y:단일선택 / N:다중선택)
	if (singleYn == "" || typeof singleYn == "undefined")
	{   
		singleYn ="N";    
    }
	var popupObj = new Object();
    popupObj.url = "/denied/vendor/brand/brandMgtListPopup.do?singleYn="+singleYn;
    popupObj.popupName = "brandSearchPopup"; 
    popupObj.width = "600";
    popupObj.height = "600";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);	
}

/*
 * 회원엑셀업로드 공통 팝업
 * 리턴값 : html
 * 부모창 함수 : opener.fnExcelCustomer(obj);
*/
function customerExcelUploadPopup(){
	var popupObj = new Object();
    popupObj.url = "/denied/member/customer/customerExcelUploadPopup.do";
    popupObj.popupName = "brandSearchPopup"; 
    popupObj.width = "500px";
    popupObj.height = "250px";
    popupObj.scrollbars = "yes";    
    customPopup(popupObj);	
}

/*
 * 상품검색 공통 팝업
 * 사용법 : itemSearchPopup();
 * 리턴값 : Object
 *     obj.storeitemId : 점포상품ID
 *     obj.itemId : 상품ID
 *     obj.itemName : 상품명
 *     obj.sellingPrice : 상품가격
 *     obj.specialPrice : 특판상품가격
 *     obj.progressStatusCode : 판매상태
 *     obj.deviceCode : 판매채널
 * 부모창 함수 : opener.fnItemChoice(obj);
 */
function itemSearchPopup(singleYn, searchStoreId){
    // 단일선택여부 (Y:단일선택 / N:다중선택)
    if (singleYn == "" || typeof singleYn == "undefined")
    {   
        singleYn ="N";    
    }
    
    var si = "";
    if(searchStoreId != "" && typeof searchStoreId != "undefined"){
        si = "&searchStoreId="+searchStoreId;
    }
    
    var popupObj = new Object();
    popupObj.url = "/denied/product/storeItem/storeItemMgtListPopup.do?singleYn="+singleYn+si;
    popupObj.popupName = "storeItemSearchPopup"; 
    popupObj.width = "1100";
    popupObj.height = "900";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);  
}

/*
 * 상품상세 공통 팝업
 * 사용법 : itemDetailPopup();
 * 리턴값 : Object
 */
function itemDetailPopup(storeItemId){
    if (storeItemId != "" && typeof storeItemId != "undefined")
    {   
        var popupObj = new Object();
        popupObj.url = "/denied/product/storeItem/storeItemMgtInfoPopup.do?STOREITEM_ID="+storeItemId;
        popupObj.popupName = "storeItemInfoPopup"; 
        popupObj.width = "1100";
        popupObj.height = "900";
        popupObj.scrollbars = "yes";
        
        customPopup(popupObj);
    }else{
        alert("상품아이디를 찾을 수 없습니다.");
    }
}

/*
 * 상품히스토리 팝업
 * 사용법 : itemHisPopup();
 * 리턴값 : Object
 */
function itemHisPopup(itemId){
    if (itemId != "" && typeof itemId != "undefined")
    {
        var popupObj = new Object();
        popupObj.url = "/denied/product/item/itemHisListPopup.do?ITEM_ID="+itemId;
        popupObj.popupName = "itemHisPopup"; 
        popupObj.width = "400";
        popupObj.height = "500";
        popupObj.scrollbars = "yes";
        
        customPopup(popupObj);
    }else{
        alert("상품아이디를 찾을 수 없습니다.");
    }
}

/*
 * 지점상품히스토리 팝업
 * 사용법 : storeItemHisPopup();
 * 리턴값 : Object
 */
function storeItemHisPopup(storeItemId){
    if (storeItemId != "" && typeof storeItemId != "undefined")
    {
        var popupObj = new Object();
        popupObj.url = "/denied/product/storeItem/storeItemHisListPopup.do?STOREITEM_ID="+storeItemId;
        popupObj.popupName = "storeItemHisPopup"; 
        popupObj.width = "400";
        popupObj.height = "500";
        popupObj.scrollbars = "yes";
        
        customPopup(popupObj);
    }else{
        alert("지점상품아이디를 찾을 수 없습니다.");
    }
}

/*
 * 상품 엑셀 일괄등록 공통 팝업
 * 사용법 : itemUploadFormPopup();
 */
function itemUploadFormPopup(){
    var popupObj = new Object();
    popupObj.url = "/product/item/itemUploadFormPopup.do";
    popupObj.popupName = "itemUploadFormPopup"; 
    popupObj.width = "500";
    popupObj.height = "250";
    popupObj.scrollbars = "no";
    
    customPopup(popupObj);
}

/*
 * 상품 정보제공고시 엑셀 일괄등록 공통 팝업
 * 사용법 : itemGoodsNotifyUploadFormPopup();
 */
function itemGoodsNotifyUploadFormPopup(){
    var popupObj = new Object();
    popupObj.url = "/product/item/itemGoodsNotifyUploadFormPopup.do";
    popupObj.popupName = "itemGoodsNotifyUploadFormPopup"; 
    popupObj.width = "500";
    popupObj.height = "250";
    popupObj.scrollbars = "no";
    
    customPopup(popupObj);
}

/*
 * 지점상품 엑셀 일괄등록 공통 팝업
 * 사용법 : storeItemUploadFormPopup();
 */
function storeItemUploadFormPopup(){
    var popupObj = new Object();
    popupObj.url = "/product/storeItem/storeItemUploadFormPopup.do";
    popupObj.popupName = "storeItemUploadFormPopup"; 
    popupObj.width = "500";
    popupObj.height = "250";
    popupObj.scrollbars = "no";
    
    customPopup(popupObj);
}

/*
 * 우편번호검색 공통 팝업
 * 사용법 : zipCodeSearchPopup();
 * 리턴값 : Object
 * 부모창 함수 : fnZipCodeChoice(obj);
 */
function zipCodeSearchPopup(){
	var popupObj = new Object();
    popupObj.url = "/denied/system/zipCode/zipCodeMgtLotListPopup.do";
    popupObj.popupName = "zipCodeSearchPopup"; 
    popupObj.width = "600";
    popupObj.height = "800";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);	
}

/*
 * 카드사 목록 팝업
 * 사용법 : cardCompListPopup();
 * 리턴값 : Object
 * 부모창 함수 : fnCardChoice(obj);
 */
function cardCompListPopup(kind){
	// 카드구분 ( A:전체 / G:자사+제휴카드)
    if (kind == "" || typeof kind == "undefined") {   
    	kind ="A";    
    }
    var popupObj = new Object();
    popupObj.url = "/denied/payment/card/cardCompListPopup.do?searchKind="+kind;
    popupObj.popupName = "cardCompListPopup"; 
    popupObj.width = "450";
    popupObj.height = "600";
    popupObj.scrollbars = "yes";    
    
    customPopup(popupObj);   
}

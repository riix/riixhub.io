(function($) {
	$.fn.extend(jQuery, {
		/**
		 * check 박스 전체 선택 해제 
		 * 사용법 : $.checkBoxSelect("allSelect", "select") 전체선택 클래스, 선택 클래스 
		 */
		checkBoxSelect : function(allSelect, select){
			//전체선택  이벤트 호출
			$(document).on("click", "."+allSelect, function (){
				$.checkBoxAllSelect(allSelect, select);
			});
			
			//개별선택 이벤트 호출
			$(document).on("click", "."+select, function (){
				$.checkBoxUnitSelect(allSelect, select);
			});
			
			//로딩
			if($("."+allSelect).is(":checked")){
				$.checkBoxAllSelect(allSelect, select);
			}else{
				$.checkBoxUnitSelect(allSelect, select);
			}
		},
		
		//전체 선택
		checkBoxAllSelect : function(allSelect, select){
			var totalCount = $("."+select).length; 
			if( totalCount <= 0 ){
				$("."+allSelect).prop("checked", false);
				return; 
			}
			if($("."+allSelect).is(":checked")){
				$("."+select).prop("checked", true);
			}else{
				$("."+select).prop("checked", false);
			}			
		},
		
		//개별 선택
		checkBoxUnitSelect : function(allSelect, select){
			var checkdLength = $("."+select + ":checked").length;
			var totalCount = $("."+select).length; 
			
			//선택된 checkbox 확인
			if(checkdLength > 0 && checkdLength == totalCount){
				$("."+allSelect).prop("checked", true);
			}else{
				$("."+allSelect).prop("checked", false);
			}
		}
	});
	
	$.fn.rowspan = function(colIdx, isStats) {
	    return this.each(function(){
	        var that;
	        $('tr', this).each(function(row) {
	            $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
	                if ($(this).html() == $(that).html() && (!isStats|| isStats && $(this).prev().html() == $(that).prev().html())) {
	                    rowspan = $(that).attr("rowspan") || 1;
	                    rowspan = Number(rowspan)+1;
	                    $(that).attr("rowspan",rowspan);
	                    // do your action for the colspan cell here
	                    $(this).hide();
	                    //$(this).remove();
	                    // do your action for the old cell here
                    } else {
                        that = this;
                    }
	                // set the that if not already set
	                that = (that == null) ? this : that;
                });
            });
        });
	};
	
	$.fn.dataRowspan = function(el, colIdx) {
        return this.each(function(){
            var that, elVal;
            $('tr', this).each(function(row) {
                $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                    if ($(this).parent().find("td > input[name="+el+"]").val() == elVal) {
                        rowspan = $(that).attr("rowspan") || 1;
                        rowspan = Number(rowspan)+1;
                        $(that).attr("rowspan",rowspan);
                        // do your action for the colspan cell here
                        $(this).hide();
                        //$(this).remove();
                        // do your action for the old cell here
                    } else {
                        that = this;
                        elVal = $(this).parent().find("td > input[name="+el+"]").val();
                    }
                    // set the that if not already set
                    that = (that == null) ? this : that;
                    elVal = (elVal == null) ? $(this).parent().find("td > input[name="+el+"]").val(): elVal;
                    console.log(elVal);
                });
            });
        });
    };
	
	$.fn.colspan = function(rowIdx) {
	    return this.each(function() {
	        var that;
	        $('tr', this).filter(":eq("+rowIdx+")").each(function(row) {
	            $(this).find('th').filter(':visible').each(function(col) {
	                if($(this).html() == $(that).html()) {
    	                colspan= $(that).attr("colSpan") || 1;
    	                colspan = Number(colspan)+1;
    	                $(that).attr("colSpan", colspan);
    	                $(this).hide();
	                }else {
	                    that = this;
	                }
	                that = (that == null) ? this : that;
	            });
	        });
	    });
	};
	
})(jQuery);

var $Common = {
    
    changeSelectToSelect: function(param) {
        param = $.extend({targetSelect: '', handleSelect: []}, param);
        
        $(param.targetSelect).change(function() {
            var thisVal = $(this).val();
            $.each(param.handleSelect, function(k, $param) {
                $param = $.extend({targetSelect: '', link: '', prefix: '', data: {searchStore: thisVal}, isAllSelected: 0}, $param);
                //console.log($param);
                
                $.postSyncJsonAjax($param.link, $param.data ,function(data) {
                    //console.log(data);
                    var $obj = $($param.targetSelect);
                    if(data.list) {
                        $obj.find("option").remove();
                    }
                    if($param.isAllSelected == 0 || $param.isAllSelected == 1) {
                        $obj.append('<option value="">'+($param.isAllSelected == 0? '전체':'선택')+'</option>');
                    }
                    $.each(data.list, function(i, obj) {
                        //console.log(eval('obj.'+$param.prefix+'_ID'));
                        $obj.append('<option value="'+eval('obj.'+$param.prefix+'_ID')+'">'+eval('obj.'+$param.prefix+'_NAME')+'</option>');
                    });
                });
            });
            
        });
    }
    
};

function writeCookie( cookie_name, cookie_value, days ){ 
	var expires = "";	
    var date = new Date();

    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = ";expires=" + date.toGMTString();
    
    document.cookie = cookie_name + "=" + escape( cookie_value ) + expires + ";path=/; ";
} 

function deleteCookie( cookie_name )
{
    writeCookie(cookie_name, "", -1);
}

function readCookie(name) {
    var cname = name + '=';
    var dc    = document.cookie;
    if (dc.length > 0 ) {
        begin = dc.indexOf(cname);
        if (begin != -1) {
            begin = begin + cname.length;
            end   = dc.indexOf(';', begin);

            if (end == -1) end = dc.length;
            return unescape(dc.substring(begin,end));
        }
    }
    return null;
}

/*
 * 숫자 체크
 */
function isNumberOnly(str)
{
    var numberValid = /^[0-9]+$/;
    var blnNumberCheck = true;
   
    for(var i=0; i<str.length; i++)
    {
        if(!numberValid.test(str.charAt(i)))
        {
        	blnNumberCheck = false;
        	return false;
        } 	
    }
    
    return blnNumberCheck;
}

/*
 * 커스텀 팝업 url, popupName, width, height
 * 사용법
 * var obj = new Object;
 *     obj.url = "";         팝업주소
 *     obj.popupName = "";   팝업창 이름
 *     obj.width = "";       팝업창 가로사이즈
 *     obj.height = "";      팝업창 세로사이즈
 *     obj.scrollbars = "";  팝업창 스크롤 여부 yes/no     
 */
function customPopup(object)
{
	return window.open(object.url, object.popupName, "width=" + object.width + ", height=" + object.height + ", scrollbars=" + object.scrollbars + ", toolbar=no, menubar=no, resizable=no");
}

/* 비밀번호 유효성 검사 
 * 사용법  pwdValidation(str) > str : 유효성 체크할 비밀번호
 * return : Object.flag : true, false(성공, 실패)
 * return : Object.msg : string(실패 사유)
 * 유형 : 영소문자, 영대문자, 특수문자
 * 체크 1 : 2가지 이상 조합시 10자 이상  
 * 체크 2 : 1가지 사용시 12자 이상
 * 체크 3 : 10자 이상 20자 미만
 */
function pwdValidation(str){
    var resultObj = new Object();
    var idxStr = "";
    var validation_pw1 = /^.*(?=.{12,19})(?=.*[a-z]).*$/;
    var validation_pw2 = /^.*(?=.{12,19})(?=.*[A-Z]).*$/;
    var validation_pw3 = /^.*(?=.{12,19})(?=.*[!@#$%^&*()-+]).*$/;
    var validation_pw4 = /^.*(?=.*[a-z]).*$/;
    var validation_pw5 = /^.*(?=.*[A-Z]).*$/;
    var validation_pw6 = /^.*(?=.*[!@#$%^&*()-+]).*$/; 
    
    resultObj.msg = "법규개정에 따른 비밀번호는 영문자, 특수문자, 대문자 중\n2개 이상 혼합 시 10자, 1개로는 12자이상 20자미만 사용 가능합니다";
    resultObj.flag = true;
	
    if(str.length > 19 || str.length < 10)
    {
        resultObj.flag = false;
    }
     
    if(resultObj.flag)
    {
        for(var i=0; i<str.length; i++)
        {
        	idxStr = str.charAt(i);
        	
        	if(resultObj.flag)
        	{
            	resultObj.flag = false;
        		
                if(validation_pw4.test(idxStr) && resultObj.flag == false)
                {
                    resultObj.flag = true;
                }
            	
                if(validation_pw5.test(idxStr) && resultObj.flag == false)
                {
                    resultObj.flag = true;        	
                }
            	
                if(validation_pw6.test(idxStr) && resultObj.flag == false)
                {
                    resultObj.flag = true;
                }        	
        	}
        } 
        
    	if(resultObj.flag)        
        {
            if(!validation_pw1.test(str))
            {
                resultObj.flag = false;
            }
            
            if(!resultObj.flag)
            {
                if(!validation_pw2.test(str))
                {
                	resultObj.flag = false;
                }
                else
                {
                	resultObj.flag = true;	
                }
            }        

            if(!resultObj.flag)
            {
                if(!validation_pw3.test(str))
                {
                	resultObj.flag = false;
                }
                else
                {
                	resultObj.flag = true;	
                }
            }        
            
            if(!resultObj.flag && str.length > 9)
            {
                if(validation_pw4.test(str) == true && validation_pw5.test(str) == true)
                {
                	resultObj.flag = true;
                }

                if(validation_pw4.test(str) == true && validation_pw6.test(str) == true)
                {
                	resultObj.flag = true;
                }            
                
                if(validation_pw5.test(str) == true && validation_pw6.test(str) == true)
                {
                	resultObj.flag = true;
                }            
            }        
            
            if(resultObj.flag)
            {
                if(/(\w)\1\1/.test(str))
                {
                	resultObj.flag = false;
                	resultObj.msg = "비밀번호에 연속된 문자가 3개이상 올 수 없습니다.";        	
                }
                else
                {
                	resultObj.msg = "";
                }
            }            
        }
    }
    
	return resultObj;
}

/*
* ClassName : stringUtil
* Description : 문자열 관련 Class
*/
var stringUtil = {
    /* 문자열확인 후 문자열 또는 기본값 리턴
    str:체크 문자열 def:기본값 */
    getString: function(str, def){
        if (str != undefined && str && str != "" && str != "null"){
            return $.trim(str);
        } else {
            return $.trim(def);
        }
    },
    /* 정수형 확인 후 정수형 또는 기본값 리턴
    num:체크 정수형 / def:기본값 */
    getInt: function(num, def){
        var val = parseInt(num, 10);

        if (isNaN(val)){
            return def;
        } else {
            return val;
        }
    },
    /* 공백제거
    str: 공백 제거 할 문자열*/
    trim: function(str){
        return $.trim(str);
    },
    /* Date
    */
    getDateView: function(regdt){

        var yyyy = regdt.substring(0, 4);
        var MM = regdt.substring(4, 6)-1;
        var dd = regdt.substring(6, 8);
        var hh = regdt.substring(8, 10);
        var mm = regdt.substring(10, 12);
        var ss = regdt.substring(12, 14);

        var nowDate = new Date();
        var regDate = new Date(yyyy, MM, dd, hh, mm, ss);

        var ss = Math.floor(nowDate.getTime() - regDate.getTime() ) / 1000;
        var mm = Math.floor(ss / 60);
        var hh = Math.floor(mm / 60);
        var day = Math.floor(hh / 24);

        var diff_hour = Math.floor(hh % 24);
        var diff_minute = Math.floor(mm % 60);
        var diff_second = Math.floor(ss % 60);

        //console.log( regdt + ' 계산 시간   : ' + day +  '일 ' + diff_hour  + ' 시간 ' + diff_minute + ' 분 ' + diff_second  + ' 초 ');
        var returnDate = "";
        if (day > 1 || diff_hour > 1){
            returnDate = regDate.format("yyyy.MM.dd HH:mm");
        } else {
            returnDate = diff_minute + "분 전";
        }

        return returnDate;
    },
    /* Format Date
    */
    formatDate: function(regdt, f){
        var yyyy = regdt.substring(0, 4);
        var yy = regdt.substring(2, 4);
        var MM = regdt.substring(4, 6);
        var dd = regdt.substring(6, 8);
        var hh = regdt.substring(8, 10);
        var mm = regdt.substring(10, 12);
        var ss = regdt.substring(12, 14);

        return f.replace(/(yyyy|yy|MM|dd|hh|mm|ss)/gi, function($1) {
            switch ($1) {
                case "yyyy": return yyyy;
                case "yy": return yy.zf(2);
                case "MM": return MM.zf(2);
                case "dd": return dd.zf(2);
                case "hh": return hh.zf(2);
                case "mm": return mm.zf(2);
                case "ss": return ss.zf(2);
                default: return $1;
            }
        });
    },
    /* set Comma */
    setComma: function(num){
        var pattern = /(^[+-]?\d+)(\d{3})/;
        num += '';
        while (pattern.test(num)){
            num = num.replace(pattern, '$1' + ',' + '$2');
        }
        return num;
    },
    /* remove Comma */
    removeComma: function(num){
        return num.replace(/,/gi,"");
    }

}

//날짜 셋팅 - 오늘, 일주일, 15일, 한달, 두달
function fnDateSet(v_start_name, v_end_name, s_year, s_month, s_day, e_year, e_month, e_day, seperator){
    var minDate = getCalculatedDate(s_year, s_month, s_day, seperator);
    var maxDate = getCalculatedDate(e_year, e_month, e_day, seperator);
    var minObj = $("#"+v_start_name	);
    var maxObj = $("#"+v_end_name	);
    minObj.attr({'data-max': maxDate }).val(minDate);
    maxObj.attr({'data-min': minDate }).val(maxDate);
    
}
function getCalculatedDate(iYear, iMonth, iDay, seperator){
    //현재 날짜 객체를 얻어옴.
    var gdCurDate = new Date();

    //현재 날짜에 날짜 게산.
    gdCurDate.setYear ( gdCurDate.getFullYear() + iYear );
    gdCurDate.setMonth( gdCurDate.getMonth() 	+ iMonth);
    gdCurDate.setDate ( gdCurDate.getDate() 	+ iDay 	);

    //실제 사용할 연, 월, 일 변수 받기.
    var giYear  = gdCurDate.getFullYear();
    var giMonth = gdCurDate.getMonth()+1;
    var giDay   = gdCurDate.getDate();

    //월, 일의 자릿수를 2자리로 맞춘다.
    giMonth = "0" + giMonth;
    giMonth = giMonth.substring(giMonth.length-2,giMonth.length);
    giDay   = "0" + giDay;
    giDay   = giDay.substring(giDay.length-2,giDay.length);

    //display 형태 맞추기.
    return giYear + seperator + giMonth + seperator +  giDay;
}

/**
 * 첨부파일 이미지 확장자 체크
 */
(function($) {
	$.fn.extend(jQuery, {
		attachTypeImage : function(attachName){
			var blnAttachTypeImage = true;
			
			if(attachName != "")
		    {
				var fileType = attachName.substring(attachName.length - 3);
				fileType = fileType.toLowerCase();
				
				if(fileType != "png" && fileType != "jpg")
				{
					blnAttachTypeImage = false;					
				}			
			}
			
			return blnAttachTypeImage;
		}
	});	
})(jQuery);

/**
 * 뒤로가기 새로고침 방지
 * 
$(document).bind("keydown",function(e){
    if( (event.ctrlKey == true && (event.keyCode == 78 || event.keyCode == 82)) || (event.keyCode == 116) || (e.keyCode==8) ){
         event.keyCode = 0;
         event.cancelBubble = true;
         event.returnValue =false;
    }
});
*/

// 달력 input 에 YYYY-MM-DD 표시
$(function(){
    var dt = new Date();
    
    var y = dt.getFullYear();
    var m = (dt.getMonth() + 1);
    var d = dt.getDate();
    
    if(m < 10){
        m = "0"+m;
    }
    if(d < 10){
        d = "0"+d;
    }

	$(".date").attr("placeholder", y + "-" + m + "-" + d);
});
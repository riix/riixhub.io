$(function() {

    /* title */
    $('span.icon em').eachQ(function() {
        var $this = $(this);
        $this.parent().attr('title', $this.text());
    });

    /* focus form element */
    $('input').on({
        "focusin": function(e) {
            $(this).addClass('focus');
        },
        "focusout": function(e) {
            $(this).removeClass('focus');
        }
    });

    /* close */
    $(document).on('click', '.js-close', function() {
        window.close();
    });

    /* print */
    $(document).on('click', '.js-print', function() {
        if (confirm("현재창을 인쇄합니다.\n이미지가 인쇄되지 않을 경우 브라우저 설정을 확인해주세요.") === true) {
            window.print();
        } else { //취소
            return;
        }
    });

    /* popup handler */
    $('.js-popup, .js-tooltip').on('click', $.handlerPopup);

    $('input.js-check-all').jsCheckAll();

    /* disabled */
    $('input.disabled, input[data-disabled]').prop('disabled', true);

    /* polyfills  ****************************************************************************/

    /* placeholder */
    $('input[placeholder]').placeHolder({
        formCheck: true
    });

    /* comment collpase */
    $('.js-collapse-comment').each(function() {
        var $table = $(this),
            $anchor = $table.find('a.js-collapse-toggler');
        $anchor.on('click', function(e) {
            e.preventDefault();
            var $parent = $(this).parents('tr.head'),
                $next = $parent.next();
            $parent.toggleClass('active').next().children('td').toggle();
            $parent.siblings().removeClass('active');
            $table.find('tr.body').not($next).children('td').hide();
        });
    });

});
$(function() {

    var t = setTimeout(function() {
        $('body').addClass('loaded');
    }, 500);

    // aside 고정 유무
    $.asideFix = function(todo) {
        if (todo !== true) {
            $('#aside').css({
                'position': 'absolute'
            });
        }
    };

    // nav 초기 활성화
    $.activeNav = function(d1, d2, d3) {
        var $d1 = $('#nav li.d1').eq(d1),
            $d2 = $d1.find('li.d2').eq(d2),
            $d3 = $d2.find('li.d3').eq(d3);
        $d1.addClass('active').children('ul').show();
        $d2.addClass('active').children('ul').show();
        $d3.addClass('active').children('ul').show();
    };

    // aside 토글
    $("#toggleAside").click(function(e) {
        e.preventDefault();

        if ($("body").hasClass("aside-on")) {
            // writeCookie("toggleAsideHide","Y",1);
            $("body").toggleClass("aside-on");
        } else {
            // deleteCookie("toggleAsideHide");
            $("body").toggleClass("aside-on");
        }
    });

    // 아이콘 그리기
    $('#nav a.d1, #nav a.d2').each(function() {
        if ($(this).next('ul').length) {
            $(this).addClass('has-child');
        }
    });

    // navigation 활성화
    $('#nav ul.core a').on('click', function(e) {

        var $li = $(this).parent(),
            $ul = $li.children('ul');

        $li.toggleClass('active');

        if ($li.hasClass('active')) {

            $li.siblings().removeClass('active').children('ul').slideUp('fast');

            if ($ul.length && $ul.is(':hidden')) {
                $ul.slideDown('fast');
            }

        } else {
            $ul.slideUp('fast');
        }

    });

    // 메뉴별 권한 설정 펼치기 접기 
    $('.menu-list .head a.button').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            $module = $this.parents('.module'),
            $body = $module.find('.body');
        $body.slideToggle('fast', function() {
            if ($body.is(':visible')) {
                $module.find('a.button').text('접기');
            } else {
                $module.find('a.button').text('펼치기');
            }
        });
    });

    // 메뉴별 권한 체크박스 토글
    $('.menu-list .head input[type="checkbox"]').on('click', function(e) {
        var $sub = $(this).parents('.module').find('.body').find('input[type="checkbox"]');
        if ($(this).prop('checked') === true) {
            $sub.prop('checked', true);
        } else {
            $sub.prop('checked', false);
        }
    });

    // 메뉴별 권한 체크박스 토글 부가기능
    $('.menu-list .body input[type="checkbox"]').on('click', function(e) {
        var index = true;
        var $group = $(this).parents('.body').find('input[type="checkbox"]');
        var $headCheck = $(this).parents('.module').find('.head').find('input[type="checkbox"]');
        $group.each(function() {
            if ($(this).prop('checked') === false) {
                index = false;
            }
        });
        if (index === false) {
            $headCheck.prop('checked', false);
        } else {
            $headCheck.prop('checked', true);
        }
    });

    // 메뉴관리 전체 선택 
    $('.js-toggle-menu a').on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var $check = $('.menu-list').find('input[type="checkbox"]');
        if ($this.hasClass('check-all')) {
            $check.prop('checked', true);
        } else {
            $check.prop('checked', false);
        }
    });

    // 하위 메뉴 추가 팝업
    $('.js-pop-addmenu').on('click', $.handlerPopup);

});
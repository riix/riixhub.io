/*
 *  접근성을 고려한 DatePicker v0.3
 *  Author : 박순길
 *  Based On : Accessible Date Picker Calendar - webSemantics, http://www.websemantics.co.uk/tutorials/accessible_date_picker_calendar/
 */

var date = new Date(),
    dateToday = date,
    dateMonths = new Array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'),
    weekDay = new Array('일', '월', '화', '수', '목', '금', '토'),
    dateSplit = "-";

function add0(x) {
    return ((x < 10) ? "0" + x : x);
}

function textDate(d, m, y, whatday) {
    var td = new Date(y, m - 1, d);
    if (whatday === true) {
        return (td.getFullYear() + "-" + dateMonths[td.getMonth()] + "-" + add0(td.getDate()) + " (" + weekDay[td.getDay()] + ")");
    } else {
        return (td.getFullYear() + "-" + dateMonths[td.getMonth()] + "-" + add0(td.getDate()));
    }
}

function subtractDate(oneDate, anotherDate) {
    return (anotherDate - oneDate);
}

function toShortDate(d) {
    d = new Date(d);
    return (add0(d.getDate()) + dateSplit + add0(d.getMonth() + 1) + dateSplit + d.getFullYear());
}

function writeInputDateValue(cObj, obj) {
    d = $(obj).data("day");
    if (cObj.shortDate) d = toShortDate(d);
    $("#" + cObj.inputId).val(d);
    hideCalendar(cObj);
}

function calendarObject(t_id, i_id, b_id, s_id, n_id) {
    this.wrapperId = t_id;
    this.inputId = i_id;
    this.buttonId = b_id;
    this.holderId = s_id;
    this.nextId = n_id;
    this.shortDate = false;
}

function matchToday() {
    $('table.calendar td a').each(function() {
        var _today = $('div.cal-wrapper div.today a.today').data('day');
        var $this = $(this);
        if ($this.data('day') == _today) $this.attr('title', $this.attr('title') + ' (오늘)').addClass('today');
    });
}

function buildCalendar(date, calendarEl) {

    var day = date.getDate(),
        month = date.getMonth(),
        year = date.getFullYear(),
        thisMonth = new Date(year, month, 1),
        nextMonth = new Date(year, month + 1, 1);

    var _isOver = false;

    var _minDay = [];
    var _maxDay = [];

    var $input = $('#' + calendarEl.inputId);

    if ($input.data('min')) _minDay = $input.data('min').split('-', 3); // 최소 선택 가능
    if ($input.data('max')) _maxDay = $input.data('max').split('-', 3); // 최대 선택 가능

    var firstWeekDay = thisMonth.getDay();
    var daysInMonth = Math.floor((nextMonth.getTime() - thisMonth.getTime()) / (1000 * 60 * 60 * 24));

    if (month == 2) daysInMonth = 31;

    var _calendarHtml = '';

    _calendarHtml += '<div id="' + calendarEl.wrapperId + '" class="cal-wrapper">';
    _calendarHtml += '  <div class="head">';
    _calendarHtml += '      <div class="year"><select title="년도 선택" class="select">';

    for (var y = 1910; y < 2020; y++) {
        if (y == year) {
            _calendarHtml += '      <option value="' + y + '" selected>' + y + '년</option>';
        } else {
            _calendarHtml += '      <option value="' + y + '">' + y + '년</option>';
        }
    }

    _calendarHtml += '      </select><a href="javascript:;" class="button"><span class="sr-only">선택한 년도로</span><span>이동</span></a></div>';
    _calendarHtml += '      <div class="month">';

    if ((year < _minDay[0]) || (year == _minDay[0] && dateMonths[month] <= _minDay[1])) {
        _calendarHtml += '          <a href="javascript:;" class="nav prevmonth disabled"><span class="icon datepicker-prev"><em>이전 ' + dateMonths[(month === 0) ? 11 : month - 1] + ' 월로 이동</em></span></a>';
    } else {
        _calendarHtml += '          <a href="javascript:;" class="nav prevmonth"><span class="icon datepicker-prev"><em>이전 ' + dateMonths[(month === 0) ? 11 : month - 1] + ' 월로 이동</em></span></a>';
    }

    _calendarHtml += '          <span class="month"><strong>' + dateMonths[month] + '</strong>월<span class="sr-only"> - 현재 월</span></span>';

    if ((year > _maxDay[0]) || (year == _maxDay[0] && dateMonths[month] >= _maxDay[1])) {
        _calendarHtml += '          <a href="javascript:;" class="nav nextmonth disabled"><span class="icon datepicker-next"><em>다음 ' + dateMonths[(month == 11) ? 0 : month + 1] + ' 월로 이동</em></span></a>';
    } else {
        _calendarHtml += '          <a href="javascript:;" class="nav nextmonth"><span class="icon datepicker-next"><em>다음 ' + dateMonths[(month == 11) ? 0 : month + 1] + ' 월로 이동</em></span></a>';
    }

    _calendarHtml += '      </div>';
    _calendarHtml += '      <div class="today"><a href="javascript:;" class="today" data-day=' + textDate(dateToday.getDate(), dateToday.getMonth() + 1, dateToday.getFullYear(), false) + '>오늘날짜: <span>' + textDate(dateToday.getDate(), dateToday.getMonth() + 1, dateToday.getFullYear(), true) + '</span><span class="sr-only"> - 해당 년/월로 이동</span></a></div>';
    _calendarHtml += '  </div>';
    _calendarHtml += '  <div class="core">';
    _calendarHtml += '      <table class="calendar" cellspacing="0" summary="날짜 선택을 위한 양식입력 테이블입니다.">';
    _calendarHtml += '          <caption>날짜 선택</caption>';
    _calendarHtml += '          <thead><tr><th scope="col" class="weekend sun"><abbr title="일요일">S</abbr></th><th scope="col" class="mon"><abbr title="월요일">M</abbr></th><th scope="col" class="tue"><abbr title="화요일">T</abbr></th><th scope="col" class="wed"><abbr title="수요일">W</abbr></th><th scope="col" class="thu"><abbr title="목요일">T</abbr></th><th scope="col" class="fri"><abbr title="금요일">F</abbr></th><th scope="col" class="weekend sat"><abbr title="토요일">S</abbr></th></tr></thead>';
    _calendarHtml += '          <tbody>';
    _calendarHtml += '              <tr>';

    for (var week = 0; week < firstWeekDay; week++) {
        if (week === 0) {
            _calendarHtml += '<td class="empty weekend sun">&nbsp;</td>';
        } else if (week == 6) {
            _calendarHtml += '<td class="empty weekend sat">&nbsp;</td>';
        } else {
            _calendarHtml += '<td class="empty">&nbsp;</td>';
        }
    }

    var mm = nextMonth.getMonth();
    if (mm < 1) mm = 12;
    mm = add0(mm);
    week_day = firstWeekDay;

    for (var dayCounter = 1; dayCounter <= daysInMonth; dayCounter++) {
        week_day %= 7;
        if (week_day === 0) _calendarHtml += '    </tr>';
        if (week_day === 0) {
            _calendarHtml += '<td class="weekend sun">';
        } else if (week_day == 6) {
            _calendarHtml += '<td class="weekend sat">';
        } else {
            _calendarHtml += '<td>';
        }
        if ((year < _minDay[0]) || (year == _minDay[0] && dateMonths[month] < _minDay[1]) || (year == _minDay[0] && dateMonths[month] == _minDay[1] && dayCounter < _minDay[2])) {
            _isOver = true;
            _calendarHtml += '<span title="' + textDate(dayCounter, mm, year, true) + '">' + add0(dayCounter) + '</span></td>';
        } else if ((year > _maxDay[0]) || (year == _maxDay[0] && dateMonths[month] > _maxDay[1]) || (year == _maxDay[0] && dateMonths[month] == _maxDay[1] && dayCounter > _maxDay[2])) {
            _isOver = true;
            _calendarHtml += '<span title="' + textDate(dayCounter, mm, year, true) + '">' + add0(dayCounter) + '</span></td>';
        } else {
            _isOver = false;
            _calendarHtml += '<a title="' + textDate(dayCounter, mm, year, true) + '" data-day="' + textDate(dayCounter, mm, year, false) + '" href="' + dayCounter + '">' + add0(dayCounter) + '</a></td>';
        }
        week_day++;
    }

    for (week_day = week_day; week_day < 7; week_day++) { // 빈 셀 채우기
        if (week_day === 0) {
            _calendarHtml += '<td class="empty weekend sun">&nbsp;</td>';
        } else if (week_day == 6) {
            _calendarHtml += '<td class="empty weekend sat">&nbsp;</td>';
        } else {
            _calendarHtml += '<td class="empty">&nbsp;</td>';
        }
    }

    _calendarHtml += '          </tr></tbody></table>';
    _calendarHtml += '  </div>';
    _calendarHtml += '  <a id="skipCalendar" href="#' + calendarEl.nextId + '" class="button-close"><span class="icon close"><em>날짜 선택창 (현재 창) 닫기</em></span></a>';
    _calendarHtml += '</div>';

    return _calendarHtml;
}

function displayCalendar(calendarEl) {

    var $holder = $("#" + calendarEl.holderId);
    var $wrapper = $("#" + calendarEl.wrapperId);
    var $button = $("#" + calendarEl.buttonId);

    $holder.empty().append(buildCalendar(date, calendarEl));

    // $("#" + calendarEl.wrapperId + " div.year a").on('click', function(e) {
    $('div.year a').on('click', $wrapper, function(e) {
        e.preventDefault();
        var _year = $(this).parents('div').eq(0).find('select').eq(0).val();
        dateTemp = new Date(_year, date.getMonth(), 1);
        date = dateTemp;
        displayCalendar(calendarEl);
        $("#" + calendarEl.wrapperId + " div.year select").eq(0).focus();
    });

    // 이전 달 
    $('a.prevmonth').on('click', $wrapper, function(e) {
        e.preventDefault();
        var dateTemp = new Date(date.getFullYear(), date.getMonth() - 1, 1);
        if ($(this).hasClass('disabled')) {
            var t = $('#' + calendarEl.inputId).data('min');
            alert(t + ' 을 벗어난 달은 선택이 불가능 합니다.');
        } else {
            date = dateTemp;
            displayCalendar(calendarEl);
        }
        $(this).focus();
    });

    // 다음 달
    $('a.nextmonth').on('click', $wrapper, function(e) {
        e.preventDefault();
        var dateTemp = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        if ($(this).hasClass('disabled')) {
            var t = $('#' + calendarEl.inputId).data('max');
            alert(t + ' 을 벗어난 달은 선택이 불가능 합니다.');
        } else {
            date = dateTemp;
            displayCalendar(calendarEl);
        }
        $(this).focus();
    });

    // 날짜 선택시
    $('table.calendar td a').on('click', $wrapper, function(e) {
        e.preventDefault();
        writeInputDateValue(calendarEl, $(this));
        return false;
    });

    // 닫기
    $('a.button-close').on('click', $wrapper, function(e) {
        e.preventDefault();
        hideCalendar(calendarEl);
        $button.focus();
    });

    // 오늘 날짜
    $('div.today a').on('click', $wrapper, function(e) {
        e.preventDefault();
        date = new Date();
        displayCalendar(calendarEl);
        $wrapper.find('a.today').eq(0).focus();
    });

    // 위치 잡기 
    var _btnOffset = $button.offset();
    $holder.css({
        top: _btnOffset.top + 30,
        left: _btnOffset.left - 70,
        opacity: 1
    }).find('select').eq(0).focus();

    matchToday();

    return false;
}

function hideCalendar(calendarEl) {
    $("#" + calendarEl.holderId).animate({
        opacity: 0
    }, 300, function() {
        $(this).remove();
    });
    var $target = $('#' + $("#" + calendarEl.buttonId).data('target'));
    $target.focus();
    return false;
}

$(function() {

    var calVar; // cal var.

    $('.btn-datepicker, .js-datepicker').each(function(i) {

        var $this = $(this);

        if (!$this.data('target')) {
            var temp = $this.attr('href');
            if (temp.substring('0', '1') !== '#') {
                _href = $this.prev('input').eq(0).attr('id');
            } else {
                _href = temp.replace('#', '');
            }
            $this.data('target', _href);
        }

        var wrapperId = "calc" + i;
        var inputId = $this.data('target');
        var $input = $('#' + inputId);

        $input.attr('maxlength', '10').on('focusout', function() {
            var regExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})/g;
            var _val = $(this).val();
            if (_val !== '' && !regExp.test(_val)) {
                alert('날짜 형식에 맞게 입력해주세요.\n예) 2013-11-11');
                setTimeout(function() {
                    $input.val('').focus();
                }, 100);
            }
        });

        $this.on('click', function(e) {
            e.preventDefault();

            $('.section-datepicker').remove();

            var _inputVal = $input.val();
            var regExp = /^([0-9]{4})-([0-9]{2})-([0-9]{2})/g;

            if (_inputVal !== '' && regExp.test(_inputVal)) {
                var _t = _inputVal.split('-', 4);
                date = new Date(_t[0], _t[1] - 1, _t[2]);
            } else {
                date = new Date();
            }

            calVar = new calendarObject("cal" + i, inputId, "calBtn" + i, "calSpace" + i, wrapperId);

            $this.attr('id', calVar.buttonId);

            var calspaceHTML = '<div class="section-datepicker" id="' + calVar.holderId + '"></div>';

            if ($input.data('position') == 'bottom') {
                $('body').append(calspaceHTML);
            } else {
                $this.after(calspaceHTML);
            }

            displayCalendar(calVar);
        });
    });

});
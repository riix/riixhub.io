/**
 * Debug Log
 */
if (!window.console) {
  var Console = function() {
   return {
    log : function(message) {},
    info : function(message) {},
    warn : function(message) {},
    error : function(message) {}
   };
  };
  console = Console();
}

// 공통 Ready
$(document).ready(function(){
    //숫자만 입력
	$(".number").keyup(function(event){
        if (!(event.keyCode >=37 && event.keyCode<=40)) {
            var inputVal = $(this).val();
            $(this).val(inputVal.replace(/[^0-9]/gi,''));
        }
    });
	
	// 상품 통합 검색
	goCombineSearch = function (domain)
	{
	    var combineSearchForm = document.combineSearchForm;
	    var combineSearchWord = combineSearchForm.combineSearchWord;
	    if(combineSearchWord == undefined) return false;
	    
	    if(combineSearchWord.value == "")
	    {
	        alert("검색어를 입력해 주십시오.");
	        combineSearchWord.focus();
	        return false;
	    }
	    else
	    {
	        combineSearchForm.action = domain + "/mo/search/combineSearch.do";
	        combineSearchForm.method = "get";
	        combineSearchForm.target = "_self";
	        combineSearchForm.submit();
	    }
	    return false;
	};
	
});

// 사이트구분
function getSite()
{
    var $site = $("#asideNav").data("site");
    if($site == undefined)
    {
        $site = "";
    }
    
    return $site; 
}

// 우편번호 팝업
function goSearchZipCode(serverDomain)
{
    window.open(serverDomain + "/mo/common/commonZipCodePopup.do", "searchZipCodePopup", "width=750, height=685, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=yes");
}

// 로그인
function goLogin(serverDomain, url, parentFunctionYn, confirmYn) 
{
	var appReturnUrl = ""; // 모바일APP parameter
	
    if(url == "")
    {
        url = document.location.href;
        if(url.indexOf("/mo/manager/login") > -1 )
        {
            url = "/intro.do";
        }
    }
    var path = window.louisclubUrl("path", url);
    var param = window.louisclubUrl("?", url);
    if(param != "") param = "?" + param;
    url = path + param;
    appReturnUrl = serverDomain + url;
    url = encodeURIComponent(url);
    
    if ( ISMOBILEAPP )
    {
        // 모바일APP - logintype : L(일반로그인), O(비회원주문로그인)
	    appReturnUrl = encodeURIComponent(appReturnUrl);
    	if(parentFunctionYn!=undefined && "Y"==parentFunctionYn)
    	{
    	    alert("louisclub://login?logintype=L&url=" + appReturnUrl + "&pFunYn=Y&site=" + getSite());
    	}
    	else
    	{
    	    alert("louisclub://login?logintype=L&url=" + appReturnUrl + "&pFunYn=N&site=" + getSite());
    	}
    }
    else
    {
    	// 모바일웹
        if(confirmYn == 'N')
        {
            if(parentFunctionYn!=undefined && "Y"==parentFunctionYn)
            {
                document.location.href = serverDomain + "/mo/manager/login.do?pFunYn=Y&url="+url + "&site=" + getSite();
            }
            else
            {
                document.location.href = serverDomain + "/mo/manager/login.do?url="+url + "&site=" + getSite();
            }
        }
        else
        {
    		if(parentFunctionYn!=undefined && "Y"==parentFunctionYn)
    		{
    		    document.location.href = serverDomain + "/mo/manager/login.do?pFunYn=Y&url="+url + "&site=" + getSite();
    		}
    		else
    		{
    		    document.location.href = serverDomain + "/mo/manager/login.do?url=" + url + "&site=" + getSite();
    		}
        }
    }
}

// 비회원 로그인 (실명인증)
function accreditationLogin(serverDomain)
{
    if ( ISMOBILEAPP )
    {
        // 모바일APP - logintype : L(일반로그인), O(비회원주문로그인)
    	alert("louisclub://login?logintype=O&url=&pFunYn=Y&site=" + getSite());
    }
    else
    {
    	// 모바일웹
    	document.location.href = serverDomain + "/mo/manager/accreditationLoginStep1.do?site=" + getSite();
    }
}

//송장번호 조회
function invoicePop(jsp_ssh, ov1, ov2, ov3, ov4)
{
	$.ajax({
        type : "POST", 
        url : jsp_ssh+"/mo/order/invoiceInfoAjax.do",  
        data : {"ORD_MST_CD" : ov1, "PRD_MST_CD" : ov2, "ORD_PRD_SEQ_IDX" : ov3, "ORD_PRD_IDX" : ov4},
        async : false,
        success : function (data) 
        {
        	popup(data.result, "555", "600", "yes", "invoice");
        },
        error : function () 
        {
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        }
    });
}

//장바구니 담기 ( 도메인정보[필수] , 상품코드[필수], 옵션코드[필수], 구매수량[필수] )
function putCart(jsp_ssh,ov,ov2,ov3) {
    $.ajax({
        type : "POST", 
        url : jsp_ssh+"/mo/order/insertCartAjax.do",  
        data : {"PRD_MST_CD" : ov, "PRD_OPT_IDX" : ov2, "CRT_CNT": ov3}, 
        async : false,
        success : function (data) 
        {
            if(data.cart.result=="S00")    // 성공
            {
            	// 광고효과측정 쿠키
            	writeCookie("KMAZ_"+ov, new Date().getDate(), 7);
            	
                $(location).attr("href",jsp_ssh+"/mo/order/cart.do?site="+getSite());
            }
            else if(data.cart.result=="F00")  // 인증오류
            {
        	    goLogin(jsp_ssh,'','Y','N');    
            }
            else if(data.cart.result=="F01")  // 판매중인 상품 X
            {
        	    alert("판매중인 상품이 아닙니다.");  
            }
            else if(data.cart.result=="F02")  // 옵션 수량부족
            {
        	    alert("수량이 부족합니다.");  
            }
            else	
            {
        	    alert("데이터 오류 입니다. 다시 시도 해 주십시오.");  
            }
        },
        error : function () 
        {
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        }
    });
}

//바로구매 ( 도메인정보[필수] , 상품코드[필수], 옵션코드[필수], 구매수량[필수], 픽업스토어YN, 팝업YN )
function goDirectOrder(jsp_ssh,ov,ov2,ov3,pickup) {
	if(pickup!="Y"){ pickup="N"; }
	$.ajax({
        type : "POST", 
        url : jsp_ssh+"/mo/order/directCheckAjax.do",  
        data : {"PRD_MST_CD" : ov, "PRD_OPT_IDX" : ov2, "CRT_CNT": ov3,"pickup": pickup}, 
        async : false,
        success : function (data) 
        {
            if(data.result=="S00")    // 성공
	        {
            	// 광고효과측정 쿠키
            	writeCookie("KMAZ_"+ov, new Date().getDate(), 7);
            	
            	if(pickup=="Y")
    			{
    				alert("픽업스토어를 신청하셨습니다.\n상품 결제 후 주문완료페이지를\n인쇄하셔서 매장을 방문해주세요.");
    			}
            	$(location).attr("href",jsp_ssh+"/mo/order/order.do?PRD_MST_CD="+ov+"&PRD_OPT_IDX="+ov2+"&CRT_CNT="+ov3+"&PATH=direct&pickup="+pickup+"&site="+getSite());
	        }
	        else if(data.result=="F00")  // 인증오류
	        {
	        	accreditationLogin(jsp_ssh);    
	        }
	        else if(data.result=="F01")  // 판매중인 상품 X
	        {
	        	alert("판매중인 상품이 아닙니다.");  
	        }
	        else if(data.result=="F02")  // 옵션 수량부족
	        {
	        	alert("수량이 부족합니다.");  
	        }
	        else if(data.result=="F03")  // 프라이빗 상품 대상자 X	
	        {
	        	alert("PRIVATE SHOP 대상자가 아닙니다.");  
	        }
	        else	
	        {
	        	alert("데이터 오류 입니다. 다시 시도 해 주십시오.");  
	        }
        },
        error : function () 
        {
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        }
    });
}

//장바구니 이동 ( 도메인정보[필수] )
function goCart(jsp_ssh)
{
	$.ajax({
      type : "POST", 
      url : jsp_ssh+"/mo/common/checkLogin.do",  
      async : false,
      success : function (data) 
      {
          if(data.result=="Y")    // 로그인 상태
	        {
        	    $(location).attr("href",jsp_ssh+"/mo/order/cart.do?site="+getSite());
	        }
	        else
	        {
	        	goLogin(jsp_ssh, jsp_ssh+"/mo/order/cart.do?site="+getSite());    
	        }
      },
      error : function () 
      {
          alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      }
  });
}

//좋아요 버튼 이벤트 로그인 후 호출 함수
likeProduct = function(prdMstCd, jsp_ssh, callbackYn, timesaleYn)
{
	var returnUrl = "/mo/product/productView.do";
	if ( timesaleYn == "Y" )
	{
		returnUrl = "/mo/specialtyshop/timeSaleView.do";
	}
	
	$.ajax({
      type : "POST", 
      url : jsp_ssh+"/mo/product/productLikeAjax.do",  
      async : false,
      data : {"PRD_MST_CD" : prdMstCd}, 
      success : function (data) 
      {
          if (data.result == 'S00')
      	  {
      	      alert("정상적으로 추천 하였습니다.");
      	      
        	  if (callbackYn == 'Y')
          	  {
      	          $(location).attr("href", jsp_ssh + returnUrl + "?PRD_MST_CD=" + prdMstCd + "&site=" + getSite());
          	  }
      	  }
          else if (data.result == 'F02')
          {
      		  alert("이미 추천 하였습니다.");
      		  
        	  if (callbackYn == 'Y')
          	  {
          	      $(location).attr("href", jsp_ssh + returnUrl + "?PRD_MST_CD=" + prdMstCd + "&site=" + getSite());
          	  }
          }
      	  else if (data.result == 'F00')
      	  {
              //alert("로그인 후 이용 가능합니다.");
              goLogin(jsp_ssh,'','Y'); 
          }
          else
          {
              alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
          }
      },
      error : function () 
      {
          alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      }
  });
};
//상품평 추천하기
goRecommend = function(prdRevIdx, revRcmRegId, sessionId, prdMstCd, jsp_ssh, callbackYn)
{
  if ( sessionId != '' && sessionId == revRcmRegId )
  {
      alert("본인이 작성한 게시글은 추천하실 수 없습니다.");
      
      return;
  }
  
	$.ajax({
      type : "POST", 
      url : jsp_ssh+"/mo/product/commentRecommendAjax.do",  
      async : false,
      data : {"PRD_REV_IDX" : prdRevIdx, "PRD_MST_CD" : prdMstCd}, 
      success : function (data) 
      {
      	if (data.result == 'S00')
      	{
      	    alert("정상적으로 추천하였습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	    else
      	    {
      	        //리스트 추천수 업데이트
      	        var revRcmCnt = parseInt($("#spanRevRcmCnt_"+prdRevIdx).text());
      	        $("#spanRevRcmCnt_"+prdRevIdx).text((revRcmCnt+1));
      	    }
      	}
      	else if (data.result == 'F02')
      	{
      		alert("이미 추천하였습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	}
      	else if (data.result == 'F03')
      	{
      		alert("본인이 작성한 게시글은 추천하실 수 없습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	}
      	else if (data.result == 'F00')
      	{
      		//alert("로그인 후 이용 가능합니다.");
      		goLogin(jsp_ssh,'','Y'); 
      	}
      	else
      	{
              alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      	}
      },
      error : function () 
      {
          alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      }
  });
};

//상품평 신고하기
goReport = function(prdRevIdx, revRcmRegId, sessionId, prdMstCd, jsp_ssh, callbackYn)
{
  if ( sessionId != '' && sessionId == revRcmRegId )
  {
      alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

      return;
  }
  
	$.ajax({
      type : "POST", 
      url : jsp_ssh+"/mo/product/commentIllegalAjax.do",  
      async : false,
      data : {"PRD_REV_IDX" : prdRevIdx, "PRD_MST_CD" : prdMstCd}, 
      success : function (data) 
      {
      	if (data.result == 'S00')
      	{
      	    alert("정상적으로 신고하였습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	}
      	else if (data.result == 'F02')
      	{
      		alert("이미 신고하였습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	}
      	else if (data.result == 'F03')
      	{
      		alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

      	    if (callbackYn == 'Y')
      	    {
      	    	$(location).attr("href",jsp_ssh+"/mo/product/productView.do?PRD_MST_CD="+prdMstCd+"&site="+getSite());
      	    }
      	}
      	else if (data.result == 'F00')
      	{
      		//alert("로그인 후 이용 가능합니다.");
      		goLogin(jsp_ssh,'','Y'); 
      	}
      	else
      	{
              alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      	}
      },
      error : function () 
      {
          alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
      }
  });
};

//STYLE IT 좋아요 버튼 이벤트 로그인 후 호출 함수
likeStyleIt = function(styMstIdx, styMstRegId, sessionId, jsp_ssh, callbackYn)
{
    if ( sessionId != '' && sessionId == styMstRegId )
    {
        alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

        return;
    }
    
	$.ajax({
        type : "POST", 
        url : jsp_ssh+"/mo/louissquare/styleItRecommendAjax.do",  
        async : false,
        data : {"STY_MST_IDX" : styMstIdx}, 
        success : function (data) 
        {
        	if (data.result == 'S00')
        	{
        	    alert("정상적으로 추천 하였습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F02')
        	{
        		alert("이미 추천 하였습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F03')
        	{
        		alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F00')
        	{
        		//alert("로그인 후 이용 가능합니다.");
        		goLogin(jsp_ssh,'','Y'); 
        	}
        	else
        	{
                alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        	}
        },
        error : function () 
        {
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        }
    });
};

// STYLE IT 신고하기
goReportStyleIt = function(styMstIdx, styMstRegId, sessionId, jsp_ssh, callbackYn)
{
    if ( sessionId != '' && sessionId == styMstRegId )
    {
        alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

        return;
    }
    
	$.ajax({
        type : "POST", 
        url : jsp_ssh+"/mo/louissquare/styleItIllegalAjax.do",  
        async : false,
        data : {"STY_MST_IDX" : styMstIdx}, 
        success : function (data) 
        {
        	if (data.result == 'S00')
        	{
        	    alert("정상적으로 신고하였습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F02')
        	{
        		alert("이미 신고하였습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F03')
        	{
        		alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

          	    if (callbackYn == 'Y')
          	    {
          	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/styleItView.do?STY_MST_IDX="+styMstIdx+"&site="+getSite());
          	    }
        	}
        	else if (data.result == 'F00')
        	{
        		//alert("로그인 후 이용 가능합니다.");
        		goLogin(jsp_ssh,'','Y'); 
        	}
        	else
        	{
                alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        	}
        },
        error : function () 
        {
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
        }
    });
};

//댓글 신고하기
goReportCommentMaster = function(cmtMstIdx, cmtMstRegId, sessionId, brdWctIdx, jsp_ssh, callbackYn)
{
if ( sessionId != '' && sessionId == cmtMstRegId )
{
    alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

    return;
}

	$.ajax({
    type : "POST", 
    url : jsp_ssh+"/mo/louissquare/webCartoonCommentMasterIllegalAjax.do",  
    async : false,
    data : {"CMT_MST_IDX" : cmtMstIdx, "BRD_WCT_IDX" : brdWctIdx}, 
    success : function (data) 
    {
    	if (data.result == 'S00')
    	{
    	    alert("정상적으로 신고하였습니다.");

    	    if (callbackYn == 'Y')
    	    {
    	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/webCartoonView.do?BRD_WCT_IDX="+brdWctIdx+"&site="+getSite());
    	    }
    	}
    	else if (data.result == 'F02')
    	{
    		alert("이미 신고하였습니다.");

    	    if (callbackYn == 'Y')
    	    {
    	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/webCartoonView.do?BRD_WCT_IDX="+brdWctIdx+"&site="+getSite());
    	    }
    	}
    	else if (data.result == 'F03')
    	{
    		alert("본인이 작성한 게시글은 신고하실 수 없습니다.");

    	    if (callbackYn == 'Y')
    	    {
    	    	$(location).attr("href",jsp_ssh+"/mo/louissquare/webCartoonView.do?BRD_WCT_IDX="+brdWctIdx+"&site="+getSite());
    	    }
    	}
    	else if (data.result == 'F00')
    	{
    		//alert("로그인 후 이용 가능합니다.");
    		goLogin(jsp_ssh,'','Y'); 
    	}
    	else
    	{
            alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
    	}
    },
    error : function () 
    {
        alert("데이터 오류 입니다. 다시 시도 해 주십시오.");
    }
});
};

/*
 * 날짜보기
 * strDate : 날짜(YYYYMMDD)
 * str : 구분자
 * */
function getDateView(strDate, str){
    strDate = strDate.replace(/\-/g, "");
    return strDate.substring(0, 4) + str + strDate.substring(4, 6) + str + strDate.substring(6, 8);
}
/*
 * 날짜보기
 * strDate : 날짜(YYYYMMDD HHMMSS)
 * str : 날짜 구분자
 * str2 : 시간 구분자
 * */
function getDateTimeView(strDate, str, str2){
    strDate = strDate.replace(/\-/g, "");
    return strDate.substring(0, 4) + str + strDate.substring(4, 6) + str + strDate.substring(6, 8) + " " + strDate.substring(8, 10) + str2 + strDate.substring(10, 12) + str2 + strDate.substring(12, 14);
}

// 날짜 셋팅 - 오늘, 일주일, 15일, 한달, 두달
function fnDateSet(v_start_name, v_end_name, s_year, s_month, s_day, e_year, e_month, e_day, seperator){
    $("#"+v_start_name	).val(getCalculatedDate(s_year, s_month, s_day, seperator));
    $("#"+v_end_name	).val(getCalculatedDate(e_year, e_month, e_day, seperator));
}
function getCalculatedDate(iYear, iMonth, iDay, seperator){
    //현재 날짜 객체를 얻어옴.
    var gdCurDate = new Date();

    //현재 날짜에 날짜 게산.
    gdCurDate.setYear ( gdCurDate.getFullYear() + iYear );
    gdCurDate.setMonth( gdCurDate.getMonth() 	+ iMonth);
    gdCurDate.setDate ( gdCurDate.getDate() 	+ iDay 	);

    //실제 사용할 연, 월, 일 변수 받기.
    var giYear  = gdCurDate.getFullYear();
    var giMonth = gdCurDate.getMonth()+1;
    var giDay   = gdCurDate.getDate();

    //월, 일의 자릿수를 2자리로 맞춘다.
    giMonth = "0" + giMonth;
    giMonth = giMonth.substring(giMonth.length-2,giMonth.length);
    giDay   = "0" + giDay;
    giDay   = giDay.substring(giDay.length-2,giDay.length);

    //display 형태 맞추기.
    return giYear + seperator + giMonth + seperator +  giDay;
}

/**
 * 글자제한
 * onkeyup="getStrByte(this,'#byte', 200);"
 */
function getStrByte(obj, tgt, size) {
	var str = obj.value;
	var str_max = size;	
	var p = 0;
	var bytes = 0;
	var len_num = 0;  
	var str2 = "";
 
	if(str != ""){
		for(p=0; p<str.length; p++) {
			(str.charCodeAt(p) > 255) ? bytes+=3 : bytes++; //한글은 3byte, 영문, 숫자, 공백은 1byte
			if(bytes <= str_max){
				len_num = p + 1;
			}else{
				alert(size + " byte를 초과 입력할수 없습니다.\n초과된 내용은 자동으로 삭제 됩니다.");
				str2 = str.substr(0, len_num);
				obj.value = str2;
				break;
			}
			$(tgt).text(bytes);
		}
	}else{
		$(tgt).text("0");
	}
	obj.focus();
}
/**
 * 글자제한(글자수기준)
 * onkeyup="getStrLength(this,'#byte', 200);"
 */
function getStrLength(obj, tgt, size) {
	var str = obj.value;
	var str_max = size;	
	var length = 0;
	var str2 = "";
 
	if(str != ""){
		length = str.length;
		if(length > str_max){
			alert(size + " 글자를 초과 입력할수 없습니다.\n초과된 내용은 자동으로 삭제 됩니다.");
			str2 = str.substr(0, str_max);
			obj.value = str2;
		}
		$(tgt).text(length);
	}else{
		$(tgt).text("0");
	}
	obj.focus();
}
/**
 * 가격숫자만 입력가능체크 및 콤마
 * @param obj
 * @param fLen
 * @returns {Boolean}
 */
function addComma(obj,fLen)
{ 
   if(event.keyCode == 37 || event.keyCode == 39 ) 
   {                                              
      return;
   }
   var fLen = fLen || 2; 
   var strValue = obj.value.replace(/,|\s+/g,'');
   var strBeforeValue = (strValue.indexOf('.') != -1)? strValue.substring(0,strValue.indexOf('.')) :strValue ;
   var strAfterValue  = (strValue.indexOf('.') != -1)? strValue.substr(strValue.indexOf('.'),fLen+1) : '' ;
   if(isNaN(strValue))
   {
      alert('숫자만 입력가능합니다.');
      obj.value = "";
      return false;
   }
   var intLast =  strBeforeValue.length-1;
   var arrValue = new Array;
   var strComma = '';

   for(var i=intLast,j=0; i >= 0; i--,j++)
   { 
         
          if( j !=0 && j%3 == 0) 
          {   
              strComma = ',';
          }
          else
          {
              strComma = '';
          } 
          arrValue[arrValue.length] = strBeforeValue.charAt(i) + strComma  ;
   }
   obj.value=  arrValue.reverse().join('') +  strAfterValue;
}
/**
 * 콤마 만들기
 * @param x
 * @returns
 */
function setComma(x){
       var pattern = /(^[+-]?\d+)(\d{3})/;
       x += '';
       while (pattern.test(x)){
           x = x.replace(pattern, '$1' + ',' + '$2');
       }
       return x;
}
/**
 * 콤마제거
 * @param val
 * @returns
 */
function removeComma(val) {
    console.log("val " + val)
    if (val == "" || val == undefined){
        val ="";
    }
    self.focus();
    return val.split(",").join("");
}

//콤마제거후 숫자리턴
function getInt(val) {
    
    if (val == "" || val == undefined || val == null){
        val ="0";
    }
    return val.split(",").join("");
}

//SNS (FACEBOOK)
function link_sns() 
{
	var paramUrl = location.href;
	var paramDesc = document.title;
	var url = 'http://www.facebook.com/sharer.php?u='+encodeURIComponent(paramUrl)+'&t='+encodeURIComponent(paramDesc);
	window.open(url,'sharer','toolbar=0,status=0,width=1024,height=665');
}
function sendSns(sns, title)
{
    var o;
    //var _url = encodeURIComponent(url);
    //var _txt = encodeURIComponent(txt);
    var _url = encodeURIComponent(document.location.href);
    var _txt = encodeURIComponent(title);
    if ( _txt == undefined || _txt == "" )
    {
        _txt = encodeURIComponent(document.title);
    }
    var _br  = encodeURIComponent('\r\n');
	
    switch(sns)
    {
        case 'facebook':
            o = {
                method:'popup',
                //url:'http://www.facebook.com/sharer/sharer.php?u=' + _url
                url : 'http://www.facebook.com/sharer.php?u='+_url+'&t='+_txt
            };
            break;
 
        case 'twitter':
            o = {
                method:'popup',
                url:'http://twitter.com/intent/tweet?text=' + _txt + '&url=' + _url
            };
            break;
 
        case 'me2day':
            o = {
                method:'popup',
                url:'http://me2day.net/posts/new?new_post[body]=' + _txt + _br + _url + '&new_post[tags]=louisquatorze'
            };
            break;
 
        case 'kakaotalk':
            o = {
                method:'web2app',
                param:'sendurl?msg=' + _txt + '&url=' + _url + '&type=link&apiver=2.0.1&appver=2.0&appid=www.louisclub.com&appname=' + encodeURIComponent('온라인스토어 – 루이까또즈 직영몰 모바일'),
                a_store:'itms-apps://itunes.apple.com/app/id362057947?mt=8',
                g_store:'market://details?id=com.kakao.talk',
                a_proto:'kakaolink://',
                g_proto:'scheme=kakaolink;package=com.kakao.talk'
            };
            break;
 
        case 'kakaostory':
            o = {
                method:'web2app',
                param:'posting?post=' + _txt + _br + _url + '&apiver=1.0&appver=2.0&appid=www.louisclub.com&appname=' + encodeURIComponent('온라인스토어 – 루이까또즈 직영몰 모바일'),
                a_store:'itms-apps://itunes.apple.com/app/id486244601?mt=8',
                g_store:'market://details?id=com.kakao.story',
                a_proto:'storylink://',
                g_proto:'scheme=kakaolink;package=com.kakao.story'
            };
            break;
 
        case 'band':
            o = {
                method:'web2app',
                param:'create/post?text=' + _txt + _br + _url,
                a_store:'itms-apps://itunes.apple.com/app/id542613198?mt=8',
                g_store:'market://details?id=com.nhn.android.band',
                a_proto:'bandapp://',
                g_proto:'scheme=bandapp;package=com.nhn.android.band'
            };
            break;
 
        default:
            alert('지원하지 않는 SNS입니다.');
            return false;
    }
 
    switch(o.method)
    {
        case 'popup':
            window.open(o.url);
            break;
 
        case 'web2app':
            if(navigator.userAgent.match(/android/i))
            {
                // Android
                setTimeout(function(){ location.href = 'intent://' + o.param + '#Intent;' + o.g_proto + ';end'}, 100);
            }
            else if(navigator.userAgent.match(/(iphone)|(ipod)|(ipad)/i))
            {
                // Apple
                setTimeout(function(){ location.href = o.a_store; }, 200);          
                setTimeout(function(){ location.href = o.a_proto + o.param }, 100);
            }
            else
            {
                alert('이 기능은 모바일에서만 사용할 수 있습니다.');
            }
            break;
    }
}

//url 복사
function copy_trackback(trb)
{
    var IE=(document.all)?true:false;
    
    var ie_version = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
      var ua = navigator.userAgent;
      var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
      if (re.exec(ua) != null)
    	  ie_version = parseFloat( RegExp.$1 );
    }
    else if (navigator.appName == 'Netscape')
    {
      var ua = navigator.userAgent;
      var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
      if (re.exec(ua) != null)
    	  ie_version = parseFloat( RegExp.$1 );
    }
    
    if (IE) {
        if(confirm("이 페이지의 주소를 클립보드에 복사하시겠습니까?")) 
        {
            window.clipboardData.setData("Text", trb);
            alert("주소가 복사되었습니다.\n붙여 넣을 곳에 Ctrl+V 하세요.");
        }
    } else {
    	if(ie_version == 11)
        {
            if(confirm("이 페이지의 주소를 클립보드에 복사하시겠습니까?")) 
            {
                window.clipboardData.setData("Text", trb);
                alert("주소가 복사되었습니다.\n붙여 넣을 곳에 Ctrl+V 하세요.");
            }
        }
    	else
    	{
    		temp = prompt("이 페이지의 주소입니다.", trb);
    	}
    }
}

//즐겨찾기 추가
function addFavorite()
{
    var title = "루이까또즈 직영몰";  //document.title;
    var url = "http://www.louisclub.com";  //location.href;

    //IE 버전 체크
    var ie_version = -1;
    if (navigator.appName == 'Microsoft Internet Explorer')
    {
      var ua = navigator.userAgent;
      var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
      if (re.exec(ua) != null)
    	  ie_version = parseFloat( RegExp.$1 );
    }
    else if (navigator.appName == 'Netscape')
    {
      var ua = navigator.userAgent;
      var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
      if (re.exec(ua) != null)
    	  ie_version = parseFloat( RegExp.$1 );
    }

	if(window.sidebar){// firefox
		//window.sidebar.addPanel(title, url, "");
		alert("Ctrl+D키를 누르시면 즐겨찾기에 추가하실 수 있습니다.");
	}else if(document.all){
		window.external.AddFavorite(url, title);
	}else if ( ie_version == 11 ){
        //IE 11
    	window.external.AddFavorite(url, title);
	}else{// Google Chrome && 그외
		alert("Ctrl+D키를 누르시면 즐겨찾기에 추가하실 수 있습니다.");
	}
	
}

/* 오늘본 상품 관련  쿠기 적용  스크립트 Start */
function setRecentlyProduct( itm_no ){
	var cookie_name = "LQ_PRODUCTS_"+itm_no;
    var oldCookieVal = readCookie("LQ_PRODUCTS_"+itm_no);

    if(oldCookieVal != null){
    	deleteCookie(itm_no);
    	writeCookie(cookie_name, itm_no, 7);
    }
    else
    {
    	writeCookie(cookie_name, itm_no, 7);
    }
}

function writeCookie( cookie_name, cookie_value, days ){ 
	var expires = "";	
    var date = new Date();
    
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = ";expires=" + date.toGMTString();
    
    document.cookie = cookie_name + "=" + escape( cookie_value ) + expires + ";path=/; ";
} 

function deleteCookie( itm_no )
{
	var cookie_name = "LQ_PRODUCTS_"+itm_no;
	
    //어제 날짜를 쿠키 소멸 날짜로 설정한다.
    writeCookie(cookie_name, "", -1);
}

function readCookie(name) {
    var cname = name + '=';
    var dc    = document.cookie;
    if (dc.length > 0 ) {
        begin = dc.indexOf(cname);
        if (begin != -1) {
            begin = begin + cname.length;
            end   = dc.indexOf(';', begin);

            if (end == -1) end = dc.length;
            return unescape(dc.substring(begin,end));
        }
    }
    return null;
}
/* 오늘본 상품 관련 스크립트 End */ 

/**
 * jquery 함수
 */
(function($) {
	$.fn.extend(jQuery, {
		/**
		 * check 박스 전체 선택 해제 
		 * 사용법 : $.checkBoxSelect("allSelect", "select") 전체선택 클래스, 선택 클래스 
		 */
		checkBoxSelect : function(allSelect, select, callback){
			//전체선택  이벤트 호출
			$(document).on("click", "."+allSelect, function (){
				$.checkBoxAllSelect(allSelect, select, callback);
			});
			
			//개별선택 이벤트 호출
			$(document).on("click", "."+select, function (){
				$.checkBoxUnitSelect(allSelect, select, callback);
			});
			
			//로딩
			if($("."+allSelect).is(":checked")){
				$.checkBoxAllSelect(allSelect, select, callback);
			}else{
				$.checkBoxUnitSelect(allSelect, select, callback);
			}
		},
		
		//전체 선택
		checkBoxAllSelect : function(allSelect, select, callback){
			var totalCount = $("."+select).length; 
			if( totalCount <= 0 ){
				$("."+allSelect).prop("checked", false);
				return; 
			}
			if($("."+allSelect).is(":checked")){
				$("."+select).prop("checked", true);
			}else{
				$("."+select).prop("checked", false);
			}
			
			if (typeof callback == 'function') { 
		        callback.call(this); 
		    }
		},
		
		//개별 선택
		checkBoxUnitSelect : function(allSelect, select, callback){
			var checkdLength = $("."+select + ":checked").length;
			var totalCount = $("."+select).length; 
			
			//선택된 checkbox 확인
			if(checkdLength > 0 && checkdLength == totalCount){
				$("."+allSelect).prop("checked", true);
			}else{
				$("."+allSelect).prop("checked", false);
			}
			
			if (typeof callback == 'function') { 
		        callback.call(this); 
		    }
			
		}
	});	
})(jQuery);
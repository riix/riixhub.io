
// $.mySwipe2 = null;

;(function($){
	var $catBtn = $('.btn.category'),
		$asideNav = $('#asideNav'),
		$asdTab = $asideNav.find('.tab .btn'),
		$asdClose = $('#asideTitle > a'),
		$cont = $('#container'),
		$wrap = $('#wrapper'),
		$shopNav = $('#shopNav'),
		$titleNav = $('#titleNav.has-sub'),
		$tNavSub = $titleNav.find('#titleNavSub'),
		$header = $('#header'),
		$lqCategory = $('#lqCategory'),
		$lcCategory = $('#lcCategory'),
		$contents = $('#contents'),
		$accordion = $('.accordion'),
		$listType = $('.list-type > a'),
		$layer = $('.js-layer'),
		$layerTarget = $('.js-layer-contents'),
		$carousel = $('#carousel'),
		$subCarousel = $('#subCarousel'),
		$quartorzeCategory = $("#quartorzeCategory"),
		$clubCategory = $("#clubCategory"),
		$goHome = $("#goHome");

	// 좌측 메뉴 탭 클릭시
	$asdTab.on('click', function(){
        var $this = $(this);
		var data = $this.attr('data-class');
		$asideNav.removeClass('quartorze club').addClass(data);
		if(data == "club"){
            $quartorzeCategory.hide();
            $clubCategory.show();
            $goHome.empty();
            $goHome.text("루이스클럽 메인가기  >");
            $goHome.attr("href", $this.attr("data-lcIndexUrl"));
        } else {
            $quartorzeCategory.show();
            $clubCategory.hide();
            $goHome.empty();
            $goHome.text("루이까또즈 메인가기  >");
            $goHome.attr("href", $this.attr("data-lqIndexUrl"));
        }
	});

	// 메뉴버튼 클릭시 & 메뉴 닫는 동작
	$catBtn.on('click', function(e){
		$cont.addClass('active');
		//alert("ISMOBILEAPP:"+ISMOBILEAPP);
		if ( ISMOBILEAPP )
		{
			//모바일APP
			alert("louisclub://topMenu");
		}
		else
		{
			//모바일웹
			e.stopPropagation();
			
			$('body, html').animate({scrollTop:0},500);
			$cont.addClass('open-nav');
			$asdClose.on('click', function(){
				$cont.removeClass('open-nav');
			});
			$wrap.on('click', function(){
				$cont.removeClass('open-nav');
				var timerActive = setTimeout(function(){
					$cont.removeClass('active');
				}, 300);
			});
		}
			
	});

	// 루이스까또즈 하위메뉴 동작 제어
	$lqCategory.on('click', 'li', function(e){
		if($(this).hasClass('current')){
			$(this).removeClass('current').find('ul').stop().slideUp().find('li').removeClass('current');
		} else {
            $(this).addClass('current').children('ul').stop().slideDown().find('li').removeClass('current');
                    $(this).siblings().removeClass('current').find('ul').stop().slideUp().find('li').removeClass('current');
		}
		e.stopPropagation();
	});
	// 루이스클럽 하위메뉴 동작 제어
	$lcCategory.on('click', 'li', function(e){
        if($(this).hasClass('current')){
            $(this).removeClass('current').find('ul').stop().slideUp().find('li').removeClass('current');
        } else {
        $(this).addClass('current').children('ul').stop().slideDown().find('li').removeClass('current');
            $(this).siblings().removeClass('current').find('ul').stop().slideUp().find('li').removeClass('current');
        }
        e.stopPropagation();
    });

	// 타이틀 드롭다운
	$titleNav.on('click', '.core', function(e){
		var $t = $(this).parents('#titleNav');
		$t.toggleClass('active');
		if($t.hasClass('active')){
			e.stopPropagation(); 
			$tNavSub.stop().fadeTo(500,0).slideUp({duration:500, queue:false});
		}else{
			$tNavSub.stop().slideDown(500).fadeTo({duration:500, queue:false}, 1)
			.click(function(e){e.stopPropagation();});
		}
	});

	// skyscraper top
	/*
	 * 
	$(window).scroll(function( ){
		var $scrolled = $(window).scrollTop();
		var $skyscraper =  $('#skyscraper');
		
		if ( $(window).scrollTop() >= $(document).height() - $(window).height()- $('#footer').height() ){
				$skyscraper.show().css({ bottom: $('#footer').height()+ 10 });
		} else if( $scrolled >= 100 ){
			$skyscraper.show().css({ bottom: 60 });
		} else {
			$skyscraper.hide();
		}
	});
	 */
	$(window).scroll(function( ){
		var $scrolled = $(window).scrollTop();
		var $skyscraper =  $('#skyscraper');
		
		if ( $(window).scrollTop() >= $(document).height() - $(window).height()- $('#footer').height() ){
				$skyscraper.show().css({ top: 'auto', bottom: $('#footer').height()+ 10 });
		} else if( $scrolled >= 100 ){
			$skyscraper.show().css({ top: ($(window).height() + $scrolled) - ($skyscraper.height()+10), bottom: 'auto' });
		} else {
			$skyscraper.hide();
		}
	});
	$(document).on('click', '#skyscraper a.top', function(e){ 
		e.preventDefault(); 
		$('html, body').animate({ scrollTop: 0}, 'fast');
	});

	// search
	$('#header a.search').on('click', function(e){
		e.preventDefault();
		$('#search').fadeIn('fast');
	});
	$('#search a.btn-close').on('click', function(e){
		e.preventDefault();
		$('#search').fadeOut('fast');
	});

	// 아코디언
	$accordion.on('click', 'a', function(){
		var $t = $(this).parents('.accordion'),
			$next = $t.next('.accordion-item');
		$t.toggleClass('open');
		if($t.hasClass('open')) $next.stop().show();
		else $next.stop().hide();
		return false;
	});
	$('.info3 li').click(function(){
		$(this).toggleClass('open');
	});

	// 아코디언
	$('div.section-box a.btn-toggle').on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		
		if( $this.hasClass('active') ){
			$this.parent().find('.toggle').slideDown();
			$this.children().children().html('닫기');
			$this.removeClass('active');

		}else {
			$this.parent().find('.toggle').slideUp();
			$this.children().children().html('열기');
			$this.addClass('active');
		}
	});

	// bbs toggle
	$('div.bbs-toggle a.subject').on('click', function(e){
		e.preventDefault();
		var $this = $(this),
			_answer = $this.next('div');

		$this.toggleClass('active');
		if($this.hasClass('active')) {
			_answer.stop().slideDown();
			$this.parent().siblings().children('div').hide();
		}else{
			_answer.stop().slideUp();
		}
		return false;
	});

	// 상품 리스트 타입 버튼 제어
	/* 
	// jsp에서 제어로 주석처리 - 20140618
	$listType.click(function(){
		var dataType = $(this).attr('data-type');
		$contents.removeClass('type-a type-b').addClass(dataType);
	});
	*/

	// 레이어팝업 제어
	/*
	$layer.click(function(e){
		var $t = $(this),
			$target = $($t.attr('href'));
		$t.toggleClass('on');
		if($t.hasClass('on')) {
			$layer.not($t).removeClass('on');
			$layerTarget.not($target).stop().fadeTo(500,0).slideUp({duration:500, queue:false});
			$target.stop().slideDown(500).fadeTo({duration:500, queue:false}, 1)
				.click(function(e){e.stopPropagation();});
			e.stopPropagation();
		} else $target.stop().fadeTo(500,0).slideUp({duration:500, queue:false});
		$('body, btn-close').click(function(){
			$t.removeClass('on');
			$target.stop().fadeTo(500,0).slideUp({duration:500, queue:false});
		});
		e.preventDefault();
	});*/

	if($carousel.length > 0) {
		$('.pagination').find('a').eq(0).addClass('selected');

		window.mySwipe = new Swipe(document.getElementById('carousel'), {
			continuous: true,
			stopPropagation: false,
			callback: function(index, element) {
                $('.pagination .selected').removeClass('selected');
                $('.pagination').find('a').eq(index).addClass('selected');
            },
			transitionEnd: function(index, element) {}
		});
	}
	
	

	if($subCarousel.length > 0) {
        var subItms = null;
		var $subCarouselInner = $subCarousel.find('ul'), 
			$subCarouselItem = $subCarousel.find('li'),
			subGetWidth = function(){
				var wth = $(window).width();
				if(wth<640) subItms = 2;
				else if(wth<800) subItms = 3;
				else subItms = 4;
				return subItms;
			},
			subCarouselFunc = function(){
                $subCarouselInner.carouFredSel({
					auto: false,
					width: null,
					responsive: true,
					items: subGetWidth(),
					prev: '#subCarousel .prev',
					next: '#subCarousel .next',
					pagination: false,
					onCreate : function () {
						setTimeout(function() {
							$subCarouselInner.parent().add($subCarouselInner).css({
								height : $subCarouselInner.children().first().outerHeight(true) + 'px',
								minHeight : $subCarouselInner.children().first().outerHeight(true) + 'px'
							});
						}, 1000);
					}
                });
			};
		subCarouselFunc();
		$(window).resize(function(){ subCarouselFunc(); });
	}	
	// 초기화
	$('body, html').animate({
		// scrollTop:34, 20141103
		scrollTop: 0
	},100);


	// type-b list
	$('.type-a .cartbox-item').removeAttr('style');
	if($('.type-b .cartbox-item').length > 0) {
		// 초기 height
		setTimeout(function() {
			$('.type-b .cartbox-item:nth-child(even)').each(function () {
				var $this = $(this);

				if($this.height() > $this.prev().height()) {
					$this.prev().height($this.height());
				} else if($this.height() < $this.prev().height()) {
					$this.height($this.prev().height() );
				}
			});
		}, 1000);
	}
	
})(jQuery);
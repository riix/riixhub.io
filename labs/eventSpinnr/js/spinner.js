(function($) {

    /**
     * 디버깅 툴
     * mode: screen, console
     * elmentId: 출력할 div id
     * data : { 'foo': _foo }
     */
    $.debugTool = function(options) {

        var setting = {
            active: false,
            mode: 'screen',
            elementId: 'debugTool',
            data: null
        };

        var opts = $.extend(setting, options);

        if (opts.active !== true || opts.data == null) return false;

        var _html = '',
            $el = $('#' + opts.elementId),
            $el = ($el.length) ? $el : $('<div />').attr('id', opts.elementId).appendTo($('body'));

        $.each(opts.data, function(_key, _value) {
            _html += '<p><span>' + _key + ':</span><strong>' + _value + '</strong>&nbsp;</p>';
        });

        if (opts.mode == 'console' && typeof window.console == 'object') {
            console.log($(_html).text());
        } else {
            if (!$el.hasClass('console')) {
                $el.css({
                    'position': 'fixed',
                    'right': 0,
                    'top': 0
                }).addClass('console');
            }
            $el.empty().html(_html);
        }

    };

    $.fn.casinoSpinner = function(options) {

        var setting = {
            duration: 50,
            delayStop: 400
        };

        var opts = $.extend(setting, options);

        var _timerRepeat = null,
            _timerStop = null,
            _isFirst = true,
            _delayStop = 0,
            _point = 0,
            _isAble,
            _idx,
            _isStarted,
            _check_1,
            _check_2,
            _check_3,
            _check_4,
            _STR_RESULT = [0, 0, 0, 0],
            _matchCount = 0;

        var $spinnerWrap = $('.core', this),
            $spinner = $('.num span', $spinnerWrap),
            $spinner1 = $spinner.eq(0),
            $spinner2 = $spinner.eq(1),
            $spinner3 = $spinner.eq(2),
            $spinner4 = $spinner.eq(3),
            $button = $('#buttonStart');

        var $sectionAction = $spinnerWrap.siblings('.action'),
            $sectionResult = $spinnerWrap.siblings('.result');

        /**
         * 포인트 얻어오기, ajax 응용 필요
         */
        var getPoint = function() {
            _point = 410;
        };

        /**
         * 결과추출, Ajax 에서 불러옴
         */
        var getResult = function() {

            var timer = setTimeout(function() { // ajax 콜백 구문으로 대체

                if (_idx < 1) {

                    _STR_RESULT[0] = returnRandom();
                    _STR_RESULT[1] = returnRandom();
                    _STR_RESULT[2] = returnRandom();
                    _STR_RESULT[3] = returnRandom();

                    // for test
                    // _STR_RESULT = [2, 0, 1, 5];

                    _matchCount = 0;

                    if (_STR_RESULT[0].toString().match('2')) _matchCount++;
                    if (_STR_RESULT[1].toString().match('0')) _matchCount++;
                    if (_STR_RESULT[2].toString().match('1')) _matchCount++;
                    if (_STR_RESULT[3].toString().match('5')) _matchCount++;

                }

                console.log(_STR_RESULT); // 결과 출력

                doStopCore(_idx);
                _idx++;

                // doStop();

            }, 0);
        };

        /**
         * 응모 완료시 실행
         * @return {[type]} [description]
         */
        var onEnd = function() {

            $sectionAction.hide();

            if (_matchCount > 0) {
                $sectionResult.children('div').empty().html('<p>축하합니다!</p>응모권 <span class="num">' + _matchCount + '</span>개를 획득하셨습니다.').parent().show();
            } else {
                $sectionResult.children('div').empty().html('<p>죄송합니다.</p>맞추신 숫자가 없습니다.').parent().show();
            }

            $('a', $sectionResult).one('click', function(e) {
                e.preventDefault();
                $button.css('cursor', 'pointer');
                $sectionAction.show();
                $sectionResult.hide();
            });

        };

        /**
         * 랜덤 값 반환
         * @return {num} 0~9 까지 반환
         */
        var returnRandom = function() {
            var _result = Math.floor(Math.random() * 9) + 0;
            return _result;
        };

        /**
         * 스피너 숫자값 반환
         * @param  {[type]} _el 스피너 객체
         */
        var returnIdx = function(_el) {
            var _result = parseInt(_el.attr('class').replace('spinner-', ''), 10);
            return _result;
        };

        /**
         * 스피너 그리기
         * @param  {[type]} _el   그릴 객체
         * @param  {[type]} _todo 'random'은 랜덤 값 반환
         */
        var drawSpinner = function(_el, _num) {
            var _class;
            if (_num !== undefined) {
                _class = _num;
            } else {
                _class = returnIdx(_el) + 1;
                _class = (_class >= 10) ? 0 : _class;
            }
            _el.removeClass().addClass('spinner-' + _class);
        };

        /**
         * 상황에 따라 버튼 상태  변경
         */
        var setButtonStatus = function() {

            $button.removeClass();

            if (_isAble === true) {
                if (_isStarted === true) {
                    $button.addClass('stop');
                } else {
                    $button.addClass('start');
                }
            } else {
                $button.addClass('disabled');
            }

            // 디버깅 툴 동작
            if (typeof $.debugTool !== 'undefined') {
                $.debugTool({
                    mode: 'screen',
                    data: {
                        '_point': _point,
                        '_isAble': _isAble,
                        '_isStared': _isStarted
                    }
                });
            }

        };

        /**
         * 스피닝
         * @param  {[type]} opts.duration 스피닝 속도
         */
        var doSpin = function() {

            if (_check_1 === false) drawSpinner($spinner1);
            if (_check_2 === false) drawSpinner($spinner2);
            if (_check_3 === false) drawSpinner($spinner3);
            if (_check_4 === false) drawSpinner($spinner4);

            activeCurrent(); // 화살표 출력

            _timerRepeat = setTimeout(function() {
                clearTimeout(_timerRepeat);
                doSpin();
            }, opts.duration);

        }

        /**
         * 시작
         */
        var doStart = function() {

            init();

            if (_isAble === true) {
                _point -= 200;
                _isStarted = true;
                drawSpinner($spinner1, returnRandom());
                drawSpinner($spinner2, returnRandom());
                drawSpinner($spinner3, returnRandom());
                drawSpinner($spinner4, returnRandom());
                setButtonStatus();
                doSpin(opts.duration);
            } else {
                alert('잔여 포인트가 부족합니다.');
            }
        }

        /**
         * 정지 버튼 클릭시 실행
         * @param  {[type]} _check 정지할 인덱스
         */
        var doStopCore = function(_check) {
            switch (_check) {
                case 0:
                    _check_1 = true;
                    drawSpinner($spinner1, _STR_RESULT[0]);
                    break;
                case 1:
                    _check_2 = true;
                    drawSpinner($spinner2, _STR_RESULT[1]);
                    break;
                case 2:
                    _check_3 = true;
                    drawSpinner($spinner3, _STR_RESULT[2]);
                    break;
                default:
                    _check_4 = true;
                    drawSpinner($spinner4, _STR_RESULT[3]);
                    clearTimeout(_timerRepeat);
                    onEnd();
                    init();
                    break;
            }
        }

        /**
         * 정지버튼 대상 활성화
         */
        var activeCurrent = function() {
            $spinner.parent().removeClass('active');
            if (_check_1 !== true) {
                $spinner1.parent().addClass('active');
            } else if (_check_2 !== true) {
                $spinner2.parent().addClass('active');
            } else if (_check_3 !== true) {
                $spinner3.parent().addClass('active');
            } else if (_check_4 !== true) {
                $spinner4.parent().addClass('active');
            }
        }

        /**
         * 버튼 핸들러
         */
        var handlerButton = function(e) {
            e.preventDefault();
            if (_isStarted === false) {
                if (_point >= 200) {
                    var _confirm = confirm("응모하시면 포인트가 차감됩니다. \n잔여포인트는 " + _point + " 점입니다. \n응모하시겠습니까? ");
                    if (_confirm === true) {
                        $button.off();
                        _isFirst = false;
                        doStart();
                    }
                } else {
                    alert("잔여포인트가 부족합니다. \n응모 1회당 200점의 포인트가 필요합니다. \n회원님의 잔여포인트는 " + _point + " 점입니다.");
                }

                // if (_isFirst !== false) {
                // } else {
                //     doStart();
                // }

            } else {
                getResult();
            }
        }

        /**
         * 상태 초기화
         */
        var init = function() {

            _isStarted = false;
            _isAble = false;
            _idx = 0;
            _delayStop = 0;
            _matchCount = 0;

            _STR_RESULT = [0, 0, 0, 0];

            if (_point >= 200) {
                _isAble = true;
            }

            _check_1 = false,
            _check_2 = false,
            _check_3 = false,
            _check_4 = false;

            setButtonStatus();
            $spinner.parent().removeClass('active');
            $button.off().on('click', handlerButton);

        };

        getPoint();
        init(); // 첫 실행

        /**
         * 정지버튼 클릭시 동작
         */
        // var doStop = function() {
        //     $button.css('cursor', 'default');
        //     _timerStop = setTimeout(function() {
        //         clearTimeout(_timerStop);
        //         _delayStop = opts.delayStop;
        //         if (_isStarted === true) {
        //             doStopCore(_idx);
        //             _idx++;
        //             doStop();
        //         }
        //     }, _delayStop);
        // };

        return this;

    };

})(jQuery);


$(function() {

    $('#casino').casinoSpinner();

});
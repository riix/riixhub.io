/*
 * Author: riix@epasscni.com
 */

$(function() {

    var _click = (document.ontouchstart !== null) ? 'click' : 'touchstart';

    /* intro slide ====================================================== */

	window.mySwipe = new Swipe(document.getElementById('intro'), {
		startSlide: 1,
		continuous: false,
		stopPropagation: false,
		callback: function(index, element) {
            $('#intro .pager a').css({ opacity : 1 });
            $('#intro .pagination .selected').removeClass('selected');
            $('#intro .pagination').find('a').eq(index).addClass('selected');
        },
		transitionEnd: function(index, element) {
            if(index === 0){
                $('#intro .prev').css({ opacity : 0 });
            } else if(index == 2) {
                $('#intro .next').css({ opacity : 0 });
			}
		}
	});
	

    $('#intro').addClass('active');
    
    // Smart resize
    $(window).on('resize', function() {
        mySwiper.resizeFix(true);
    });
    
    // Landscape
    $(window).on("orientationchange",function(){
        if(window.orientation === 0) { // Portrait
            $('html, body').removeAttr('style');
            mySwiper.resizeFix(true);
        } else { // Landscape
            $('html, body').height($(window).height()*2);
            mySwiper.resizeFix(true);
        }
    });
});
/*
 * Author: riix@epasscni.com
 */
$.mySwipe = null;

(function(window, document, ua) {
	
	$.ua = ua;
	
    $.thro = {};

    var throNames = {};
    
    /*
     * HEIGHT : 높이 조정 지연
     * ORIENTATION : 화면 회전 반응
     * 
     */
    $.each(["HEIGHT", "ORIENTATION", "A", "B", "B1", "C", "D"], function(i, name) {
        throNames[name] = (function() {
            var _timerThrottle;
            return function(fn, delay) {
                clearTimeout(_timerThrottle);
                _timerThrottle = setTimeout(fn, delay);
            };
        })();
    });

    $.each(throNames, function(name, arg) {
        $.thro[name] = arg;
    });

    $.fn.imagesLoaded = function(callback) {
        var el = this.filter('img'),
            len = el.length,
            totalLen = 0,
            blank = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        el.one('load.imgloaded', function() {
            totalLen = --len;
            if (totalLen <= 0 && this.src !== blank) {
                el.off('load.imgloaded');
                callback.call(el, this);
            }
        });
        el.each(function() {
            if (this.complete || this.complete === undefined) {
                var src = this.src; // + '?time = ' + new Date().getTime();
                this.src = blank;
                this.src = src;
            }
        });
        return this;
    };

})(window, document, navigator.userAgent || navigator.vendor || window.opera);

$(function() {

	var isAndroid = (/android/i.exec($.ua)) ? true : false;

	var delayHeight = 300;
	var delayInitSlides = 700;
	var delayOrientationChangeHeight = 1000;
	
	/* 안드로이드 일때 */
	
	if(isAndroid){
		delayInitSlides = 700;
	}
	
    /* vars. ====================================================== */

    var _click = (document.ontouchstart !== null) ? 'click' : 'touchstart'; // 이벤트

    var $gnbItems = $('#nav li'),
        $underbar = $('#navUnderBar'),
        $swipeContainer = $('#shop'),
        $swipeSlides = $swipeContainer.find('.swiper-slide'),
        $slideA = $('#slidePromotion ul'),
        $slideB = $('#slideB'),
        $slideBItems = $('.section', $slideB),
        $slideC = $('#slideC'),
        $slideCUl = $('ul', $slideC),
        $slideD = $('#slideD ul');

    /* func. ====================================================== */

    // 네비게이션 활성화
    var activeNav = function(idx) {

        var $el = $gnbItems.eq(idx).find('span');

        var _left = $el.position().left,
            _width = parseInt($el.innerWidth(), 10);

        $underbar.css({
            'opacity': '1',
            'width': _width,
            '-webkit-transform': 'translate(' + _left + 'px, 0px)',
            '-moz-transform': 'translate(' + _left + 'px, 0px)',
            '-o-transform': 'translate(' + _left + 'px, 0px)',
            'transform': 'translate(' + _left + 'px, 0px)'
        });
    };

    // 높이 조정
    var changeHeight = function(_index, _instant) {

        var $activeSlide = $swipeSlides.eq(_index);
        var _activeHeight = null;

        $swipeSlides.removeClass('first').css('height', '');

        if (_instant !== true) {
            $.thro.HEIGHT(function() {
                _activeHeight = $activeSlide.find('.swiper-inner').height();
                $swipeContainer.css('height', _activeHeight);
            }, delayHeight);
        } else {
            _activeHeight = $activeSlide.find('.swiper-inner').height();
            $swipeContainer.css('height', _activeHeight);
        }
    };

    /* slide A ====================================================== */

    var initSlideA = function() {
        $slideA.carouFredSel({
    		auto    : { 
    			timeoutDuration: 3000
    		},
            responsive: true,
            items: {
                width: '640',
                height: '555',
                visible: 1,
                start: 'random'
            },
            prev: '#slidePromotion .prev',
            next: '#slidePromotion .next',
            pagination: '#slidePromotion .pagination'
        }, { 
            transition: true
        });
    };
    
    /* slide B ====================================================== */

    var initSlideB = function() {

        $slideBItems.width($('.slide-b-inner').width());

        $slideB.carouFredSel({
            auto: false,
            items: {
                visible: 1
            },
            height: 'auto',
            prev: '.slide-b-wrapper .prev',
            next: '.slide-b-wrapper .next',
            pagination: '.slide-b-wrapper .pagination'
        }, { 
            transition: true
        });
    };

    /* slide C ====================================================== */

    var initSlideC = function() {

        $slideCUl.width($('.slide-c-inner').width());

        $slideC.addClass('active');

        $slideC.carouFredSel({
            auto: false,
            items: {
                visible: 1
            },
            height: 'auto',
            prev: '.slide-c-wrapper .prev',
            next: '.slide-c-wrapper .next',
            pagination: '.slide-c-wrapper .pagination'
        }, { 
            transition: true
        });
    };

    if ($slideA.length) {
        $('img', $slideA).imagesLoaded(function() {
            initSlideA();
        });
        $('img', $slideB).imagesLoaded(function() {
            initSlideB();
        });
        $('img', $slideC).imagesLoaded(function() {
            initSlideC();
        });
    }

    /* slide D ====================================================== */

    var initSlideD = function() {
        $slideD.carouFredSel({
    		auto    : { 
    			timeoutDuration: 3000
    		},
            responsive: true,
            height: 'auto',
            items: {
                visible: 1,
                start: 'random'
            },
            prev: '#slideD .prev',
            next: '#slideD .next',
            pagination: '#slideD .pagination'
        }, { 
            transition: true
        });
    };

    if ($slideD.length) {
        $('img', $slideD).imagesLoaded(function() {
            initSlideD();
        });
    }

    /* init ====================================================== */

    activeNav(0);
    changeHeight(0, false);

    /* wrap slider ====================================================== */

    var sliderEl = document.getElementById('shop');

    $.mySwipe = Swipe(sliderEl, {
        continuous: false,
        stopPropagation: true,
        callback: function(index, element) {
            activeNav(index);
        },
        transitionEnd: function(index, element) {
            changeHeight(index, true);
        }
    });

    /* when orientationchange ====================================================== */

    $(window).on('orientationchange', function(e) {

        /* slideA */
        if ($slideA.length) {

            $slideA.trigger('destroy');
            $slideB.trigger('destroy');
            $slideC.removeClass('active');
            $slideC.trigger('destroy');
            $slideBItems.width('auto');
            $slideCUl.width('auto');
            $.thro.A(function(){
                initSlideA();
                initSlideB();
                initSlideC();
            }, delayInitSlides);
        }

        /* slideD */
        if ($slideD.length) {

            $slideD.trigger('destroy');

            $.thro.D(function() {
                initSlideD();
            }, delayInitSlides);
        }

        /* wrap slider */
        $.thro.ORIENTATION(function() {
            var _index = $.mySwipe.getPos();
            changeHeight(_index, true);
            activeNav(_index);
        }, delayOrientationChangeHeight);
        
    });

    /* etc ====================================================== */

    $swipeContainer.addClass('active');

    /* click 유지할 것 */
    $('a', $gnbItems).on('click', function(e) {
        e.preventDefault();
        $.mySwipe.slide($(this).parent().index(), 300);
    });

});
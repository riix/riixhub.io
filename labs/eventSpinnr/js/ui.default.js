
// Dom Cache for Browser Performance
if(typeof($window) == 'undefined') var $window = $($(window)[0]);
if(typeof($document) == 'undefined') var $document = $($(document.body)[0]);

(function($) {

	// jQuery('div').eachQ(function(i){ this.attr('id', 'd' + i).css('color', 'red'); });
	$.fn.eachQ = (function(){
		var jq = $([1]);
		return function(c) {
			var i = -1, el, len = this.length;
			try {
				while (
					++i < len &&
					(el = jq[0] = this[i]) &&
					c.call(jq, i, el) !== false
				);
			} catch(e){
				delete jq[0];
				throw e;
			}
			delete jq[0];
			return this;
		};
	}());

	$.fn.extend({

		dataVal: function(el, _default){ // 커스텀 데이터 값 얻기, 없을 땐 두번째 값으로 리턴 var a = dataVal('fx','fadeIn') || var a = ( $this.data('item') ) ? $this.data('item') : 1
			var _value = this.data(el);
			if (_value === undefined) _value = _default;
			return _value;
		},

        active: function() {
			return this.addClass('active');
        },

        deactive: function() {
            return this.removeClass('active');
        },

		clicked: function() {
			return this.bind('click', function() {
				$(this).addClass('clicked');
			});
		},

		unclicked: function() {
			return this.removeClass('clicked').unbind('click');
		},

		oneClick: function() {
			return this.bind('click', function() {
				$(this).addClass('clicked').attr('disabled','true');
			}).removeClass('clicked').removeAttr('disabled');
		},
		
		jsRowSpan: function() { // td[rowspan] polyfills.
			$(this).eachQ(function(){
				var $this = $(this);
				$this.find('td[rowspan]').addClass('rowspan');
			});
		},

		center: function(_position){ // 화면 중앙에 위치 시키기
			var $this = $(this);
			var _marginTop = Math.abs((($window.height() - $this.outerHeight()) / 2)),
				_marginLeft = Math.abs((($window.width() - $this.outerWidth()) / 2));

			$this.css('visibility','visible');

			if (_position == 'fixed') {
				$this.css({
					'position': 'fixed',
					'top': _marginTop - 50,
					'left': _marginLeft,
					'opacity': 0
				}).animate({
					'top': _marginTop,
					'opacity': 1
				});
			} else {
				$this.css({
					'position': 'absolute',
					'top': _marginTop + $window.scrollTop() - 50,
					'left': _marginLeft,
					'opacity': 0
				}).animate({
					'top': _marginTop + $window.scrollTop(),
					'opacity': 1
				});
			}

			$this.attr('tabindex', 0).show().focus();

		}, // center

		jsSpinner: function(){ // Spinner 만들기

			$(this).attr('autocomplete','off').eachQ(function(){
				var _strMinus = '<a href="javascript:;" class="ir spinner minus"><em>값 줄이기</em></a>',
					_strPlus = '<a href="javascript:;" class="ir spinner plus"><em>값 늘이기</em></a>';

				var $this = $(this),
					$btnMinus = $this.siblings('.minus'),
					$btnPlus = $this.siblings('.plus');

				var _min = parseFloat($this.attr('min')),
					_max = parseFloat($this.attr('max')),
					_disabledClass = 'spinner-disabled';

				if(!$btnMinus.length){ $btnMinus = $(_strMinus); $this.before($btnMinus); }
				if(!$btnPlus.length){ $btnPlus = $(_strPlus); $this.after($btnPlus); }
				if( jQuery.browser.safari ) {
					$this.attr('type','text');
				}

				$this.on('blur', function(e){
					var _val = $this.val();

					$this.siblings('a.spinner').removeClass(_disabledClass);

					if(_val >= _max) {
						$btnPlus.addClass(_disabledClass);
					} else if(_val <= _min) {
						$btnMinus.addClass(_disabledClass);
					}

				});

				$btnMinus.on('click', function(e){
					e.preventDefault();
					_result = parseFloat($this.val())-1;
					if (_result >= _min) $this.val(_result);
					$this.trigger('blur');
					
					if($this.attr("onchange") !== undefined)
					{
						setTimeout($this.attr("onchange"),0);	
					}
				});

				$btnPlus.on('click', function(e){
					e.preventDefault();
					_result = parseFloat($this.val())+1;
					if (_result <= _max) $this.val(_result);
					$this.trigger('blur');
					
					if($this.attr("onchange") !== undefined)
					{
						setTimeout($this.attr("onchange"),0);	
					}
				});

				$this.trigger('blur');

			});

		}, // jsSpinner

		jsTabContents: function(){ // 컨텐츠형 탭

			$(this).eachQ(function(idx){

				var $this = $(this);

				var $wrapper = $(this).find('ul.core').eq(0),
					$tab = $wrapper.find('a.tab'),
					$li = $wrapper.find('li'),
					$contents = $wrapper.find('.contents');

				var _height = 0,
					_isHover = false,
					_isPlay = true,
					_isAutoplay = $(this).dataVal('autoplay', false);

				$tab.eachQ(function(){
					$(this).css('top',_height);
					// _height += $(this).outerHeight();
				});

				$tab.on('click focusin', function(e){
					e.preventDefault();
					var $this = $(this);
					$tab.find('span.current').remove();
					$this.parents('li').active().find('a').eq(0).append('<span class="semantic current">&nbsp;-&nbsp;현재 위치</span>');
					$this.parents('li').find('div:eq(0)').fadeIn('fast');
					$this.parents('li').siblings().deactive().find('.contents').hide();
				}).eq(0).trigger('click','auto');

				if(_isAutoplay){
					var $playBtn = $('<a href="javascript:;" class=ir "pause"><em>멈춤</em></a>');
					$playBtn.insertBefore($wrapper).wrap('<div class="contents-pager" />');
					$playBtn.on('click', function(e){
						if(_isPlay === true){
							_isPlay = false;
							$playBtn.find('span').eq(0).text('재생');
						} else {
							_isPlay = true;
							$playBtn.find('span').eq(0).text('멈춤');
						}
					});
					var tabTimer = setTimeout(function tabAutoplay() {
                        var $currentLi = $wrapper.find('li.active').eq(0);
                        if($li.length-1 == $currentLi.index() && _isHover !== true && _isPlay === true){
                            $li.eq(0).find('a').eq(0).trigger('click','auto');
						} else if (_isHover !== true && _isPlay === true) {
                            $currentLi.next().find('a').eq(0).trigger('click','auto');
						}
                        setTimeout(tabAutoplay, 2000);
					}, 2000);
				}
			});

		}, // jsTabContents

		jsJumpmenuToggle: function(){ // 레이어 팝업
			$(this).eachQ(function(){
				var $this = $(this);
				var $button = $this.siblings('.button');
				$button.hide();
				$this.on('focus', function(){
					$button.hide();
					$this.next().css('display','inline-block');
				});
			});
		}, // jsJumpmenuToggle

		jsPopup: function(){
			$(this).on('click', function(e){
				e.preventDefault();
				var $this = $(this);
				var _href = $this.attr('href'),
					_width = $this.data('width') ? $this.data('width') : 400,
					_height = $this.data('height') ? $this.data('height') : 300,
					_scroll = ($this.data('scroll') === true) ? 'yes' : 'no';
				popup(_href, _width, _height, _scroll);
			});
		}, // jsPopup

		jsLayer: function(){ // 소규모 레이어 팝업
			$(this).on('click', function(e){
				e.preventDefault();
				var $this = $(this);
				var _href = $this.attr('href'),
					_focus = $this;
					_width = $this.dataVal('width', '600'),
					_height = $this.dataVal('height', '400'),
					_top = $this.offset().top + $this.outerHeight(),
					_left = $this.offset().left,
					_scroll = $this.data('scroll');
				layer(_href, _focus, _width, _top, _left, _height, _scroll);
			});
		} // jsLayer

	}); // extend

})(jQuery);

// overlay
var _isOverlay = false;
var overlay  = function(todo){
	var $overlay = $('<div class="overlay" aria-hidden="true"></div>');
	var $el = $('div.overlay');
	switch(todo){
		case 'remove':
			$el.animate({ opacity: 0 }, 500, function(){
				$el.remove();
				_isOverlay = false;
				return 'off';
			});
			break;
		default:
			// overlay 생성
			var _overlayHeight = Math.max($window.height(), $document.height());
			if(_isOverlay !== true && $el.length <= 0) {
				_isOverlay = true;
				$overlay.css({
					'position': 'absolute',
					'z-index': '110',
					'top': '0',
					'left': '0',
					'right': '0',
					'bottom': '0',
					'width': '100%',
					'height': _overlayHeight,
					'opacity': '0',
					'background': '#000',
					'cursor': 'default'
				}).appendTo($(document.body)).animate({
					opacity: 0.5
				}, 500, function(){
					return 'on';
				});
			}
			break;
	}
};

// indicator
var waiting = function(todo){
	var $el = $('div.waiting');
    var $message = null;
    var tt = null;
	switch(todo){
		case 'off':
			overlay('remove');
			$el.remove();
			break;
		case 'inquiry':
			$message = $('<div class="waiting"><div class="inner"><p>잠시만 기다려주십시요.<br />요청하신 데이터를 처리중 입니다.</p><span class="animation"></span></span></div>');
			overlay('append');
			if($el.length <= 0){
				tt = setTimeout(function(){
					$message.appendTo($document).show();
					clearTimeout(tt);
				}, 200);
			}
			break;
		default:
			$message = $('<div class="waiting"><div class="inner"><p>잠시만 기다려 주십시오.<br />데이터를 불러오는 중입니다.</p><span class="animation"></span></span></div>');
			overlay('append');
			if($el.length <= 0){
				tt = setTimeout(function(){
					$message.appendTo($document).show();
					clearTimeout(tt);
				}, 200);
			}
			break;
	}
};

// 팝업 관련 함수
var popup = function(_href, _width, _height, _scroll, _id){
	if (!_scroll) _scroll = 'no';
	var _left = parseInt((screen.width - _width) / 2, 10),
		_top = parseInt((screen.height - _height) / 2, 10) - 100;
	var popup =  window.open(_href, 'popup'+_id, 'top='+ _top +', left='+ _left +', width='+ _width +', height='+ _height +', scrollbars='+ _scroll +', toolbar=no, menubar=no, location=no, resizable=true, status=yes');
	popup.focus();
};

// layer
var layer = function(_href, _focus, _width, _top, _left, _height, _scroll){
	var $layer = $(_href).eq(0),
		$focus = _focus;
	
	if (_width >= $window.width() ){
		$layer.filter(':hidden').css({
			'width': _width,
			'height': _height,
			marginLeft : 0,
			left: '0px'
		}).show('fast').attr('tabindex','0').focus();

	} else {
		$layer.filter(':hidden').css({
			'width': _width,
			'height': _height,
			marginLeft : -_width/2
		}).show('fast').attr('tabindex','0').focus();
	
	}
	$layer.append('<a href="javascript:;" class="ir btn-close"><em>닫기</em></a>');

	if (_scroll === true ){
		$layer.find('div.layer-inner').css({ height:  _height - 30 });
	}

	$layer.find('a.btn-close').one('click', function(e){
		e.preventDefault();
		$(this).parent().fadeOut('fast');
		$focus.focus();
	});
	
	/** 
	 *  레이어 팝업 호출 후 함수 호출
	 *  _href 변수 값이 #searchOrder면
	 *  searchOrder 함수를 호출한다.
	 */
	if(_href !== ''){
        if(_href.indexOf('#') > -1){
            var functionNm = _href.replace('#', '');
            functionNm = eval(functionNm);
            if(functionNm.call() !== undefined){
                functionNm.call();
            }
        }
	}
};

$(function(){
	$('a.js-layer', document).jsLayer(); // Layer - <a href="#layerBenefit" class="js-layer" data-width="300" data-height="250" data-scroll="false">
	$('.js-popup', document).jsPopup(); // popup layer
	$('div.js-tab-contents', document).jsTabContents(); // TabContents
	$('input.js-spinner', document).jsSpinner(); // Spinner
});
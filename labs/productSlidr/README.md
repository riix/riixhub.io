# productSlidr

Product Slider for Shopping Mall Main / Category Main Page.

## demo
- [static html format](http://riix.github.io/labs/productSlidr/index.html)
- [json infinite format](http://riix.github.io/labs/productSlidr/json.infinite.html)
- [json no infinite format](http://riix.github.io/labs/productSlidr/json.noinfinite.html)
- [json notab format](http://riix.github.io/labs/productSlidr/json.notab.html)


## html
### outter
```html
<div class="section-slider">

    <h3>제휴몰 BEST ITEM</h3>

    <div id="slider" class="slidr js-prductSlidr">
 
        <a href="#!" class="nav prev"><img src="./img/module/nav_prev.png" alt="이전" /></a>
        <a href="#!" class="nav next"><img src="./img/module/nav_next.png" alt="다음" /></a>

        <div class="counter"></div>
        <div class="pager"></div>

    </div>

</div>
```

## html
### inner
```html
<div class="itemwrap"><!-- itemwrap -->
    <ul class="tab">
        <li class="item">
            <div class="thumb">
                <a href="#!"><img src="http://riix.github.io/cdn/img/thumb/125x125_01.jpg" alt="" /></a>
            </div>
            <div class="desc">
                <p class="brand">001</p>
                <p class="name">Prospect</p>
                <p class="price">
                    <span class="before">590,000 원</span>
                    <span class="after">370,000 원</span>
                </p>                       
            </div>
        </li>
        <li class="item">
            <div class="thumb">
                <a href="#!"><img src="http://riix.github.io/cdn/img/thumb/125x125_02.jpg" alt="" /></a>
            </div>
            <div class="desc">
                <p class="brand">002</p>
                <p class="name">Prospect</p>
                <p class="price">
                    <span class="before">590,000 원</span>
                    <span class="after">370,000 원</span>
                </p>                       
            </div>
        </li>
        ...
    </ul>
</div>
```

## javascript

### default format
```javascript
// init slider
$('#slider').productSlidr({
    json: _jsonItems,
    formatter: _jsonFilter, // formatter function
    infinite: false,
    autoPlay: true,
    visibleLength: 4,
    sliderHeight: '275px',
    onComplete: function(base){
        var $button = $('#sliderPause');
        $button.on('click', function(){
            $button.toggleClass('pause');
            getPause(base, $button);
        });
        getPause(base, $button);
    }
});
```

### options
```javascript
json: null,
formatter: function($item, _idx) {}
infinite: true,
imagesLoaded: true,
randomActive: true,
autoPlay: false,
pauseOnHover: true,
resizable: false,
vertical: false,
delay: 3500, // auto play delay
duration: 600, // animation duration
tabDelay: 0,
visibleLength: 5,
slideLength: 0,
wrapEl: '.itemwrap',
tabEl: '.subject',
tabsPosition: 'right',
counter: '.counter',
navNext: '.next',
navPrev: '.prev',
pager: '.pager',
sliderHeight: 'auto',
indicatorURL: './img/module/ajax-loader.gif',
onSlideStart: null,
onSlideEnd: null,
onComplete: null
```

### json formatter
```javascript
var _jsonFilter = function($item, _idx) {
    var _html = '';
    _html += '<li class="item">';
    _html += '<div class="thumb">';
    _html += '<a href="' + $item.src + '"><img src="' + $item.thumb + '" alt="" /></a>';
    _html += '</div>';
    _html += '<div class="desc">';
    _html += '<p class="brand">' + $item.brand + '</p>';
    _html += '<p class="name">' + $item.name + '</p>';
    _html += '<p class="price">';
    _html += '<span class="before">' + $item.priceBefore + ' 원</span>';
    _html += '<span class="after">' + $item.priceAfter + ' 원</span>';
    _html += '</p>';
    _html += '</div>';
    _html += '</li>';
    return _html;
}
```

### json format
```javascript
var _jsonItems = {
    "group": [{
        "subject": "롯데닷컴",
        "items": [{
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }]
    }, {
        "subject": "AK MALL",
        "items": [{
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }]
    }, {
        "subject": "다이소몰",
        "items": [{
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }]
    }, {
        "subject": "알라딘",
        "items": [{
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }, {
            "thumb": "http://lorempixel.com/200/200/",
            "src": "http://google.com",
            "brand": "BLACK YAK",
            "name": "뉴트로지나 에이펜더 샥크",
            "priceBefore": "590,000",
            "priceAfter": "290,000"
        }]
    }]
};
```
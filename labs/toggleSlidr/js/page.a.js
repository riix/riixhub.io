$(function() {

    var __animation = true;

    $('#issueSlide').toggleSlidr({
        pager: '#issuePager',
        css3animation: __animation,
        duration: 300,
        delay: 5000,
        autoPlay: false
    });

    $('#newArrivalSlide').toggleSlidr({
        nav: '#newArrivalNav',
        css3animation: __animation,
        duration: 500,
        delay: 5200,
        autoPlay: false
    });

    $('#catalog').toggleSlidr({
        nav: '#catalogNav',
        css3animation: __animation,
        duration: 250,
        delay: 3000,
        autoPlay: false
    });

});
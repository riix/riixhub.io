# UI Dev Labs
Product Viewer for Shopping Mall Product Detail Page.

### DEMO
- [fxSlidr](http://riix.github.io/labs/fxSlidr/)
- [underScor](http://riix.github.io/labs/underScor/)
- [productViewr](http://riix.github.io/labs/productViewr/)

### Log
- 0.1 최초 작성
- 0.4 Kenburns 옵션 추가, 크기 지정 옵션 추가
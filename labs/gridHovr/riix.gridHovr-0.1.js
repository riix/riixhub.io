(function($, window, document, ua) {

    'use strict';

    $.fn.gridHover = function(_options) {

        var base = [];

        var _setting = {
            items: '.col',
            speed: 400,
            easing: 'easeOutExpo',
            hoverClass: '.fill'
        };

        var opts = $.extend(_setting, _options);

        base.$el = $(this);
        base.$items = $(opts.items, base.$el);

        var returnDirection = function(e, obj, w, h) { // return direction
            var x = (e.pageX - obj.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
                y = (e.pageY - obj.offset().top - (h / 2)) * (h > w ? (w / h) : 1),
                _result = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;
            return _result;
        };

        var core = function(e, _el, _todo) {

            var $hoverEl = _el.find(opts.hoverClass).eq(0);
            var _width = _el.width(),
                _height = _el.height(),
                _dir = returnDirection(e, _el, _width, _height);
            var show = function(_el, _top, _left) { // show from
                    _el.css({
                        "opacity": 0,
                        "top": _top,
                        "left": _left
                    });
                },
                hide = function(_el, _top, _left) { // hide to
                    _el.stop().animate({
                        "opacity": 0,
                        "top": _top,
                        "left": _left
                    }, opts.speed, opts.easing, function() {
                        _el.hide();
                    });
                }
            var _core = (_todo === 'show') ? show : hide;

            if (_dir === 0) { // upper
                _core($hoverEl, -_height, 0);
            } else if (_dir == 1) { // right
                _core($hoverEl, 0, _width);
            } else if (_dir == 2) { // downer
                _core($hoverEl, _height, 0);
            } else { // left
                _core($hoverEl, 0, -_width);
            }

            if (_todo === 'show') { // show to
                $hoverEl.stop().show().animate({
                    opacity: 1,
                    top: 0,
                    left: 0
                }, opts.speed, opts.easing);
            }
        }

        var setHandler = function() { // set handler
            base.$items.on({
                mouseenter: function(e) {
                    core(e, $(this), 'show');
                },
                mouseleave: function(e) {
                    core(e, $(this), 'hide');
                }
            });
        };

        var reset = function() { // reset
            base.$items.css({
                'position': 'relative',
                'overflow': 'hidden'
            }).find(opts.hoverClass).css({
                'display': 'none',
                'position': 'absolute',
                'left': 0,
                'top': 0,
                'width': '100%',
                'height': '100%',
                'opacity': 0
            });
        }

        var init = function() { // init
            reset();
            setHandler();
        };

        return this.each(function() {
            init();
        });

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
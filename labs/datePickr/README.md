# datePickr

- a web accessible date picker

## demo
- [sample](http://riix.github.io/labs/datePickr/index.html)

## HTML
```html
<p>
    <input type="date" data-min="2015-02-10" data-max="2015-04-20" />
    <a href="#!" class="js-datepicker">날짜선택</a>
</p>

<p>
    <input type="text" data-min="2015-02-10" data-max="2015-04-20" />
    <a href="#!" class="js-datepicker">날짜선택</a>
</p>

<p>
    <input type="text" data-min="2015-02-10" data-max="2015-04-14" />
    <a href="#!" class="js-datepicker">시작날짜</a>
    <input type="text" data-min="2015-03-15" data-max="2015-04-20" />
    <a href="#!" class="js-datepicker">마감날짜</a>
</p>

<p>
    <input type="text" data-min="2015-02-10" data-max="2015-04-20" />
    <a href="#!" class="js-datepicker">날짜선택</a>
</p>
```


## Javascript
```javascript
$(function() {
    $('.js-datepicker').datePickr();
});
```

## Settings

```javascript
var setting = {
    shortDate: false, // 표기 방법
    appendAfter: true // 달력 객체 삽입 위치
};
```
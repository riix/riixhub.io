# fxSlidr

CSS3 FX Slider with Typho Transitions

## demo
- [basic format](http://riix.github.io/labs/fxSlidr/default.classic.html)

## basic format

### html 
```html

```

### javascript
```javascript

```

## options
```javascript
var setting = {

};
```

## fx
```javascript
var _arrFx = [
    'fxCrossFade',
    'fxSoftScale', // Full Width
    'fxPressAway',
    'fxSideSwing',
    'fxFortuneWheel',
    'fxSwipe',
    'fxPushReveal',
    'fxSnapIn',
    'fxLetMeIn',
    'fxStickIt',
    'fxArchiveMe',
    'fxVGrowth',
    'fxSlideBehind',
    'fxSoftPulse',
    'fxEarthquake',
    'fxCliffDiving',
    'fxSlideForward', // Transparent
    'fxTableDrop',
    'fxSlideIt',
    'fxBottleKick',
    'fxShelf',
    'fxCorner', // Small Component
    'fxVScale',
    'fxFall',
    'fxFPulse',
    'fxRPulse',
    'fxHearbeat',
    'fxCoverflow',
    'fxRotateSoftly',
    'fxDeal',
    'fxFerris',
    'fxShinkansen',
    'fxSnake',
    'fxShuffle',
    'fxPhotoBrowse',
    'fxSlideBehind',
    'fxVacuum',
    'fxHurl'
];
```

### plugins
- [imagesloaded.3.1.8.pkgd.min.js](https://github.com/desandro/imagesloaded)
- [jquery.finger.0.1.2.min.js](https://github.com/ngryman/jquery.finger)
- [jquery.mousewheel.3.1.12.min.js](https://github.com/jquery/jquery-mousewheel)
- [moustrap.1.4.6.min.js](https://github.com/ccampbell/mousetrap)
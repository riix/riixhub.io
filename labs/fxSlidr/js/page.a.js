$(function() {

    var __animation = true;

    $('#jumbotronC').fxSlider({
        fx: 'random',
        autoPlay: false,
        prev: '#jumbotronC a.prev',
        next: '#jumbotronC a.next',
        pager: '#jumbotronPagerC',
        css3animation: __animation,
        imagesLoaded: true,
        duration: 1000,
        delay: 3500,
        reverse: false,
        typho: {
            el: '.typho',
            duration: '.7s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        }
    });


    $('#jumbotron').fxSlider({
        fx: 'fxCrossFade',
        autoPlay: false,
        prev: '#jumbotron a.prev',
        next: '#jumbotron a.next',
        pager: '#jumbotronPager',
        css3animation: __animation,
        duration: 1000,
        delay: 3500,
        reverse: true,
        typho: {
            el: '.typho',
            duration: '.7s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        }
    });

    $('#jumbotronB').fxSlider({
        fx: 'random',
        autoPlay: false,
        prev: '#jumbotronB a.prev',
        next: '#jumbotronB a.next',
        pager: '#jumbotronPagerB',
        css3animation: __animation,
        duration: 1000,
        delay: 3500,
        typho: {
            el: 'img',
            duration: '.7s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        }
    });


});

// fxPressAway,
// fxSideSwing,
// fxFortuneWheel,
// fxSwipe,
// fxPushReveal,
// fxSnapIn,
// fxLetMeIn,
// fxStickIt,
// fxArchiveMe,
// fxVGrowth,
// fxSlideBehind,
// fxSoftPulse,
// fxEarthquake,
// fxCliffDiving
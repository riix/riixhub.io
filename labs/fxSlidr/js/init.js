// config
// lksls
require.config({
    baseUrl: './js',
    paths: {
        'jquery': 'vendor/jquery-1.11.2.min',
        'modernizr': 'vendor/modernizr-2.6.2-respond-1.1.0.min',
        'imagesLoaded': 'plugins/imagesloaded.3.1.8.pkgd.min',
        'stellar': 'plugins/jquery.stellar.0.6.2.min',
        'finger': 'plugins/jquery.finger.0.1.2.min',
        'mousetrap': 'plugins/moustrap.1.4.6.min',
        'mousewheel': 'plugins/jquery.mousewheel.3.1.12.min',
        'riixBase': 'riix.base-0.2',
        'riixJsonModal': 'riix.jsonModal-0.1',
        'riixFxSlider': 'riix.fxSlider-0.3'
    },
    shim: {
        'modernizr': {
            exports: 'Modernizr' // modernizr global var.
        }
    },
    urlArgs: 'timestamp=' + (new Date()).getTime() // cache buster
});

// init
require([

    'jquery',
    'modernizr'

], function($, modernizr) {

    'use strict';

    var _bodyClass = $('body').attr('class');

    require([

        'imagesLoaded',
        'finger',
        'mousetrap',
        'riixBase'

    ], function() {

        $.appendPreloadImg();

        $('html').imagesLoaded(function() {
            $('img.preload').remove(); // preload 이미지 제거
        });

        if (_bodyClass.match('default')) {

            require([ // view

                'riixFxSlider'

            ], function() {

                require([ // a

                    'page.a'

                ]);

            });

        } else if (_bodyClass.match('fullscreen')) {

            require([ // view

                'riixFxSlider'

            ], function() {

                require([ // a

                    'page.fullscreen'

                ], function() {
                    $.initFullscreen();
                });

            });

        };

    });

});
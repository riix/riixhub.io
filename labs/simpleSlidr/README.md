# simpleSlidr

CSS3 FX Slider with Typho Transitions

## demo
- [horizon](http://riix.github.io/labs/simpleSlidr/horizon.html)
- [vertical](http://riix.github.io/labs/simpleSlidr/vertical.html)

## basic format

### html 
```html
<div id="jumbotron" class="container">
    <div class="badge-holder">
        <span class="badge"><img src="./img/common/badge_01.gif" alt="" /></span>
    </div>
    <div class="mask">
        <div class="inner">
            <div class="left"></div>
            <div class="right"></div>
        </div>
    </div>
    <div id="slider" class="itemwrap">
        <div class="item">
            <div class="slider-copy">
                <p class="nth-child-1"><img src="./img/common/txt_a01.png" alt="" /></p>
                <p class="nth-child-1"><img src="./img/common/txt_a02.png" alt="" /></p>
                <p class="nth-child-1"><img src="./img/common/txt_a03.png" alt="" /></p>
                <p class="nth-child-1"><a href="a_sub.html"><img src="./img/common/txt_04.png" alt="" /></a></p>
            </div>
            <img src="./img/common/pic_a01.jpg" alt="" />
        </div>
        ...
        <div class="item">
            <div class="slider-copy">
                <p class="nth-child-1"><img src="./img/common/txt_01.png" alt="" /></p>
                <p class="nth-child-1"><img src="./img/common/txt_02.png" alt="" /></p>
                <p class="nth-child-1"><img src="./img/common/txt_03.png" alt="" /></p>
                <p class="nth-child-1"><a href="a_sub.html"><img src="./img/common/txt_04.png" alt="" /></a></p>
            </div>
            <img src="./img/common/pic_02.jpg" alt="" />
        </div>
    </div>
</div>

<div id="jumbotronPager" class="container">
    <div class="wrap">
        <div class="pager">
            <span class="nav prev"><img src="./img/common/btn_prev.gif" alt="" /></span>
            <span class="nav next"><img src="./img/common/btn_next.gif" alt="" /></span>
        </div>
        <ul class="tab">
            <li>
                <a href="#!">
                    <span class="over"><img src="./img/common/jumbotron_nav_on_01.gif" alt="" /></span>
                    <img src="./img/common/jumbotron_nav_off_01.gif" alt="" class="out" />
                </a>
            </li>
            ...
            <li>
                <a href="#!">
                    <span class="over"><img src="./img/common/jumbotron_nav_on_02.gif" alt="" /></span>
                    <img src="./img/common/jumbotron_nav_off_02.gif" alt="" class="out" />
                </a>
            </li>
        </ul>
    </div>
</div>
```
  
### javascript
```javascript
$(function(){
    $('#jumbotron').simpleSlidr({
        width: '980px',
        itemsLength: 4,
        innerEl: '.itemwrap',
        pagerEl: '#jumbotronPager',
        vertical: false,
        autoPlay: true,
        delay: 4000
    });
});
```

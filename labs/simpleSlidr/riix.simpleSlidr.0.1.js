$(function() {


    $.fn.simpleSlidr = function(options) {

        var setting = {
            width: '980px',
            itemsLength: 4,
            innerEl: '.itemwrap',
            pagerEl: '#jumbotronPager',
            vertical: false,
            autoPlay: true,
            delay: 4000,
            onSlide: null
        }

        // set opts
        var opts = $.extend(setting, options);

        var isSupport3d = function(_is) {
            var _result = false;
            if (_is === undefined || _is === true) {
                _result = (document.body && document.body.style.perspective !== undefined) ? true : _result;
                var _tempDiv = document.createElement("div"),
                    style = _tempDiv.style,
                    a = ["Webkit", "Moz", "O", "Ms", "ms"],
                    i = a.length;
                _result = (_tempDiv.style.perspective !== undefined) ? true : _result;
                while (--i > -1) {
                    _result = (style[a[i] + "Perspective"] !== undefined) ? true : _result;
                }
            }
            return _result;
        };


        var idxProof = function(_idx, _max) { // 인덱스값 범위 내 반환
            _idx = (_idx < 0) ? _max - 1 : _idx;
            _idx = (_idx < _max) ? _idx : 0;
            return _idx;
        };

        var currentActive = function(_el, _className, _imgToggle) { // active current item
            var $li = _el.parent('li'),
                $siblings,
                $img;
            _className = _className || 'active';
            _el = ($li.length) ? $li : _el;
            $siblings = _el.siblings();
            $img = $('img', _el).eq(0);
            _el.addClass(_className).siblings().removeClass(_className);
            if ($img.length && _imgToggle !== false) {
                imgToggle($img, '_on');
                imgToggle($siblings.find('img'), '_off');
            }
        };

        var imgToggle = function(_el, _first, _second) { // img toggler
            var _asIs = '_on',
                _toBe = '_off';
            if (_second !== undefined) {
                _asIs = _first;
                _toBe = _second;
            } else {
                if (_first === undefined || _first == '_on') {
                    _asIs = '_off';
                    _toBe = '_on';
                }
            }
            for (var i = 0, len = _el.length; i < len; i++) {
                var $el = _el.eq(i);
                if ($el[0].tagName.toLowerCase() !== 'img') {
                    $el = $el.find('img').eq(0);
                }
                $el.attr('src', $el.attr('src').replace(_asIs, _toBe));
            }
        };

        var callFunc = function(_callback, _param, _delay) {
            _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
            if ($.isFunction(_callback)) {
                _param = (_param === null) ? '' : _param;
                _delay = _delay || 50;
                setTimeout(function() {
                    // _callback.call(null, base, _param);
                    _callback.call(null, null, _param);
                }, _delay);
            } else {
                return false;
            }
        };

        var $el = $(this),
            $wrap = $el.parent(),
            $inner = $(opts.innerEl, $el),
            $pager = $(opts.pagerEl);

        var base = [];

        var _isCSS3 = isSupport3d(),
            _itemWidth = parseInt(opts.width, 10),
            _itemsLength = opts.itemsLength,
            _start = 0,
            _isHover = false,
            _timerAutoPlay = null;

        base.idx = 0;

        if (_isCSS3 === true && opts.vertical !== true) {
            _start: -(_itemWidth * 1.5);
        }

        var _setSlide = (function() {
            if (_isCSS3) {
                $inner.css({
                    'transition': 'all 0.4s'
                });
            }
        })();

        var _slideTo = function() {

            base.idx = idxProof(base.idx, _itemsLength);

            var _to = _start - (_itemWidth * base.idx);

            if (opts.vertical !== true) {
                if (_isCSS3) {
                    $inner.css({
                        'transform': 'translate3d(' + _to + 'px, 0, 0)'
                    });
                } else {
                    $inner.stop().animate({
                        'marginLeft': _to
                    });
                }
            } else {
                if (_isCSS3) {
                    $inner.css({
                        'transform': 'translate3d(0, ' + _to + 'px, 0)'
                    });
                } else {
                    $inner.stop().animate({
                        'marginTop': _to
                    });
                }
            }
            currentActive($('.item', $el).eq(base.idx + 1));
            currentActive($('li', base.$pager).eq(base.idx), 'active', false);

            if ($.isFunction(opts.onSlide)) { // callback
                opts.onSlide.call(this, base);
            };
        }

        var _setHandler = (function() {

            $('li:not(".not")', $pager).hover(function() {
                base.idx = $(this).index();
                _slideTo();
            });

            $wrap.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
            });

        })();

        var _autoPlay = function() {
            _timerAutoPlay = setTimeout(function() {
                if (base.idx >= _itemsLength) {
                    base.idx = 0;
                }
                if (_isHover == false && opts.autoPlay == true) {
                    _slideTo();
                    base.idx++;
                }
                _autoPlay();
            }, opts.delay);
        }

        _slideTo(0);
        _autoPlay();

    }

});
/**
 * 숫자 카운터
 * author: riix@epasscni.com
 */

(function($, window, document, ua) {

    'use strict';

    //  countr
    $.fn.flipCountr = function() {

        var _setting = {
            start: '00',
            height: 80,
            speed: 200,
            delayRatio: 0.8,
            isSecond: false,
            css3animation: true,
            onInit: function() {
                // console.log('1');
            },
            onComplete: null
        };

        var base = [];

        base.arrNumber = [];

        var $el = $(this);
        var _timerCSS = null,
            _timerDone = null;
        var _isSecond = false;

        var isSupport3d = function(_is) {
            var _result = false;
            if (_is === undefined || _is === true) {
                _result = (document.body && document.body.style.perspective !== undefined) ? true : _result;
                var _tempDiv = document.createElement("div"),
                    style = _tempDiv.style,
                    a = ["Webkit", "Moz", "O", "Ms", "ms"],
                    i = a.length;
                _result = (_tempDiv.style.perspective !== undefined) ? true : _result;
                while (--i > -1) {
                    _result = (style[a[i] + "Perspective"] !== undefined) ? true : _result;
                }
            }
            return _result;
        };

        var callFunc = function(_callback, _param, _delay) {
            _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
            if ($.isFunction(_callback)) {
                _param = (_param === null) ? '' : _param;
                _delay = _delay || 50;
                setTimeout(function() {
                    _callback.call(null, base, _param);
                }, _delay);
            } else {
                return false;
            }
        };

        var util = {
            drawCountr: function(_el) {
                if (_el.find('.countr').length && _el.find('.countr').length === base.text.length) return false;
                _el.empty();
                for (var i = 0, len = base.text.length; i < len; i++) {
                    base.arrNumber[i] = base.text.charAt(i);
                }
                for (var j = 0, lenArr = base.arrNumber.length; j < lenArr; j++) {
                    var _html = '';
                    for (var k = 0; k < 10; k++) {
                        if (k < 1) {
                            _html += '<strong class="current num-' + k + '"><em>' + k + '</em></strong>';
                        } else {
                            _html += '<strong class="num-' + k + '"><em>' + k + '</em></strong>';
                        }
                    }
                    _el.append($('<div class="countr flip"><p>' + _html + '</p></div>'));
                }
            },
            cssCountr: function(_el) {
                var $item = _el.find('div'),
                    $inner = _el.find('p'),
                    $strong = _el.find('strong');
                $.each([
                    $item,
                    $inner,
                    $strong
                ], function() {
                    $(this).css({
                        'display': 'block',
                        'padding': '0',
                        'text-align': 'center',
                        'font-size': base.opts.height + 'px',
                        'line-height': base.opts.height + 'px',
                        'height': base.opts.height + 'px',
                        'width': base.opts.height + 'px'
                    });
                });
                $item.css({
                    'position': 'relative',
                    'display': 'inline-block',
                    'overflow': 'hidden'
                });
                $inner.css({
                    'position': 'absolute',
                    'margin': '0'
                });
            },
            setCountr: function(_el) {

                for (var i = 0, len = base.text.length; i < len; i++) {
                    base.arrNumber[i] = base.text.charAt(i);
                }

                // div
                for (var j = 0, lenArr = base.arrNumber.length; j < lenArr; j++) {

                    var $inner = _el.find('div').eq(j).children(),
                        $strong = $inner.children(),
                        $before = $inner.find('.current');

                    // set before
                    var _beforeIdx = $inner.data('current');
                    $inner.data('before', _beforeIdx);

                    // set after
                    var _currentIdx = base.arrNumber[j];
                    $inner.data('current', _currentIdx);

                    // set class
                    $strong.removeClass('before current');
                    $strong.eq(_beforeIdx).addClass('before');
                    $strong.eq(_currentIdx).addClass('current');

                    // $before.addClass('before');

                }

            },
            goCountr: function(_el) {

                var $inner = _el.find('p'),
                    _delay = 0,
                    _end = 0;

                _el.addClass('animating');

                $inner.each(function(i) {

                    var $this = $(this);

                    // set before
                    var _beforeIdx = ($this.data('before') !== undefined) ? $this.data('before') : $this.find('.before').index();
                    var _currentIdx = ($this.data('current') !== undefined) ? $this.data('current') : $this.find('.current').index();

                    _beforeIdx = (_beforeIdx === undefined || _beforeIdx < 0) ? 0 : _beforeIdx;
                    _currentIdx = (_currentIdx === undefined || _currentIdx < 0) ? 0 : _currentIdx;

                    var _term = Math.abs(_beforeIdx - _currentIdx),
                        _speed = _term * base.opts.speed,
                        _height = 0 - (_currentIdx * base.opts.height);

                    ///////////////////// console.log($item.index() == base.arrNumber.length);

                    // if 0 to 9 then faster in second timer mode
                    // console.log(_isSecond);
                    if (base.opts.isSecond === true && _term >= 9) {
                        _speed = base.opts.speed;
                    }

                    if (base.opts.isSecond === true) _delay = 0;

                    if (_term === 0) {

                        return true;

                    } else if (base.opts.css3animation === true) { // css 3d

                        $this.css({
                            'transition-timing-function': 'ease-out',
                            'transition-duration': _speed + 'ms',
                            'transition-delay': _delay + 'ms',
                            'transform': 'translate3d(0, 0, 0)'
                        });
                        _timerCSS = setTimeout(function() {
                            $this.css({
                                'transform': 'translate3d(0, ' + _height + 'px, 0)'
                            });
                        }, 10);

                    } else { // jquery animate

                        $this.stop(false, false).delay(_delay).animate({
                            'margin-top': _height
                        }, _speed);

                    }

                    _delay = _delay + (_term * base.opts.speed * base.opts.delayRatio);

                });

                // done
                _timerDone = setTimeout(function() {
                    _el.removeClass('animating');
                    callFunc(base.opts.onComplete); // onComplete
                }, _delay + 100);

            }
        };

        // core
        var init = function(_options) {

            base.opts = (typeof _options === 'object') ? $.extend(_setting, _options) : _setting;
            base.opts.css3animation = (isSupport3d(base.opts.css3animation)) ? base.opts.css3animation : false;

            for (var i = 0, len = $el.length; i < len; i++) {
                var $this = $el.eq(i);
                $this.data('base', base);
                base.text = ($this.data('to') !== undefined) ? $.trim($this.data('to')) : base.opts.start;
                util.drawCountr($this);
                util.cssCountr($this);
                callFunc(base.opts.onInit);
            }

        };


        // run
        var run = function(_options) {

            for (var i = 0, len = $el.length; i < len; i++) {

                var $this = $el.eq(i);

                if ($this.data('base') === undefined || $this.data('to') === undefined) {
                    return false;
                }

                base = $this.data('base');

                if (base.opts.isSecond !== true && $this.hasClass('animating') === true) {
                    return false;
                }

                base.text = $.trim($this.data('to'));

                if (base.opts.isSecond) {
                    $this.addClass('second');
                }

                util.setCountr($this);
                util.goCountr($this);

            }

        };


        return {
            init: function(_options) {
                init(_options);
            },
            run: function() {
                run();
            }
        };



    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
/**
 * countdownr
 * author: riix
 */
$.fn.countdownr = function(_options) {

    var _setting = {
        date: '00:00 April 14, 2015',
        onInit: function(base) {
            // console.log('on init');
        },
        onEverySecond: function(base) {
            // console.log('on second');
        },
        onFinish: function(base) {
            // console.log('finished');
        }
    };

    var base = [];

    var _timerRepeat = null,
        _delay = 10;

    base.opts = (typeof _options === 'object') ? $.extend(_setting, _options) : _setting;

    base.$el = $(this);
    base.isFinished = false;

    var callFunc = function(_callback, _param, _delay) {
        _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
        if ($.isFunction(_callback)) {
            _param = (_param === null) ? '' : _param;
            _delay = _delay || 50;
            setTimeout(function() {
                _callback.call(null, base, _param);
            }, _delay);
        } else {
            return false;
        }
    };

    var util = {

        returnTimeLeft: function(_dateTarget) {

            var result = [],
                currentDate = new Date().getTime(),
                targeDate = new Date(_dateTarget).getTime(),
                leftSec = (targeDate - currentDate) / 1000;

            var digitTwo = function(n) {
                var result = (n < 10 ? '0' : '') + n;
                return result;
            };

            leftSec = (leftSec < 0) ? 0 : leftSec;

            result.finish = false; // finish key

            result.day = digitTwo(parseInt(leftSec / 86400, 10));
            leftSec = leftSec % 86400;

            result.hr = digitTwo(parseInt(leftSec / 3600, 10));
            if (leftSec < 1) result.finish = true; // finish key
            leftSec = leftSec % 3600;

            result.min = digitTwo(parseInt(leftSec / 60, 10));
            result.sec = digitTwo(parseInt(leftSec % 60, 10));

            result.leftSec = leftSec;

            // console.log(result.day + 'd:' + result.hr + 'h:' + result.min + 'm:' + result.sec + 's');

            return result;
        }

    };

    var onFinish = function() {

        clearTimeout(_timerRepeat);
        base.$el.removeClass('going').addClass('finished');
        callFunc(base.opts.onFinish); // callback

    };

    var init = function() {

        base.$el.addClass('countdownr going');

        if (base.$el.data('date')) {
            base.opts.date = base.$el.data('date');
        }

        callFunc(base.opts.onInit); // callback

        var repeat = function() {

            _timerRepeat = setTimeout(function() {

                clearTimeout(_timerRepeat);

                base.count = util.returnTimeLeft(base.opts.date);
                base.isFinished = base.count.finish;

                callFunc(base.opts.onEverySecond); // callback

                if (base.isFinished === true) { // finish
                    clearTimeout(_timerRepeat);
                    onFinish();
                    return false;
                }

                _delay = 1000; // reset delay
                repeat();

            }, _delay);


        };

        repeat();
    };

    /**
     * return counter html
     * @param  {[type]} _count    [description]
     * @param  {[type]} _isSecond 초 출력 유무
     * <strong>0</strong>
     * <strong>8</strong>
     * <span class="ir colon"><em>:</em></span>
     * <strong>1</strong>
     * <strong>7</strong>
     * <span class="ir colon"><em>:</em></span>
     * <strong>0</strong>
     * <strong>6</strong>
     */
    base.returnCounterHtml = function(_count, _isSecond) {
        var _html = '';
        _html += '<strong>' + _count.hr.substr(0, 1) + '</strong><strong>' + _count.hr.substr(1, 2) + '</strong>';
        _html += '<span class="ir colon"><em>:</em></span><strong>' + _count.min.substr(0, 1) + '</strong><strong>' + _count.min.substr(1, 2) + '</strong>';
        if (_isSecond === true) {
            _html += '<span class="ir colon"><em>:</em></span><strong>' + _count.sec.substr(0, 1) + '</strong><strong>' + _count.sec.substr(1, 2) + '</strong>';
        }
        return _html;
    };

    return this.each(function() {
        init();
    });

};
# productViewr

Product Viewer for Shopping Mall Product Detail Page.

## demo
- [basic format](http://riix.github.io/labs/productViewr/index.html)
- [json format](http://riix.github.io/labs/productViewr/index.json.html)
 
## basic format

### html
```html
<div id="productViewr" class="viewr">
    <div class="middle">
        <ul class="reset" id="middle">
            <li class="item nth-child-1"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480.jpg"><p>상품 사진 설명이 들어가는 부분 1</p></a></li>
            <li class="item nth-child-2"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_02.jpg"><p>상품 사진 설명이 들어가는 부분 2</p></a></li>
            <li class="item nth-child-3"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_03.jpg"><p>상품 사진 설명이 들어가는 부분 3</p></a></li>
            <li class="item nth-child-4"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_04.jpg"><p>상품 사진 설명이 들어가는 부분 4</p></a></li>
            <li class="item nth-child-5"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_05.jpg"><p>상품 사진 설명이 들어가는 부분 5</p></a></li>
            <li class="item nth-child-6"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480.jpg"><p>상품 사진 설명이 들어가는 부분 6</p></a></li>
            <li class="item nth-child-7"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_02.jpg"><p>상품 사진 설명이 들어가는 부분 1</p></a></li>
            <li class="item nth-child-8"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_03.jpg"><p>상품 사진 설명이 들어가는 부분 2</p></a></li>
            <li class="item nth-child-9"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/480x480_04.jpg"><p>상품 사진 설명이 들어가는 부분 3</p></a></li>
        </ul>
    </div>
    <div class="small">
        <ul class="tab" id="small">
            <li class="item nth-child-1"><a href="http://riix.github.io/cdn/img/thumb/1024x1024.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70.jpg"></a></li>
            <li class="item nth-child-2"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_02.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_02.jpg"></a></li>
            <li class="item nth-child-3"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_03.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_03.jpg"></a></li>
            <li class="item nth-child-4"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_04.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_04.jpg"></a></li>
            <li class="item nth-child-5"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_05.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_05.jpg"></a></li>
            <li class="item nth-child-6"><a href="http://riix.github.io/cdn/img/thumb/1024x1024.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70.jpg"></a></li>
            <li class="item nth-child-7"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_02.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_02.jpg"></a></li>
            <li class="item nth-child-8"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_03.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_03.jpg"></a></li>
            <li class="item nth-child-9"><a href="http://riix.github.io/cdn/img/thumb/1024x1024_04.jpg"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_04.jpg"></a></li>
        </ul>
    </div>
</div>

<div id="modalLarge" class="viewr-modal"><!-- modalLarge -->
    <div class="wrap">
        <h3 class="subject">상품이미지 보기</h3>
        <div class="nav">
            <ul id="largeNav" class="tab">
                <li class="item nth-child-1"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70.jpg"></a></li>
                <li class="item nth-child-2"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_02.jpg"></a></li>
                <li class="item nth-child-3"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_03.jpg"></a></li>
                <li class="item nth-child-4"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_04.jpg"></a></li>
                <li class="item nth-child-5"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_05.jpg"></a></li>
                <li class="item nth-child-6"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70.jpg"></a></li>
                <li class="item nth-child-7"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_02.jpg"></a></li>
                <li class="item nth-child-8"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_03.jpg"></a></li>
                <li class="item nth-child-9"><a href="#!"><img alt="blah blah" src="http://riix.github.io/cdn/img/thumb/70x70_04.jpg"></a></li>
            </ul>
        </div>
        <div class="large">
            <div id="large"></div>
        </div>
        <a href="#!" class="close"><img src="http://riix.github.io/cdn/img/module/btn_close.gif" alt="닫기" /></a>
    </div>
</div><!-- // modalLarge -->
```

### javascript
```javascript
$(function(){
    $('#productViewr').productViewr({
        modal: '#modalProductViewr',
        onComplete: function(base){
        }
    });
});
```

## json format

### html

```html
<div id="productViewr" class="viewr">
    <div class="middle">
        <ul id="middle" class="reset"></ul>
    </div>
    <div class="small">
        <ul id="small" class="tab"></ul>
    </div>
</div>

<div id="modalLarge" class="viewr-modal"><!-- modalLarge -->
    <div class="wrap">
        <h3 class="subject">상품이미지 보기</h3>
        <div class="nav">
            <ul id="largeNav" class="tab"></ul>
        </div>
        <div class="large">
            <div id="large"></div>
        </div>
        <a href="#!" class="close"><img src="http://riix.github.io/cdn/img/module/btn_close.gif" alt="닫기" /></a>
    </div>
</div><!-- // modalLarge -->
```

### javascript

```javascript
var _jsonItems = {
    "items": [{
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 1"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_02.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_02.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_02.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 2"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_03.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_03.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_03.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 3"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_04.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_04.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_04.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 4"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_05.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_05.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_05.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 5"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 6"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_02.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_02.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_02.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 1"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_03.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_03.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_03.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 2"
    }, {
        "large": "http://riix.github.io/cdn/img/thumb/1024x1024_04.jpg",
        "middle": "http://riix.github.io/cdn/img/thumb/480x480_04.jpg",
        "small": "http://riix.github.io/cdn/img/thumb/70x70_04.jpg",
        "alt": "blah blah",
        "desc": "상품 사진 설명이 들어가는 부분 3"
    }]
}

$('#productViewr').productViewr({
    json: _jsonItems,
    modal: '#modalLarge'
});
```


## options
```javascript
var setting = {
    json: null,
    small: '#small',
    middle: '#middle',
    large: '#large',
    largeNav: '#largeNav',
    modal: '#modalLarge',
    itemClass: '.item',
    largeClose: '.close',
    onInit: null,
    onComplete: null,
    onFullsize: null
};
```
(function($, window, document, ua) {

    'use strict';

    var GLOBAL = [];

    GLOBAL.delay = 300;

    var arrFx = [
        'ball-pulse--3',
        'ball-grid-pulse--9',
        'ball-clip-rotate--1',
        'ball-rotate--1',
        'ball-triangle-path--3',
        'ball-scale--1',
        'ball-scale-multiple--3',
        'ball-pulse-sync--3',
        'ball-beat--3',
        'ball-scale-ripple--1',
        'ball-scale-ripple-multiple--3',
        'ball-spin-fade-loader--8',
        'ball-grid-beat--9',
        'line-scale--5',
        'line-scale-party--4',
        'line-scale-pulse-out--5',
        'line-scale-pulse-out-rapid--5',
        'line-spin-fade-loader--8',
        'square-spin--1',
        'cube-transition--2',
        'triangle-skew-spin--1',
        'semi-circle-spin--1',
        'ball-clip-rotate-pulse--2',
        'ball-clip-rotate-multiple--2',
        'ball-pulse-rise--5',
        'ball-zig-zag--2',
        'ball-zig-zag-deflect--2',
        'pacman--5'
    ]

    var getRandInArray = function(_arr) {
        var _result = ['blue', 'red', 'green', 'orange', 'pink'];
        _result = (typeof _arr === 'object') ? _arr : _result;
        _result = _result[Math.floor(Math.random() * _result.length)];
        return _result;
    };

    /**
     * preloadr
     * @param  {[type]} _todo    [description]
     * @param  {[type]} _options [description]
     * @return {[type]}          [description]
     */
    $.fn.preloadr = function(_todo, _options) {

        var base = [];

        var _setting = {
            loader: '.loader',
            classReady: 'preloadr-ready',
            classActive: 'preloadr-active',
            classOverlay: 'overlay-active',
            delay: GLOBAL.delay,
            fx: 'ball-triangle-path--3',
            width: 80,
            height: 50,
            clickToStop: false
        };

        var opts = $.extend(_setting, _options);

        base.$el = $(this);
        base.$loader = $(opts.loader, base.$el);

        var timerPreloadr = null;
        var innerLength = 0;
        var fx = null;

        var init = function() { // init

            if (!base.$loader.length) {
                base.$loader = $('<div class="loader js-preloadr" aria-hidden="true"><div class="loader-inner"></div></div>').appendTo(base.$el);
            }

            base.$inner = base.$loader.children();

        };

        base.active = function() {


            var fill = (function() {

                innerLength = parseInt(base.$inner.data('item'), 10);

                if (opts.fx === 'random') { // random
                    opts.fx = getRandInArray(arrFx);
                }

                fx = opts.fx.split('--');
                base.$inner.empty();
                base.$el.removeClass('init');

                base.$loader.removeAttr('style');
                base.$inner.removeClass();

                base.$inner.addClass('loader-inner ' + fx[0]);

                if (!base.$el.hasClass('init') && fx[1] !== null) {
                    for (var i = 0; i < fx[1]; i++) {
                        base.$inner.append('<div></div>');
                    }
                    base.$el.addClass('init');
                }


            })();

            if (opts.clickToStop === true) {
                base.$el.overlay('show', {
                    onClick: function(overlay) {
                        base.deactive();
                        overlay.deactive();
                    }
                });
            } else {
                base.$el.overlay('show');
            }

            base.$el.addClass(opts.classReady);

            var move = function() {
                var width = base.$inner.width(),
                    height = base.$inner.height();
                // width = (width < 100) ? 100 : width;
                // height = (height < 50) ? 50 : height;
                width = opts.width;
                height = opts.height;
                base.$loader.css({
                    'width': width,
                    'height': height,
                    'margin-top': -(height / 2),
                    'margin-left': -(width / 2),
                    'top': '50%',
                    'left': '50%'
                });
            }

            timerPreloadr = setTimeout(function() {
                clearTimeout(timerPreloadr);
                move();
                base.$el.addClass(opts.classActive);
            }, 1);

        }

        base.deactive = function() {

            base.$el.overlay('hide');
            base.$el.removeClass(opts.classActive);
            base.$el.removeClass(opts.classOverlay);

            timerPreloadr = setTimeout(function() {
                clearTimeout(timerPreloadr);
                base.$el.removeClass(opts.classReady);
            }, opts.delay);
        }

        return this.each(function() {
            init();
            if (_todo === 'show') {
                base.active();
            } else {
                base.deactive();
            }
        });

    };

    /**
     * overlay
     * @param  {[type]} _todo    [description]
     * @param  {[type]} _options [description]
     * @return {[type]}          [description]
     */
    $.fn.overlay = function(_todo, _options) {

        var base = [];

        var _setting = {
            overlay: '.overlay',
            classReady: 'overlay-ready',
            classActive: 'overlay-active',
            classPreloadr: 'preloadr-active',
            delay: GLOBAL.delay,
            onClick: null
        };

        var opts = $.extend(_setting, _options);

        var timerOverlay = null;

        base.$el = $(this);
        base.$overlay = $(opts.overlay, base.$el);

        // run callback func
        var callFunc = function(_callback, _param, _delay) {
            _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
            if ($.isFunction(_callback)) {
                _param = (_param === null) ? '' : _param;
                _delay = _delay || 50;
                setTimeout(function() {
                    _callback.call(null, base, _param);
                }, _delay);
            } else {
                return false;
            }
        };

        var init = function() {
            if (!base.$overlay.length) {
                base.$overlay = $('<div class="overlay js-overlay" aria-hidden="true"><div class="overlay-inner"></div></div>').appendTo(base.$el);
            }
        };

        base.active = function() {

            base.$el.addClass(opts.classReady);
            timerOverlay = setTimeout(function() {
                clearTimeout(timerOverlay);
                base.$el.addClass(opts.classActive);

                if ($.isFunction(opts.onClick)) {
                    base.$overlay.on('click', function(e) {
                        e.preventDefault();
                        callFunc(opts.onClick);
                    });
                }

            }, 1);
        };

        base.deactive = function() {
            base.$el.removeClass(opts.classPreloadr);
            base.$el.removeClass(opts.classActive);
            timerOverlay = setTimeout(function() {
                clearTimeout(timerOverlay);
                base.$el.removeClass(opts.classReady);
            }, opts.delay);
        };

        return this.each(function() {
            init();
            if (_todo === 'show') {
                base.active();
            } else {
                base.deactive();
            }
        });

    };




})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
# underScor

Responsive Slide Navigation Width Underscore

### demo
[basic](http://riix.github.io/labs/underScor/)
 
### html
```html
<div id="container">
    <div id="underscoreWrap">
        <ul id="underscore" class="tab">
            <li class="item"><a href="#!"><span>메뉴1</span></a></li>
            <li class="item"><a href="#!"><span>메뉴2</span></a></li>
            <li class="item"><a href="#!"><span>메뉴3</span></a></li>
            <li class="item"><a href="#!"><span>메뉴4</span></a></li>
            <li class="item"><a href="#!"><span>메뉴5</span></a></li>
            <li class="item"><a href="#!"><span>메뉴6</span></a></li>
            <li class="item"><a href="#!"><span>메뉴7</span></a></li>
            <li class="item"><a href="#!"><span>메뉴8</span></a></li>
            <li class="item"><a href="#!"><span>메뉴9</span></a></li>
            <li class="item"><a href="#!"><span>메뉴10</span></a></li>
            <li class="item"><a href="#!"><span>메뉴11</span></a></li>
            <li class="item"><a href="#!"><span>메뉴12</span></a></li>
        </ul>
    </div>
    <div class="indicator-wrap">
        <div id="underscoreIndicator" class="indicator"></div>
    </div>
    <p class="buttons">
        <button>1</button>
        <button>2</button>
        <button>3</button>
        <button>4</button>
        <button>5</button>
        <button>6</button>
        <button>7</button>
        <button>8</button>
        <button>9</button>
        <button>10</button>
        <button>11</button>
        <button>12</button>
    </p>
</div>
```

### javascript
```javascript
$('#underscore').underScor({   
    
    // indicator
    indicator: '#underscoreIndicator', 

    // callback function on setup complete
    onComplete: function(base) { 
        $('.buttons button').on('click', function(e) {
            e.preventDefault();
            var _idx = $(this).index();
            base.$el.data('afteridx', _idx).trigger('goto');
        });
    }

});
```

### options
```
indicator: indicator selector
item: item selector
activeClass: activate class name
onComplete: callback function on setup complete
```
(function($) {

    'use strict';

    /**
     * 목록 탭 너비 구하기
     * @param  {[type]} _el 너비 구할 대상
     * @return {int}     너비 width 값
     */
    $.getListTabWidth = function(_el) {
        _el = (_el[0].tagName.toLowerCase() == 'li' || _el.hasClass('item')) ? _el.parent() : _el;
        var $items = _el.children();
        if (!$items.length) return false;
        var _result = 0;
        for (var i = 0, len = $items.length; i < len; i++) {
            _result += $items.eq(i).width();
        }
        return _result;
    };

    /**
     * 슬라이더 네비게이션 포지션 반환
     * @param  {[type]} _el         활성화된 아이템
     * @param  {[type]} _wrapper    감싸는 요소
     * @param  {[type]} _totalWidth 아이템 묶음 전체 길이
     * var _pos = $.getActiveItemPos(base.$active, base.$wrap, base.itemsWidth);
     * base.$el.stop().animate({
     *     'margin-left': _pos.left
     * });
     * if (_isIndicator === true) {
     *     base.$indicator.stop().animate({
     *         'width': _pos.itemWidth,
     *         'margin-left': _pos.indicatorLeft
     *     });
     * }
     */
    $.getActiveItemPos = function(_el, _wrapper, _totalWidth) {

        var _return = {
            left: 0, // slider nav left position
            indicatorLeft: 0, // underscore indicator left position
            itemWidth: _el.width() // element width
        };

        _wrapper = (_wrapper !== undefined) ? _wrapper : _el.parent().parent();
        _totalWidth = (_totalWidth !== undefined) ? _totalWidth : el.parent().width();

        var _itemWidthHalf = (_return.itemWidth / 2),
            _itemPosLeft = _el.position().left,
            _parentWidth = _wrapper.width(),
            _parentWidthHalf = (_parentWidth / 2);

        if (_totalWidth < _parentWidth) { // item width 가 wrapper width 보다 작을때

            _return.indicatorLeft = 0;
            _return.left = 0;

        } else if ((_totalWidth - _itemPosLeft - _itemWidthHalf) < _parentWidthHalf) { // 뒷단

            _return.indicatorLeft = _itemPosLeft - _totalWidth + _parentWidth;
            _return.left = 0 - (_totalWidth - _parentWidth);

        } else if (_itemPosLeft <= _parentWidthHalf) { // 앞단

            _return.indicatorLeft = _itemPosLeft;
            _return.left = 0;

        } else { // 기본

            _return.indicatorLeft = _parentWidthHalf - _itemWidthHalf;
            _return.left = _return.indicatorLeft - _itemPosLeft;

        }
        return _return;
    };


    /**
     * 슬라이드 네비게이션 언더스코어 생성자
     */
    $.fn.underScor = function(options) {

        var base = [];

        var setting = {
            indicator: '#underscoreIndicator',
            item: '.item',
            activeClass: 'active',
            onComplete: null
        };

        base.opts = $.extend(setting, options);

        base.$el = $(this);
        base.$wrap = base.$el.parent();
        base.$items = base.$el.find(base.opts.item);
        base.$indicator = $(base.opts.indicator);
        base.afterIdx = 0;
        base.itemsWidth = 0;

        var _isIndicator = ($(base.opts.indicator).length) ? true : false;
        var _timerResize = null;

        // active current item
        var _currentActive = function(_el, _className, _imgToggle) {

            _className = _className || 'active';

            var $this = _el,
                $parent = $this.parent('li'),
                $el = ($parent.length) ? $parent : $this;

            $el.addClass('active').siblings().removeClass('active');

        };

        // goto
        var _goto = function() {

            if (!base.$items.eq(base.afterIdx).length) return false;

            base.$active = base.$items.eq(base.afterIdx);
            _currentActive(base.$active, base.opts.activeClass);

            var _pos = $.getActiveItemPos(base.$active, base.$wrap, base.itemsWidth);

            base.$el.stop().animate({
                'margin-left': _pos.left
            });

            if (_isIndicator === true) {
                base.$indicator.stop().animate({
                    'width': _pos.itemWidth,
                    'margin-left': _pos.indicatorLeft
                });
            }

        };

        // core
        var _core = function() {

            // trigger
            base.$el.on('goto', function() {
                base.afterIdx = base.$el.data('afteridx');
                _goto();
            });

            // on complete callback
            if ($.isFunction(base.opts.onComplete)) {
                setTimeout(function() {
                    base.opts.onComplete.call(null, base);
                }, 50);
            }

            $(window).on('resize orientationchange', function() {
                clearTimeout(_timerResize);
                _timerResize = setTimeout(function() {
                    _goto();
                }, 100);
            }).trigger('resize');

        };

        return this.each(function() {

            base.itemsWidth = $.getListTabWidth(base.$el);

            base.$wrap.css({
                'position': 'relative',
                'overflow': 'hidden'
            });

            base.$el.css({
                'position': 'absolute',
                'width': base.itemsWidth
            });

            if (_isIndicator === true) {
                base.$indicator.css({
                    'position': 'absolute'
                });
            }

            _core();

        });

    };

})(jQuery);
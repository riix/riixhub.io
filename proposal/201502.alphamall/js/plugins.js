/*
 *  접근성을 고려한 DatePicker v0.3
 *  Author : 박순길
 *  Based On : Accessible Date Picker Calendar - webSemantics, http://www.websemantics.co.uk/tutorials/accessible_date_picker_calendar/
 */
function add0(a) {
    return 10 > a ? "0" + a : a
}

function textDate(a, t, e, d) {
    var n = new Date(e, t - 1, a);
    return d === !0 ? n.getFullYear() + "-" + dateMonths[n.getMonth()] + "-" + add0(n.getDate()) + " (" + weekDay[n.getDay()] + ")" : n.getFullYear() + "-" + dateMonths[n.getMonth()] + "-" + add0(n.getDate())
}

function subtractDate(a, t) {
    return t - a
}

function toShortDate(a) {
    return a = new Date(a), add0(a.getDate()) + dateSplit + add0(a.getMonth() + 1) + dateSplit + a.getFullYear()
}

function writeInputDateValue(a, t) {
    d = $(t).data("day"), a.shortDate && (d = toShortDate(d)), $("#" + a.inputId).val(d), hideCalendar(a)
}

function calendarObject(a, t, e, d, n) {
    this.wrapperId = a, this.inputId = t, this.buttonId = e, this.holderId = d, this.nextId = n, this.shortDate = !1
}

function matchToday() {
    $("table.calendar td a").each(function() {
        var a = $("div.cal-wrapper div.today a.today").data("day"),
            t = $(this);
        t.data("day") == a && t.attr("title", t.attr("title") + " (오늘)").addClass("today")
    })
}

function buildCalendar(a, t) {
    var e = (a.getDate(), a.getMonth()),
        d = a.getFullYear(),
        n = new Date(d, e, 1),
        s = new Date(d, e + 1, 1),
        r = !1,
        l = [],
        o = [],
        i = $("#" + t.inputId);
    i.data("min") && (l = i.data("min").split("-", 3)), i.data("max") && (o = i.data("max").split("-", 3));
    var c = n.getDay(),
        p = Math.floor((s.getTime() - n.getTime()) / 864e5);
    2 == e && (p = 31);
    var h = "";
    h += '<div id="' + t.wrapperId + '" class="cal-wrapper">', h += '  <div class="head">', h += '      <div class="year"><select title="년도 선택" class="select">';
    for (var u = 1910; 2020 > u; u++) h += u == d ? '      <option value="' + u + '" selected>' + u + "년</option>" : '      <option value="' + u + '">' + u + "년</option>";
    h += '      </select><a href="javascript:;" class="button"><span class="sr-only">선택한 년도로</span><span>이동</span></a></div>', h += '      <div class="month">', h += d < l[0] || d == l[0] && dateMonths[e] <= l[1] ? '          <a href="javascript:;" class="nav prevmonth disabled"><span class="icon datepicker-prev"><em>이전 ' + dateMonths[0 === e ? 11 : e - 1] + " 월로 이동</em></span></a>" : '          <a href="javascript:;" class="nav prevmonth"><span class="icon datepicker-prev"><em>이전 ' + dateMonths[0 === e ? 11 : e - 1] + " 월로 이동</em></span></a>", h += '          <span class="month"><strong>' + dateMonths[e] + '</strong>월<span class="sr-only"> - 현재 월</span></span>', h += d > o[0] || d == o[0] && dateMonths[e] >= o[1] ? '          <a href="javascript:;" class="nav nextmonth disabled"><span class="icon datepicker-next"><em>다음 ' + dateMonths[11 == e ? 0 : e + 1] + " 월로 이동</em></span></a>" : '          <a href="javascript:;" class="nav nextmonth"><span class="icon datepicker-next"><em>다음 ' + dateMonths[11 == e ? 0 : e + 1] + " 월로 이동</em></span></a>", h += "      </div>", h += '      <div class="today"><a href="javascript:;" class="today" data-day=' + textDate(dateToday.getDate(), dateToday.getMonth() + 1, dateToday.getFullYear(), !1) + ">오늘날짜: <span>" + textDate(dateToday.getDate(), dateToday.getMonth() + 1, dateToday.getFullYear(), !0) + '</span><span class="sr-only"> - 해당 년/월로 이동</span></a></div>', h += "  </div>", h += '  <div class="core">', h += '      <table class="calendar" cellspacing="0" summary="날짜 선택을 위한 양식입력 테이블입니다.">', h += "          <caption>날짜 선택</caption>", h += '          <thead><tr><th scope="col" class="weekend sun"><abbr title="일요일">S</abbr></th><th scope="col" class="mon"><abbr title="월요일">M</abbr></th><th scope="col" class="tue"><abbr title="화요일">T</abbr></th><th scope="col" class="wed"><abbr title="수요일">W</abbr></th><th scope="col" class="thu"><abbr title="목요일">T</abbr></th><th scope="col" class="fri"><abbr title="금요일">F</abbr></th><th scope="col" class="weekend sat"><abbr title="토요일">S</abbr></th></tr></thead>', h += "          <tbody>", h += "              <tr>";
    for (var v = 0; c > v; v++) h += 0 === v ? '<td class="empty weekend sun">&nbsp;</td>' : 6 == v ? '<td class="empty weekend sat">&nbsp;</td>' : '<td class="empty">&nbsp;</td>';
    var f = s.getMonth();
    1 > f && (f = 12), f = add0(f), week_day = c;
    for (var y = 1; p >= y; y++) week_day %= 7, 0 === week_day && (h += "    </tr>"), h += 0 === week_day ? '<td class="weekend sun">' : 6 == week_day ? '<td class="weekend sat">' : "<td>", d < l[0] || d == l[0] && dateMonths[e] < l[1] || d == l[0] && dateMonths[e] == l[1] && y < l[2] ? (r = !0, h += '<span title="' + textDate(y, f, d, !0) + '">' + add0(y) + "</span></td>") : d > o[0] || d == o[0] && dateMonths[e] > o[1] || d == o[0] && dateMonths[e] == o[1] && y > o[2] ? (r = !0, h += '<span title="' + textDate(y, f, d, !0) + '">' + add0(y) + "</span></td>") : (r = !1, h += '<a title="' + textDate(y, f, d, !0) + '" data-day="' + textDate(y, f, d, !1) + '" href="' + y + '">' + add0(y) + "</a></td>"), week_day++;
    for (week_day = week_day; 7 > week_day; week_day++) h += 0 === week_day ? '<td class="empty weekend sun">&nbsp;</td>' : 6 == week_day ? '<td class="empty weekend sat">&nbsp;</td>' : '<td class="empty">&nbsp;</td>';
    return h += "          </tr></tbody></table>", h += "  </div>", h += '  <a id="skipCalendar" href="#' + t.nextId + '" class="button-close"><span class="icon close"><em>날짜 선택창 (현재 창) 닫기</em></span></a>', h += "</div>"
}

function displayCalendar(a) {
    var t = $("#" + a.holderId),
        e = $("#" + a.wrapperId),
        d = $("#" + a.buttonId);
    t.empty().append(buildCalendar(date, a)), $("div.year a").on("click", e, function(t) {
        t.preventDefault();
        var e = $(this).parents("div").eq(0).find("select").eq(0).val();
        dateTemp = new Date(e, date.getMonth(), 1), date = dateTemp, displayCalendar(a), $("#" + a.wrapperId + " div.year select").eq(0).focus()
    }), $("a.prevmonth").on("click", e, function(t) {
        t.preventDefault();
        var e = new Date(date.getFullYear(), date.getMonth() - 1, 1);
        if ($(this).hasClass("disabled")) {
            var d = $("#" + a.inputId).data("min");
            alert(d + " 을 벗어난 달은 선택이 불가능 합니다.")
        } else date = e, displayCalendar(a);
        $(this).focus()
    }), $("a.nextmonth").on("click", e, function(t) {
        t.preventDefault();
        var e = new Date(date.getFullYear(), date.getMonth() + 1, 1);
        if ($(this).hasClass("disabled")) {
            var d = $("#" + a.inputId).data("max");
            alert(d + " 을 벗어난 달은 선택이 불가능 합니다.")
        } else date = e, displayCalendar(a);
        $(this).focus()
    }), $("table.calendar td a").on("click", e, function(t) {
        return t.preventDefault(), writeInputDateValue(a, $(this)), !1
    }), $("a.button-close").on("click", e, function(t) {
        t.preventDefault(), hideCalendar(a), d.focus()
    }), $("div.today a").on("click", e, function(t) {
        t.preventDefault(), date = new Date, displayCalendar(a), e.find("a.today").eq(0).focus()
    });
    var n = d.offset();
    return t.css({
        top: n.top + 30,
        left: n.left - 70,
        opacity: 1
    }).find("select").eq(0).focus(), matchToday(), !1
}

function hideCalendar(a) {
    $("#" + a.holderId).animate({
        opacity: 0
    }, 300, function() {
        $(this).remove()
    });
    var t = $("#" + $("#" + a.buttonId).data("target"));
    return t.focus(), !1
}
var date = new Date,
    dateToday = date,
    dateMonths = new Array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"),
    weekDay = new Array("일", "월", "화", "수", "목", "금", "토"),
    dateSplit = "-";
$(function() {
    var a;
    $(".btn-datepicker, .js-datepicker").each(function(t) {
        var e = $(this);
        if (!e.data("target")) {
            var d = e.attr("href");
            _href = "#" !== d.substring("0", "1") ? e.prev("input").eq(0).attr("id") : d.replace("#", ""), e.data("target", _href)
        }
        var n = "calc" + t,
            s = e.data("target"),
            r = $("#" + s);
        r.attr("maxlength", "10").on("focusout", function() {
            var a = /^([0-9]{4})-([0-9]{2})-([0-9]{2})/g,
                t = $(this).val();
            "" === t || a.test(t) || (alert("날짜 형식에 맞게 입력해주세요.\n예) 2013-11-11"), setTimeout(function() {
                r.val("").focus()
            }, 100))
        }), e.on("click", function(d) {
            d.preventDefault(), $(".section-datepicker").remove();
            var l = r.val(),
                o = /^([0-9]{4})-([0-9]{2})-([0-9]{2})/g;
            if ("" !== l && o.test(l)) {
                var i = l.split("-", 4);
                date = new Date(i[0], i[1] - 1, i[2])
            } else date = new Date;
            a = new calendarObject("cal" + t, s, "calBtn" + t, "calSpace" + t, n), e.attr("id", a.buttonId);
            var c = '<div class="section-datepicker" id="' + a.holderId + '"></div>';
            "bottom" == r.data("position") ? $("body").append(c) : e.after(c), displayCalendar(a)
        })
    })
});
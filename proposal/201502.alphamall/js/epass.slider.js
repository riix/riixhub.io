/*
 * Author: @epass
 * Source: http://tympanus.net/codrops/?p=18632
 * $('#contents').epassSlider();
 */

var Modernizr = Modernizr || [];

(function($, window, document, ua) {

    'use strict';

    // base
    if (!$.epass) $.epass = {};

    $.epass.slider = function(el, options, param) {

        // setup ==================================================

        // set base
        var base = this; // to avoid scope issues, use 'base' instead of 'this' to reference this class from internal events and functions.

        // detect device
        var isChrome = /chrome/i.exec(ua),
            isAndroid = /android/i.exec(ua),
            hasTouch = 'ontouchstart' in window && !(isChrome && !isAndroid),
            isMobile = ((/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(ua) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(ua.substr(0, 4)))) ? true : false;

        // modernizr option
        var support = {
            animations: Modernizr.cssanimations
        };

        var animEndEventNames = {
            'WebkitAnimation': 'webkitAnimationEnd',
            'OAnimation': 'oAnimationEnd',
            'msAnimation': 'MSAnimationEnd',
            'animation': 'animationend'
        };

        // dom
        base.$el = $(el).eq(0);
        base.$beforeItem = null;
        base.$afterItem = null;

        // var
        base.beforeIdx = 0;
        base.afterIdx = 0;
        base.countAnims = 0;
        base.dir = 'next';
        base.isAnimating = false;
        base.isHover = false;
        base.isPause = false;
        base.timerAutoplay = null;
        base.NAMESPACE_ANIMATION = '.animation';

        // animation end event name
        if (support.animations) {
            base.animEndEventName = animEndEventNames[Modernizr.prefixed('animation')] + base.NAMESPACE_ANIMATION;
        }

        // class
        base.ACTIVECLASS = 'current';
        base.REGFX = /\bfx.*?\b/g;

        // add a reverse reference to the DOM object
        base.$el.data('epass', base);

        // set before item ==================================================
        base.setBeforeItem = function(idx) {
            base.$beforeItem = $(base.opts.itemEl, base.$coreEl).eq(idx);
        };

        // set current item ==================================================
        base.setAfterItem = function(idx) {
            base.$afterItem = $(base.opts.itemEl, base.$coreEl).eq(idx);
        };

        // set current item ==================================================
        base.toggleActiveClass = function(idx) {
            var _index = (idx !== undefined) ? idx : base.afterIdx;
            $(base.opts.itemEl, base.$coreEl).eq(_index).addClass(base.ACTIVECLASS).siblings().removeClass(base.ACTIVECLASS);
        };

        // reset item scaffold for effect ==================================================
        base.resetScaffold = function() {

            $(base.opts.itemEl, base.$el).each(function(i) {
                $(this).off(base.NAMESPACE_ANIMATION).empty().append($(base.opts.itemEl, base.$clone).eq(i).html());
            });

            if (base.fx == 'fxSlide') { // slide

                // reset
                $(base.opts.itemEl, base.$coreEl).each(function(i) {
                    $(this).css('left', base.itemWidth * i);
                });

                // rebuild index, class
                base.afterIdx = 0;
                base.toggleActiveClass(0);

            } else if (base.fx == 'fxFade') {

                // rebuild
                base.$beforeItem.show().siblings().hide();

            } else {

                // rebuild
                base.$beforeItem.show();
                base.$beforeItem.siblings().show();

            }
        };

        // change animation effect ==================================================
        base.setEffect = function() {

            // get select option
            base.fx = (base.$effectSel.length) ? $('option:selected', base.$effectSel).val() : base.fx;

            // but ie7 polyfill
            base.fx = (!support.animations && base.fx !== 'fxFade') ? 'fxSlide' : base.fx;

            // adjust fx effect class
            base.$el.attr('class', base.$el.attr('class').replace(base.REGFX, '')).addClass(base.fx);
            base.$items.removeAttr('style').removeClass(base.ACTIVECLASS);

            // reset item
            base.setBeforeItem(base.afterIdx);
            base.toggleActiveClass(base.afterIdx);

            // reset item scaffold for effect
            base.resetScaffold();

            // set pager
            base.setPager();

        };

        // listener animation end ==================================================
        base.onEndAnimationCurrentItem = function() {
            $(this).removeClass(base.ACTIVECLASS).removeClass(base.dir == 'next' ? 'navOutNext' : 'navOutPrev');
            ++base.countAnims;
            base.isAnimating = (base.countAnims >= 2) ? false : true;
        };

        // listener current animation end ==================================================
        base.onEndAnimationNextItem = function() {
            $(this).addClass(base.ACTIVECLASS).removeClass(base.dir == 'next' ? 'navInNext' : 'navInPrev');
            ++base.countAnims;
            base.isAnimating = (base.countAnims >= 2) ? false : true;

            // user custom func on after animate
            if (base.isAnimating === false) {
                base.onAfterSlide();
            }

        };

        // on css ==================================================
        base.fxCss = function(dir) {
            // get current item animation
            base.$beforeItem.one(base.animEndEventName, base.onEndAnimationCurrentItem);

            // get next item animation
            base.$afterItem.one(base.animEndEventName, base.onEndAnimationNextItem);

            // add animation class
            base.$beforeItem.addClass(base.dir == 'next' ? 'navOutNext' : 'navOutPrev');
            base.$afterItem.addClass(base.dir == 'next' ? 'navInNext' : 'navInPrev');
        };

        // on fade ==================================================
        base.fxFade = function(dir) {
            base.$items.not(base.$afterItem).stop().fadeOut();
            base.$afterItem.stop().fadeIn(function() {

                base.setBeforeItem(base.afterIdx);
                base.toggleActiveClass(base.afterIdx);

                base.isAnimating = false;
                // user custom func on after animate
                base.onAfterSlide();

            });
        };

        // on slide ==================================================
        base.fxSlide = function(dir) {

            var _key = base.afterIdx - base.beforeIdx;
            var _abskey = Math.abs(_key);

            if (base.dir == 'next') { // next

                // for last one
                _abskey = _key < 0 ? 1 : _abskey;

                base.$items.each(function(i) {

                    // animate
                    $(this).stop().animate({
                        'left': '-=' + base.itemWidth * _abskey
                    }, base.opts.nonCssDuration, base.opts.nonCssEasing, function() {

                        // for last one
                        if (i == base.itemsCount - 1) {

                            // and move element
                            $(base.opts.itemEl, base.$coreEl).slice(0, _abskey).each(function(i) {
                                var _fromPos = (base.itemWidth * (base.itemsCount - _abskey));
                                $(this).css({
                                    'left': _fromPos + base.itemWidth * i
                                }).appendTo(base.$coreEl);
                            });

                            // end
                            base.toggleActiveClass(0);
                            base.isAnimating = false;

                            // user custom func on after animate
                            base.onAfterSlide();

                        }
                    });
                });

            } else if (base.dir == 'prev') { // prev

                // for first one
                _abskey = _key > 0 ? 1 : _abskey;

                // move element
                $(base.opts.itemEl, base.$coreEl).slice(base.itemsCount - _abskey).prependTo(base.$coreEl).each(function(i) {
                    var _fromPos = -(base.itemWidth * _abskey);
                    $(this).css({
                        'left': _fromPos + base.itemWidth * i
                    });
                });

                // and animate
                base.$items.each(function(i) {
                    $(this).stop().animate({
                        'left': '+=' + base.itemWidth * _abskey
                    }, base.opts.nonCssDuration, base.opts.nonCssEasing, function() {

                        if (i == base.itemsCount - 1) {

                            // current
                            base.toggleActiveClass(0);
                            base.isAnimating = false;

                            // user custom func on after animate
                            base.onAfterSlide();

                        }
                    });
                });
            }
        };

        // do navigate ==================================================
        base.navigate = function(dir) {

            // block running
            if (base.isAnimating) return false;

            // reinit repeat autoplay 
            base.repeatAutoplay('clear');

            // set var
            base.dir = dir;
            base.isAnimating = true;

            // reset var
            base.countAnims = 0;

            // set before item
            base.beforeIdx = base.afterIdx;

            // detect direction
            if (base.dir == 'next') {
                base.afterIdx = base.afterIdx < base.itemsCount - 1 ? base.afterIdx + 1 : 0;
            } else if (base.dir == 'prev') {
                base.afterIdx = base.afterIdx > 0 ? base.afterIdx - 1 : base.itemsCount - 1;
            } else {
                if (base.afterIdx < dir) {
                    base.dir = 'next';
                } else {
                    base.dir = 'prev';
                }
                base.afterIdx = dir;
            }

            // set item
            base.setBeforeItem(base.beforeIdx);
            base.setAfterItem(base.afterIdx);

            // active effect
            if (base.fx == 'fxSlide') {
                base.fxSlide(dir);
            } else if (base.fx == 'fxFade') {
                base.fxFade(dir);
            } else {
                base.fxCss(dir);
            }

            // set pager
            base.setPager();

        };

        // repeat autoplay ==================================================
        base.repeatAutoplay = function() {

            if (base.opts.autoplay === true) {
                clearTimeout(base.timerAutoplay);
                base.timerAutoplay = setTimeout(function() {
                    if (base.isHover !== true && base.isAnimating !== true && base.isPause !== true) {
                        base.navigate('next');
                    }
                    base.repeatAutoplay();
                }, base.opts.durationAutoplay);

            }
        };

        // init autoplay ==================================================
        base.initAutoplay = function() {

            // hover blocking
            base.$el.hover(function() {
                base.isHover = true;
            }, function() {
                base.isHover = false;
            });

            // first init autoplay
            var timerFirstInitAutoplay = setTimeout(function() {
                clearTimeout(timerFirstInitAutoplay);
                base.repeatAutoplay();
            }, base.opts.delayAutoplay);

        };

        // init pager ==================================================
        base.setPager = function() {

            // if pager empty
            if (!$('a', base.$pager).length) {
                for (var i = 0, j = base.itemsCount; i < j; i++) {
                    var $pager = $('<a href="javascript:;"><span>' + (i + 1) + '</span></a>');
                    $pager.appendTo(base.$pager);
                }
            }

            $('span.item', base.$page).text('index:' + base.afterIdx + '/ oldItem: ' + base.$beforeItem.index());

            // activation
            $('a', base.$pager).eq(base.afterIdx).addClass(base.ACTIVECLASS).siblings().removeClass(base.ACTIVECLASS);
        };

        // lazyLoad ==================================================
        base.lazyLoad = function(img, func) {

            if (img !== undefined && typeof func !== 'undefined') {

                var _timerLoad = null;
                var _blocker = 0;
                var lazyLoadCore = function(i) {

                    var $loadEl = img.eq(i);

                    clearTimeout(_timerLoad);

                    if ($loadEl.length) {

                        _timerLoad = setTimeout(function() {
                            clearTimeout(_timerLoad);
                            lazyLoadCore(i + 1);
                        }, 100);

                        $('<img />', {
                            'src': $loadEl.attr('src')
                        }).on('load', function() {
                            lazyLoadCore(i + 1);
                        }).error(function() {
                            console.log('no.' + i + ' image load error.');
                            lazyLoadCore(i + 1);
                        });

                    } else {

                        if (_blocker < 1) {
                            _blocker++;
                            func();
                        }
                    }
                    return false;
                };

                lazyLoadCore(0);

            } else {
                console.log('img or func error');
            }
        };

        // set gesture ==================================================
        base.setGesture = function() {
            if (hasTouch !== true) { // no touchable device

                base.$items.off('drag.finger');
                base.$items.on('drag.finger', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.direction == 1) {
                        base.navigate('prev');
                    } else if (e.direction == -1) {
                        base.navigate('next');
                    }
                }).on('dragstart', function(e) {
                    return false;
                }).addClass('cursor-grab');

            } else { // touchable device

                base.$items.off('flick.finger');
                base.$items.on('flick.finger', function(e) {
                    if (e.orientation == 'horizontal') {
                        e.preventDefault();
                        e.stopPropagation();
                        if (e.direction == 1) {
                            base.navigate('prev');
                        } else if (e.direction == -1) {
                            base.navigate('next');
                        }
                    }
                });
            }
        };

        // call on after slide ==================================================
        base.onAfterSlide = function() {

            if (typeof(base.opts.onAfterSlide) == "function") {
                var _timerAfter = setTimeout(function() {
                    base.opts.onAfterSlide(base);
                    clearTimeout(_timerAfter);
                }, base.opts.delayOnAfterSlide);
            }
        };

        // on init ==================================================
        base.onInit = function() {

            base.$el.after(base.$page);

            // set effect first
            base.$effectSel.on('change', base.setEffect);
            base.setEffect();

            // set autoplay
            base.initAutoplay();

            // bind buttons
            base.$navNext.on('click', function(e) {
                e.preventDefault();
                base.navigate('next');
            });

            base.$navPrev.on('click', function(e) {
                e.preventDefault();
                base.navigate('prev');
            });

            $('a', base.$pager).on('click', function(e) {
                e.preventDefault();
                if (!$(this).hasClass(base.ACTIVECLASS)) {
                    base.navigate($(this).index());
                }
            });

            // overflow
            if (base.opts.overflow === false) {
                base.$coreEl.css('overflow', 'hidden');
            }

            // bind gesture
            if (base.opts.enableGesture === true) {
                base.setGesture();
            }

            // user custom func
            if (typeof(base.opts.onInit) == "function") {
                var _timerInit = setTimeout(function() {
                    base.opts.onInit(base);
                    clearTimeout(_timerInit);
                }, base.opts.delayOnInit);
            }
        };

        // init ==================================================
        base.init = (function() {

            // show for flicking polyfill ( css hidden );
            base.$el.show();

            // set param
            base.param = param;

            // set opts
            base.defaults = $.epass.slider.defaults;
            base.opts = $.extend({}, base.defaults, options);

            // set el
            base.$effectSel = $(base.opts.effectSelEl);
            base.$coreEl = $(base.opts.coreEl, base.$el);
            base.$clone = base.$coreEl.clone();
            base.$items = $(base.opts.itemEl, base.$coreEl);
            base.$itemImgs = $('img', base.$items);
            base.$navPrev = $(base.opts.prevNavEl);
            base.$navNext = $(base.opts.nextNavEl);
            base.$pager = $(base.opts.pagerEl);

            base.$page = $('<div class="pagination"><span class="item">1</span>/<span class="total">' + base.$items.length + '</span></div>');

            // set var
            base.fx = base.opts.fx;
            base.itemsCount = base.$items.length;
            base.itemWidth = parseInt(base.$items.eq(0).width(), 10);

            // init with lazyload
            base.lazyLoad(base.$itemImgs, base.onInit);

        }()); // run initializer

    };

    // get defualt opts ==================================================
    $.epass.slider.defaults = {

        /* effects are
        fxSlide
        fxFade

        fxCorner
        fxVScale
        fxFall
        fxFPulse
        fxRPulse
        fxHearbeat
        fxCoverflow
        fxRotateSoftly
        fxDeal
        fxFerris
        fxShinkansen
        fxSnake
        fxShuffle
        fxPhotoBrowse
        fxSlideBehind
        fxVacuum
        fxHurl

        fxSoftScale
        fxPressAway
        fxSideSwing
        fxFortuneWheel
        fxSwipe
        fxPushReveal
        fxSnapIn
        fxLetMeIn
        fxStickIt
        fxArchiveMe
        fxVGrowth
        fxSlideBehind
        fxSoftPulse
        fxEarthquake
        fxCliffDiving

        fxSlideForward
        fxTableDrop
        fxSlideIt
        fxBottleKick
        fxShelf
        */

        fx: 'fxFade',
        nonCssDuration: 400,
        nonCssEasing: 'easeInOutExpo',
        coreEl: '.itemwrap',
        effectSelEl: '#fxSelect',
        itemEl: 'li',
        navEl: '.nav',
        prevNavEl: 'a.prev',
        nextNavEl: 'a.next',
        pagerEl: '.pager',

        // set var
        overflow: true,
        autoplay: true,
        enableGesture: true,
        delayOnInit: 50,
        delayOnAfterSlide: 50,
        delayAutoplay: 0,
        durationAutoplay: 4000,
        onInit: null,
        onAfterSlide: null
    };

    // make object plugin ==================================================
    $.fn.epassSlider = function(options, param) {

        return this.each(function() {
            (new $.epass.slider(this, options, param));
        });
    };

    // })(jQuery);
})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);

/*!
 * easing functions
 * based on easing equations from Robert Penner (http://www.robertpenner.com/easing)
 */
(function() {

    // easing defaults
    $.easing.def = "easeInOutQuad";

    var baseEasings = {};

    $.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(i, name) {
        baseEasings[name] = function(p) {
            return Math.pow(p, i + 2);
        };
    });

    $.extend(baseEasings, {
        Circ: function(p) {
            return 1 - Math.sqrt(1 - p * p);
        },
        Elastic: function(p) {
            return p === 0 || p === 1 ? p : -Math.pow(2, 8 * (p - 1)) * Math.sin(((p - 1) * 80 - 7.5) * Math.PI / 15);
        },
        Back: function(p) {
            return p * p * (3 * p - 2);
        }
    });

    $.each(baseEasings, function(name, easeIn) {
        $.easing["easeIn" + name] = easeIn;
        $.easing["easeOut" + name] = function(p) {
            return 1 - easeIn(1 - p);
        };
        $.easing["easeInOut" + name] = function(p) {
            return p < 0.5 ?
                easeIn(p * 2) / 2 :
                1 - easeIn(p * -2 + 2) / 2;
        };
    });
})();
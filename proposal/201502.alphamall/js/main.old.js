$(function() {

    'use strict';

    var timerDocLoaded = setTimeout(function() {
        $('body').addClass('loaded');
    }, 500);

    // var isHover = false,
    //     timerNavSub = null,
    //     _speed = 150;

    var $window = $(window),
        $html = $('html'),
        $body = $('body'),
        $container = $('#container');

    var csstransitions = (!$html.attr('class').match('no-csstransitions')) ? true : false;


    var initAsideHeight = function() {
        $('#aside').removeAttr('style');
        $('#aside').css({
            height: $(document).height()
        });
    };

    initAsideHeight();

    $('#utility .toggler .button').on('click', function(e) {
        e.preventDefault();
        $('body').toggleClass('active-utility');
        initAsideHeight();
    });

    // var $item = $('.item');
    // var _timer = null;
    // var initItem = function() {
    //     $item.each(function() {

    //         var $this = $(this);
    //         if ($this.is(':inview')) {
    //             $this.addClass('inview');

    //             // for oldie
    //             if (csstransitions !== true && !$this.hasClass('done')) {
    //                 var j = $this.data('index') * 300;
    //                 $this.stop().delay(j).animate({
    //                     'opacity': 1
    //                 }, 300, function() {
    //                     $this.addClass('done');
    //                 });
    //             }
    //         } else {
    //             $this.removeClass('inview');
    //         }
    //     });
    // };

    // // for oldie
    // if (csstransitions !== true) {
    //     $item.css({
    //         'opacity': 1
    //     });
    // }

    // $window.on('load scroll', function() {
    //     initItem();
    // });

    // $('#slidesA').find('.slide').eq(0).addClass('active-slide').addClass('visible-silde');

    // var timerSlideA = setTimeout(function() {
    //     $('#slidesA').slidesjs({
    //         width: 992,
    //         height: 480,
    //         play: {
    //             auto: false
    //         },
    //         navigation: false,
    //         pagination: {
    //             effect: "slide"
    //         },
    //         callback: {
    //             loaded: function(number) {},
    //             start: function(number) {},
    //             complete: function(number) {
    //                 $('#slidesA').find('.slide').eq(number - 1).addClass('active-slide').siblings().removeClass('active-slide');
    //                 // $('#slidesA').find('.slide').eq(number - 1).addClass('visible-slide').siblings().removeClass('visible-slide');
    //             }
    //         }
    //     });
    // }, 500);

    // $('#slidesB').slidesjs({
    //     width: 992,
    //     height: 480,
    //     play: {
    //         auto: false
    //     },
    //     navigation: false,
    //     pagination: {
    //         effect: "slide"
    //     },
    //     callback: {
    //         loaded: function(number) {},
    //         start: function(number) {},
    //         complete: function(number) {
    //             $('#slidesB').find('.slide').eq(number - 1).addClass('active-slide').siblings().removeClass('active-slide');
    //             // $('#slidesB').find('.slide').eq(number - 1).addClass('visible-slide').siblings().removeClass('visible-slide');
    //         }
    //     }
    // }, 1000);

    // var timerPopup = setTimeout(function() {
    //     $('#popup .slidejs-wrapper').slidesjs({
    //         width: 825,
    //         height: 575,
    //         play: {
    //             auto: false
    //         },
    //         navigation: false,
    //         pagination: {
    //             effect: "slide"
    //         }
    //     });
    // }, 1500);







    // /**
    //  * gnb 셋팅
    //  */


    // $('#gnb a.js-trigger').on('click', function(e) {
    //     e.preventDefault();

    //     var timerSlide = null;
    //     var $this = $(this);
    //     var _index = $this.parent().index();
    //     var $target = $('#sub .sub').eq(_index);

    //     $this.find('img').imgToggle();
    //     $this.parent().siblings().find('img').imgToggle({
    //         off: '_on',
    //         on: '_off'
    //     });

    //     $target.addClass('active-holder').siblings().removeClass('active-holder');
    //     $('#sub').find('.slide').removeClass('visible');
    //     $target.find('.active').addClass('visible');

    // });
    // //.eq(0).trigger('click');




    // /* banner */
    // $('#banner a').on('click', function(e) {
    //     var $this = $(this);
    //     var $sub = $this.next();

    //     $this.toggleClass('active');

    //     if ($this.hasClass('active')) {
    //         $this.find('img').imgToggle();
    //         $sub.slideDown(300, function() {
    //             $sub.find('img').animate({
    //                 opacity: 1
    //             }, 300);
    //         });
    //     } else {
    //         $this.find('img').imgToggle({
    //             off: '_on',
    //             on: '_off'
    //         });
    //         $sub.slideUp(300, function() {
    //             $sub.find('img').animate({
    //                 opacity: 0
    //             }, 300);
    //             $window.trigger('scroll');
    //         });
    //     }
    // });

    // /* popup */
    // $('#lecture a').on('click', function(e) {
    //     e.preventDefault();
    //     $body.addClass('active-popup');
    //     $('#popup .core img').animate({
    //         // opacity: 1
    //     }, 500);
    // });
    // $('#popup a.close').on('click', function(e) {
    //     e.preventDefault();
    //     $('#popup .core img').animate({
    //         // opacity: 0
    //     }, 0, function() {
    //         $body.removeClass('active-popup');
    //     });
    // });

    // // roll over
    // $('.rollover img').imgHover();






    // var hideNavSub = function() {
    //     timerNavSub = setTimeout(function() {
    //         clearTimeout(timerNavSub);
    //         if (isHover === false) {
    //             $('div.d2', $gnb).stop().slideUp(_speed);
    //             $gnb.stop().animate({
    //                 'height': 58
    //             }, _speed);
    //             $('a img', $gnb).imgToggle({
    //                 off: '_on',
    //                 on: '_off'
    //             });
    //         }
    //     }, 500);
    // };

    // $('a.d1', $gnb).hover(function() {
    //     var $this = $(this);
    //     var $d2 = $this.next();

    //     isHover = true;

    //     if ($d2.length) {
    //         $this.find('img').imgToggle();
    //         $d2.stop().slideDown(_speed);
    //         $gnb.stop().animate({
    //             'height': 197
    //         }, _speed);
    //     }
    //     $gnb.one('mouseleave', function() {
    //         isHover = false;
    //         hideNavSub();
    //     });
    // });

    // searchform 
    // $('.utility a', $logo).on('click', function() {
    //     var $this = $(this),
    //         $img = $this.find('img'),
    //         $target = $($this.data('target'));

    //     var _height = ($this.data('target') == '#searchForm') ? 598 : 650;

    //     $this.parent().siblings().children().removeClass('active').find('img').imgToggle({
    //         off: '_on',
    //         on: '_off'
    //     });

    //     $this.toggleClass('active');

    //     if ($target.length) {
    //         if ($this.hasClass('active')) {
    //             $img.imgToggle();
    //             $target.siblings().stop().animate({
    //                 opacity: 0.4,
    //                 height: 0
    //             }, _speed);
    //             $target.stop().animate({
    //                 opacity: 1,
    //                 height: _height
    //             }, _speed);
    //         } else {
    //             $img.imgToggle({
    //                 off: '_on',
    //                 on: '_off'
    //             });
    //             $utilityWrapper.children().stop().animate({
    //                 opacity: 0.4,
    //                 height: 0
    //             }, _speed);
    //         }
    //     }
    // });

    // jumbotron
    // $('#jumbotron .swiper-container').activeSwiper({
    //     slidesPerView: 1,
    //     paginationClass: '#jumbotron .swiper-pagination',
    //     onSlideChangeEnd: function(swiper) {
    //         // alert(swiper.activeIndex);
    //     }
    // });

    // theme change
    // $('#themeMenu .core a').on('click', function(e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     var _class = $this.data('class');
    //     $('#jumbotron').removeClass().addClass(_class);
    //     $('#jumbotron .toggler').trigger('click');
    // });

    // theme toggle
    // $('#jumbotron .toggler').on('click', function(e) {
    //     e.preventDefault();

    //     var $this = $(this),
    //         $img = $this.find('img');

    //     $this.toggleClass('active');

    //     if ($this.hasClass('active')) {
    //         $('#jumbotron .swiper-pagination').css('z-index', '0');
    //         $('#themeMenu').stop().animate({
    //             opacity: 1,
    //             marginTop: -447
    //         }, _speed, function() {

    //         });
    //     } else {
    //         $('#jumbotron .swiper-pagination').css('z-index', '100');
    //         $('#themeMenu').stop().animate({
    //             opacity: 0.6,
    //             marginTop: -52
    //         }, _speed, function() {

    //         });
    //     }
    // });

    // $('#bestHover').hover(function() {
    //     $('#bestSeasonFood').addClass('hover');
    //     $('.hover-image').one('click', function() {
    //         $(this).parents('.hover').removeClass('hover');
    //     });
    // });

    // sticky
    // var headerStickyTop = $header.offset().top;

    // click
    // $('.js-trigger', $utilityWrapper).on('click', function(e) {
    //     e.preventDefault();
    //     var $this = $(this),
    //         $target = $($this.data('target')).find('a').eq($this.data('index'));
    //     if ($target.length) {
    //         $target.trigger('click');
    //     }
    // });

    // slide tab
    // $('#slideList .tab a').on('click', function(e) {
    //     e.preventDefault();
    //     $('#slideList').removeClass().addClass($(this).data('class'));
    // });

    // slide pop toggle
    // $('#slideCore a.btn').on('click', function(e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     $('#slideCore').removeClass().addClass($this.data('class'));
    //     $('#slideCore .pop a').one('click', function(e) {
    //         e.preventDefault();
    //         $('#slideCore').removeClass();
    //     });
    // });

    // gift
    // $('#giftCore a.js-class').on('click', function(e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     $('#giftCore').removeClass().addClass($this.data('class'));
    // });

    // $window.on('scroll', function() {
    //     var windowTop = $window.scrollTop();
    //     if (headerStickyTop < windowTop) {
    //         $header.css({
    //             position: 'fixed',
    //             top: 0
    //         });
    //     } else {
    //         $header.css({
    //             position: 'absolute',
    //             top: headerStickyTop
    //         });
    //     }
    // });

});
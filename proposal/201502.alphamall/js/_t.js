base.fxSlide = function(e) {
    var t = base.afterIdx - base.beforeIdx;
    var n = Math.abs(t);
    if (base.dir == "next") {
        n = t < 0 ? 1 : n;
        base.$items.each(function(e) {
            $(this).stop().animate({
                left: "-=" + base.itemWidth * n
            }, base.opts.nonCssDuration, base.opts.nonCssEasing, function() {
                if (e == base.itemsCount - 1) {
                    base.$coreEl.children(base.opts.itemEl).slice(0, n).each(function(e) {
                        var t = base.itemWidth * (base.itemsCount - n);
                        $(this).css({
                            left: t + base.itemWidth * e
                        }).appendTo(base.$coreEl)
                    });
                    base.toggleActiveClass(0);
                    base.isAnimating = false;
                    base.onAfterSlide()
                }
            })
        })
    } else if (base.dir == "prev") {
        n = t > 0 ? 1 : n;
        base.$coreEl.children(base.opts.itemEl).slice(base.itemsCount - n).prependTo(base.$coreEl).each(function(e) {
            var t = -(base.itemWidth * n);
            $(this).css({
                left: t + base.itemWidth * e
            })
        });
        base.$items.each(function(e) {
            $(this).stop().animate({
                left: "+=" + base.itemWidth * n
            }, base.opts.nonCssDuration, base.opts.nonCssEasing, function() {
                if (e == base.itemsCount - 1) {
                    base.toggleActiveClass(0);
                    base.isAnimating = false;
                    base.onAfterSlide()
                }
            })
        })
    }
}
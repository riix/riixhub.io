(function($, window, document, ua) {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    /**
     * CSS3 토글 슬라이더
     * @param  {[type]} options 옵션
     */
    $.fn.toggleSlidr = function(options) {

        var base = [];

        var _animEndEventNames = EPASS.animEndEventName || 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd';

        var setting = {
            css3animation: true,
            autoPlay: true,
            delay: 3000,
            baseZindex: 100,
            duration: 300,
            mouse: true,
            pager: null,
            nav: null,
            reverse: false,
            animEndEventNames: _animEndEventNames
        };

        // set opts
        base.opts = $.extend(setting, options);
        base.opts.css3animation = (EPASS.support.transforms3d !== true) ? false : base.opts.css3animation; // fix animation support

        // global Var
        base.$el = $(this);

        base.$beforeItem = null;
        base.$afterItem = null;

        var _timerAutoPlay = null,
            _timerDuration = null,
            _isAnimating = false,
            _isHover = false,
            _isPause = false,
            _beforeIdx = 0,
            _afterIdx = 0,
            _itemsLength = 0;

        /**
         * 페이징등, 인덱싱된 앵커를 찾는 함수
         * @param  {[type]} _parent 탐지 대상
         * @param  {[type]} _idx    생략시 전체 반환
         * var $anchor = getIndexedAnchor($pager);
         * var $currentAnchor = getIndexedAnchor($pager, 0);
         */
        var getIndexedAnchor = function(_parent, _idx) {
            var $parent = _parent,
                $el = $parent.find('a');
            if ($el.eq(0).parent('li').length) {
                $el = $parent.find('li');
                $el = (_idx === undefined) ? $el.find('a') : $el.eq(_idx).find('a');
            } else if (_idx !== undefined) {
                $el = $el.eq(_idx);
            }
            return $el;
        };

        /**
         * 인덱스값 범위 내 반환
         * @param  {[type]} _idx 받아올 인덱스
         * @param  {[type]} _max 제한범위
         */
        var idxProof = function(_idx, _max) {
            _idx = (_idx < 0) ? _max - 1 : _idx;
            _idx = (_idx < _max) ? _idx : 0;
            return _idx;
        };

        /**
         * 페이징 등 객체의 인덱스 순서 추출
         * @param  {[type]} _el 추출할 객체
         */
        var getElemIdx = function(_el) {
            var $el = $(_el),
                $li = $el.parent('li'),
                _result;
            $el = ($li.length) ? $li : $el;
            _result = ($el.length) ? $el.index() : 0;
            return _result;
        };


        /** 페이징 셋팅 */
        var _setPaginate = function() {
            if (base.opts.pager && _itemsLength !== getIndexedAnchor(base.$pager).length) {
                var $el = getIndexedAnchor(base.$pager, 0),
                    $pagerInner = $el.parent().empty();
                for (var i = 0; i < _itemsLength; i++) {
                    $el.clone().appendTo($pagerInner);
                }
            }
        };

        /** 페이징 */
        var _doPaginate = function() {
            if (base.opts.pager !== undefined) {
                getIndexedAnchor(base.$pager, _afterIdx).currentActive(); // 해당 페이지네이트  활성화
            }
        };

        /** 페이지 이동 */
        var _togglePage = function(_idx) {

            if (_isAnimating === true) return false;

            // set index
            _beforeIdx = (_beforeIdx !== _afterIdx) ? _afterIdx : _itemsLength - 1;
            _afterIdx = idxProof(_idx, _itemsLength);

            // set item and z-index, poiner events
            base.$beforeItem = base.$items.eq(_beforeIdx).setZindex(2, base.opts.baseZindex).css('pointer-events', 'none');
            base.$afterItem = base.$items.eq(_afterIdx).setZindex(3, base.opts.baseZindex).css('pointer-events', 'auto');

            // paginate
            _doPaginate();

            if (base.opts.css3animation === true) { // css3 지원시

                base.$beforeItem.removeClass('active');
                base.$afterItem.addClass('ready');

                _isAnimating = true;

                _timerDuration = setTimeout(function() {
                    clearTimeout(_timerDuration);
                    base.$beforeItem.removeClass('ready');
                    base.$afterItem.addClass('active');
                    _isAnimating = false;
                }, base.opts.duration);

            } else { // css3 미지원시

                base.$beforeItem.hide();
                base.$afterItem.fadeIn(200).promise().done(function() {
                    base.$afterItem.currentActive();
                    _isAnimating = false;
                });

            }

            // init autoplay
            _autoPlay();

        };

        /** 자동실행  */
        var _autoPlay = function() {
            clearTimeout(_timerAutoPlay);
            if (base.opts.autoPlay === true) {
                _timerAutoPlay = setTimeout(function() {
                    if (_isHover !== true) {
                        _navigate('next');
                    }
                }, base.opts.delay);
            }
        };

        /**
         * 이전, 다음 페이지 이동
         * @param  {[type]} _dir [description]
         */
        var _navigate = function(_dir) {
            if (_isAnimating !== true) {
                var _idx = _afterIdx;
                if (_dir.match('prev')) {
                    _idx--;
                } else {
                    _idx++;
                }
                _togglePage(_idx);
            }
        };

        /** 이벤트 핸들러  */
        var _setHandler = function() {

            base.$el.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
                _autoPlay();
            });

            $('a', base.$nav).on('click.slide', function(e) {
                e.preventDefault();
                _navigate($(this).attr('class'));
            });

            $('a', base.$pager).off().on('click.slide', function(e) {
                e.preventDefault();
                var _idx = getElemIdx($(this));
                if (_idx !== _afterIdx) {
                    _togglePage(_idx);
                } else {
                    return false;
                }
            });

            if (EPASS.hasTouch !== true && base.opts.mouse === true) { // no touchable device

                base.$items.on('drag.slide', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.direction == 1) {
                        _navigate('prev');
                    } else if (e.direction == -1) {
                        _navigate('next');
                    }
                }).on('dragstart', function(e) {
                    return false;
                });

                base.$items.css({
                    'user-select': 'none',
                    'user-drag': 'none'
                });

            } else if (EPASS.hasTouch === true) { // touchable device

                base.$items.on('flick.slide', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.orientation == 'horizontal') {
                        if (e.direction == 1) {
                            _navigate('prev');
                        } else if (e.direction == -1) {
                            _navigate('next');
                        }
                    }
                }).trigger('flickr.slide');

            }

        };

        /** 코어  */
        var _core = function() {

            // set animation class
            if (base.opts.css3animation !== true) {
                base.$el.addClass('no-animation');
            }

            _setHandler();
            _togglePage(0);
        };

        /** 초기화 */
        var _init = (function() {

            // sel elements
            base.$items = $('.item', base.$el),
            base.$pager = $(base.opts.pager),
            base.$nav = $(base.opts.nav);

            // set custom data
            base.$el.data('slider', base);

            // set z-index in order
            $.each([
                base.$el,
                base.$items,
                base.$items.eq(0),
                base.$nav,
                base.$pager
            ], function(i, _el) {
                _el.setZindex(i, base.opts.baseZindex);
            });

            // set vars.
            _itemsLength = base.$items.length;

            // set paginate
            _setPaginate();

            // do core
            _core();

        })();

        return this;

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
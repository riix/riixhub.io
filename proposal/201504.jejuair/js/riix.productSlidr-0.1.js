/**
 * 전시 상품 리스트 슬라이더
 * author: riix@epasscni.com
 */

;
(function($, window, document, ua) {

    'use strict';

    //  전시 상품 슬라이더
    $.fn.productSlidr = function(options) {

        var setting = {
            json: null,
            formatter: function($item, _idx) {},
            infinite: true,
            imagesLoaded: true,
            randomActive: true,
            autoPlay: false,
            pauseOnHover: true,
            resizable: false,
            vertical: false,
            delay: 3500, // auto play delay
            duration: 600, // animation duration
            tabDelay: 0,
            visibleLength: 5,
            slideLength: 0,
            wrapEl: '.itemwrap',
            tabEl: '.subject',
            tabsPosition: 'right',
            counter: '.counter',
            navNext: '.next',
            navPrev: '.prev',
            pager: '.pager',
            sliderHeight: 'auto',
            indicatorURL: './img/module/ajax-loader.gif',
            onInit: null,
            onComplete: null,
            onSlideStart: null,
            onSlideEnd: null
        };

        setting.formatter = function($item, _idx) {
            var _html = '';
            _html += '<li class="item">';
            _html += '<div class="thumb">';
            _html += '<a href="' + $item.src + '"><img src="' + $item.thumb + '" alt="" /></a>';
            _html += '</div>';
            _html += '<div class="desc">';
            _html += '<p class="brand">' + $item.brand + '</p>';
            _html += '<p class="name">' + $item.name + '</p>';
            _html += '<p class="price">';
            _html += '<span class="before">' + $item.priceBefore + ' 원</span>';
            _html += '<span class="after">' + $item.priceAfter + ' 원</span>';
            _html += '</p>';
            _html += '</div>';
            _html += '</li>';
            return _html;
        }

        var opts = $.extend(setting, options);

        var base = [],
            slide = [],
            page = [];

        var _isVertical = opts.vertical,
            _isInfinite = opts.infinite,
            _visibleLength = opts.visibleLength,
            _slideLength = opts.slideLength,
            _isAuto = opts.autoPlay;

        var _isHover = false,
            _isAnimating = false,
            _delayAutoPlay = 0,
            _timerAutoPlay = null;

        var _itemHeight = (opts.sliderHeight !== 'auto' && typeof opts.sliderHeight === 'string') ? parseInt(opts.sliderHeight, 10) : base.$el.height();

        _slideLength = (_slideLength == 0 || _slideLength > _visibleLength) ? _visibleLength : _slideLength; // bulletProof

        base.$el = $(this);
        base.$navPrev = $(opts.navPrev, base.$el);
        base.$navNext = $(opts.navNext, base.$el);
        base.$pager = $(opts.pager, base.$el);
        base.$counter = $(opts.counter, base.$el);

        base.isPause = (_isAuto !== true) ? true : false;

        // preloader
        base.$preloader = $('<div class="js-productSlidr-preloader preloader"><img src="' + opts.indicatorURL + '" alt="loading..." /><p>0%</p></div>').css({
            'position': 'absolute',
            'left': '50%',
            'top': '50%',
            'margin': '-23px 0 0 -16px',
            'text-align': 'center'
        });

        /** common method *******************************************************************************************/

        // call callback function
        // callFunc(opts.onSlideEnd);
        var callFunc = function(_callback, _param, _delay) {
            _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
            if ($.isFunction(_callback)) {
                _param = (_param === null) ? '' : _param;
                _delay = _delay || 50;
                setTimeout(function() {
                    _callback.call(null, base, _param);
                }, _delay);
            } else {
                return false;
            }
        };

        // throttle
        var throttle = (function() {
            var _timerThrottle;
            return function(_fn, _delay) {
                clearTimeout(_timerThrottle);
                _timerThrottle = setTimeout(_fn, _delay);
            };
        })();

        // do pull up own's height half
        var doPullUp = function(_el) {
            if (_el === null || !_el.length) return false;
            $.each(_el, function() {
                var $this = $(this),
                    marginTop = -($this.outerHeight() / 2);
                $this.css({
                    'margin-top': marginTop
                });
            });
        };

        // 동일한 패턴의 엘리먼트 집합 인덱스를 구함
        // alert(indexType($this, '.subject'));
        var indexType = function(_el, _selector) {
            var $this = _el,
                result;
            if (_selector === undefined) {
                if ($this.attr('class') !== undefined) { // class
                    _selector = '.' + $this.attr('class').split(' ')[0];
                } else { // tagname
                    _selector = $this.get(0).tagName.toLowerCase();
                }
            }
            result = $this.parent().children(_selector).index($this);
            return result;
        };

        // set absolute tabs position
        // setAbsoluteTapPosition(_el, opts.tabsPosition); // set tabs position
        var setAbsoluteTapPosition = function(_el, _pos) {
            if (_el === null || !_el.length) return false;
            var i, len;
            var marginTop = -(_el.eq(0).outerHeight()),
                $tab = null,
                to = 0;
            var _do = function(_prop) {
                    $tab = _el.eq(i);
                    $tab.css(_prop, to).addClass('nth-child-' + (i + 1));
                    if (_prop == 'margin-top') {
                        to += $tab.outerHeight();
                    } else {
                        to += $tab.outerWidth();
                    }
                },
                toLeft = function() {
                    for (i = 0, len = _el.length; i < len; i++) {
                        _do('left');
                    }
                    _el.css('margin-top', marginTop);
                },
                toRight = function() {
                    for (i = _el.length; i >= 0; i--) {
                        _do('right');
                    }
                    _el.css('margin-top', marginTop);
                },
                toVertical = function() {
                    for (i = 0, len = _el.length; i < len; i++) {
                        _do('margin-top');
                    }
                };
            var _core = toRight;
            _core = (_pos == 'left') ? toLeft : _core;
            _core = (_pos == 'vertical') ? toVertical : _core;
            _core();
            _el.css('top', 0);
        };

        // set base element class
        var baseClass = function(_remove, _add) {
            var el = base.$el;
            if (_remove !== null) el.removeClass(_remove);
            if (_add !== undefined) el.addClass(_add);
        };

        // set item class
        // setClassSlide($inner, _visibleLength, first, last);
        var setClassSlide = function(_parent, _visibleLength, _first, _last) {
            var items = null;
            if (_first === undefined && _parent.find('active').length) {
                _first = _parent.find('active').index();
            } else {
                _first = _first || 0;
            }
            _last = _last || _visibleLength - 1;
            items = _parent.children();
            items.removeClass('first last active visible');
            items.eq(_first).addClass('first active').eq(_last).addClass('last');
            items.slice(_first, _last + 1).addClass('visible');
        };

        // move children items 
        // moveChildItems('prepend', $inner, $items, _slideLength, _itemsLength);
        var moveChildItems = function(_dir, _el, _items, _slideLength, _itemsLength) {
            var i,
                len;
            var prepend = function() { // core
                    _items.eq((_itemsLength - i) - 1).clone().remove().end().prependTo(_el);
                },
                append = function() {
                    _items.eq(i).clone().remove().end().appendTo(_el);
                },
                _core = (_dir == 'prepend') ? prepend : append;
            for (i = 0, len = _slideLength; i < len; i++) {
                _core();
            }
        };

        // return loop paging
        // page.current = returnLoopPaging(page.current - 1, page.total);
        var returnLoopPaging = function(_num, _max) {
            if (_num <= 0) {
                _num = _max + _num;
            } else if (_num > _max) {
                _num = Math.abs(_max - _num, 10);
            }
            return _num;
        };

        // return random in scope
        var returnRandom = function(_scope) {
            var result = Math.floor(Math.random() * _scope);
            return result;
        };

        /** private method *******************************************************************************************/

        // do pager (slideActive, slideEnd)
        var _doPager = function(_el, _pageIdx) {
            if (_el === null || !_el.length) return false;
            var $active = _el.find('.active');
            if (_pageIdx === undefined) {
                _pageIdx = ($active.length) ? $active.index() + 1 : 1;
            }
            _el.find('.nth-child-' + _pageIdx).addClass('active').siblings().removeClass('active');
        };

        // do page counter
        var _doPageCounter = function(_el, _current, _total) {
            if (_el === null || !_el.length) return false;
            _el.empty().append('<strong>' + _current + '</strong>/<span>' + _total + '</span>');
        };

        var _toggleNav = function(_el, _idx, _max) { // do toggle navigation
            var classPrevOut = 'prevOut',
                classNextOut = 'nextOut';
            if (_isInfinite === true) return false;
            _el.removeClass(classPrevOut + ' ' + classNextOut);
            if (_idx <= 1) _el.addClass(classPrevOut);
            if (_idx >= _max) _el.addClass(classNextOut);
        };

        // do navigation
        var doNavigation = function(_current, _total) {
            _doPager(base.$pager, _current); // do pager
            _doPageCounter(base.$counter, _current, _total);
            _toggleNav(base.$el, _current, _total);
        };

        // set current page index
        var setCurrentPageIdx = function() {
            var _activeIdx = (slide.$active !== null && slide.$active.length) ? slide.$active.index() : 0,
                _current = Math.ceil((_activeIdx + _visibleLength) / _visibleLength);
            if (_isInfinite === true) {
                _current = page.current;
                slide.$el.data('page', page.current);
            } else {
                page.current = _current;
            }
            page.activeIdx = _activeIdx;
        };

        /** reset slide *******************************************************************************************/

        // fit slides width when slide on active, on resize
        var fitSlidesWidth = function() {
            var $el = base.$el;
            var itemWidth = Math.floor($el.width() / _visibleLength),
                innerWidth = (itemWidth * slide.itemsLength);

            if (_isVertical === true) { // for vertical mode
                innerWidth = (itemWidth * _visibleLength);
                if (_itemHeight !== 'auto') slide.$items.css('height', _itemHeight);
            }

            slide.$inner.width(innerWidth);
            slide.$items.width(itemWidth);
            slide.itemWidth = itemWidth;
        };

        // resize slide
        var resetSlide = function(_el, _when) {

            slide.$el = _el;
            slide.$tab = slide.$el.prev();
            slide.$inner = _el.children();
            slide.$items = slide.$inner.children();
            slide.$active = slide.$inner.find('.active') || slide.$inner.eq(0);

            slide.itemsLength = slide.$items.length; // get items length

            page.current = slide.$el.data('page') || 1; // load page current

            var addeGhostItems = (function() {
                if (slide.itemsLength - _visibleLength < _slideLength && _isInfinite === true) {
                    slide.$items.clone().appendTo(slide.$inner);
                    slide.$items = slide.$inner.children();
                    slide.itemsLength = slide.$items.length; // fix items length
                }
            })();

            page.total = Math.ceil(slide.itemsLength / _visibleLength);

            slide.$el.css('overflow', 'hidden').fadeIn(opts.tabDelay);
            slide.$tab.addClass('active').siblings('.subject').removeClass('active');

            fitSlidesWidth();

            // set pager (slideActive, slideEnd)
            var _setPager = (function(_el, _itemsLength) {
                if (_el === null || !_el.length) return false;
                _el.empty();
                var pageNum, pageItemIdx, className;
                var page = Math.ceil(_itemsLength / _visibleLength);
                for (var i = 0; i < page; i++) {
                    pageNum = (i + 1);
                    pageItemIdx = (pageNum * _visibleLength) - _visibleLength + 1;
                    className = 'nth-child-' + pageNum;
                    $('<a href="#' + pageItemIdx + '" data-page="' + pageNum + '" data-idx="' + pageItemIdx + '" class="ir ' + className + '"><em>' + pageNum + '</em></a>').appendTo(_el);
                }
            })(base.$pager, slide.itemsLength);

            doNavigation(page.current, page.total); // do navigation

        };

        // core do slide
        var _coreDoSlide = function(_todo, _idx, _page, $inner, _itemWidth, _itemsLength) {

            if (_isAnimating === true || $inner === null) return false;

            _isAnimating = true;

            var first = 0,
                last = 0;

            // on slide end
            var _onSlideEnd = function(_el) {
                _isAnimating = false;
                slide.$active = slide.$inner.find('.active') || slide.$inner.eq(0);
                setCurrentPageIdx(); // set current page index
                callFunc(opts.onSlideEnd);
            };

            if (_isInfinite === true) { // infinite mode

                var $items = $inner.children(),
                    pageMargin = 0,
                    posInfinite = 0,
                    _prev,
                    _next;

                if (_isVertical === true) {

                    posInfinite = -_itemHeight;

                    // prev
                    _prev = function(_count) {
                        moveChildItems('prepend', $inner, $items, (_slideLength * _count), _itemsLength);
                        $inner.stop(true, true).css('margin-top', -(_itemHeight * _count)).animate({
                            'margin-top': 0
                        }, opts.duration, function() {
                            setClassSlide($inner, _visibleLength);
                            _onSlideEnd($inner);
                        });
                    };

                    // next
                    _next = function(_count) {
                        $inner.stop(true, true).animate({
                            'margin-top': -(_itemHeight * _count)
                        }, opts.duration, function() {
                            moveChildItems('append', $inner, $items, (_slideLength * _count), _itemsLength);
                            $inner.stop().css('margin-top', 0);
                            setClassSlide($inner, _visibleLength);
                            _onSlideEnd($inner);
                        });
                    };
                } else {

                    posInfinite = -(_slideLength * slide.itemWidth);

                    // prev
                    _prev = function(_count) {
                        moveChildItems('prepend', $inner, $items, (_slideLength * _count), _itemsLength);
                        $inner.stop(true, true).css('margin-left', posInfinite * _count).animate({
                            'margin-left': 0
                        }, opts.duration, function() {
                            setClassSlide($inner, _visibleLength);
                            _onSlideEnd($inner);
                        });
                    };

                    // next
                    _next = function(_count) {
                        $inner.stop(true, true).animate({
                            'margin-left': posInfinite * _count
                        }, opts.duration, function() {
                            moveChildItems('append', $inner, $items, (_slideLength * _count), _itemsLength);
                            $inner.stop().css('margin-left', 0);
                            setClassSlide($inner, _visibleLength);
                            _onSlideEnd($inner);
                        });
                    };

                }

                // set page margin
                if (_todo == 'prev') {
                    pageMargin = -1;
                } else if (_todo == 'next') {
                    pageMargin = 1;
                } else {
                    pageMargin = _page - page.current;
                }

                // do
                if (pageMargin < 0) {
                    pageMargin = Math.abs(pageMargin);
                    page.current = returnLoopPaging(page.current - pageMargin, page.total);
                    _prev(pageMargin); // 1...
                } else if (pageMargin > 0) {
                    page.current = returnLoopPaging(page.current + pageMargin, page.total);
                    _next(pageMargin); // 1...
                }

                doNavigation(page.current, page.total);

            } else { // no infinite mode

                var posNoInfinte = 0,
                    current = 0,
                    beforeIdx = $inner.find('.active').index(); // get before item index

                var _doGo = function(_idx) {

                    first = (_idx < 0 || _idx === undefined) ? 0 : parseInt(_idx, 10);
                    first = (first >= _itemsLength) ? (_itemsLength) : first;
                    first = (first >= _itemsLength - _visibleLength) ? _itemsLength - _visibleLength : first;
                    last = first + (_visibleLength - 1);

                    posNoInfinte = -(first * _itemWidth);

                    current = (Math.ceil((first + _visibleLength) / _visibleLength));

                    if (_isVertical === true) {

                        posNoInfinte = -((current - 1) * _itemHeight);

                        // do animate
                        $inner.stop(true, false).animate({
                            'margin-top': posNoInfinte
                        }, opts.duration, function() {
                            setClassSlide($inner, _visibleLength, first, last);
                            _onSlideEnd($inner);
                        });

                    } else { // vertical

                        // do animate
                        $inner.stop(true, false).animate({
                            'margin-left': posNoInfinte
                        }, opts.duration, function() {
                            setClassSlide($inner, _visibleLength, first, last);
                            _onSlideEnd($inner);
                        });

                    }

                    doNavigation(current, page.total);

                };

                if (_todo == 'prev') {
                    _doGo(beforeIdx - _slideLength);
                } else if (_todo == 'next') {
                    _doGo(beforeIdx + _slideLength);
                } else {
                    _doGo(parseInt(_idx, 10) - 1);
                }

            }
        };

        /** public method *******************************************************************************************/

        // do slide
        var doSlide = function(_todo, _idx, _page) {

            // on slide start
            var onSlideStart = (function() {
                callFunc(opts.onSlideStart);
            })();

            _coreDoSlide(
                _todo,
                _idx,
                _page,
                slide.$inner,
                slide.itemWidth,
                slide.itemsLength);

        };

        // on images loaded
        var onImgsLoaded = function(_el, _callback) {

            var _doCallback = function(_param, _delay) {
                _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
                if ($.isFunction(_callback)) {
                    _param = (_param === null) ? '' : _param;
                    _delay = _delay || 300;
                    setTimeout(function() {
                        _callback.call(null, null, _param);
                    }, _delay);
                }
            };

            // do preloader
            var _preloader = function(_count, _total) {
                var percent;
                if (_count === undefined || _count <= 0) {
                    base.$preloader.hide().prependTo(_el).fadeIn();
                } else {
                    percent = (_count * 100 / _total);
                    base.$preloader.find('p').text(parseInt(percent, 10) + '%');
                }
            };

            // imagesLoaded Core
            var _core = function(imgLoad) {
                var loaded = 0;
                _preloader();
                imgLoad.on('progress', function(instance, image) {
                    loaded++;
                    _preloader(loaded, imgLoad.images.length);
                }).on('done', function() {
                    baseClass(null, 'loaded items-' + loaded);
                    _doCallback();
                }).on('fail', function() {
                    baseClass(null, 'loaded items-' + loaded);
                    _doCallback();
                });
            };

            if (opts.imagesLoaded && typeof require === "function" && require.defined('imagesLoaded')) { // requireJs
                requirejs([
                    'imagesLoaded'
                ], function(imagesLoaded) {
                    _core(imagesLoaded(_el));
                });
            } else if (opts.imagesLoaded && typeof imagesLoaded == 'function') { // imagesLoaded
                _core(imagesLoaded(_el));
            } else {
                _doCallback();
            }

        };

        // activate slide
        var activateSlide = function(_idx) {

            var $before = slide.$el,
                $after = null,
                start = 0;

            // active slide
            var activeSlide = function(_el) {
                resetSlide(_el);
                setClassSlide(_el.children(), opts.visibleLength);
                if (!_el.hasClass('loaded')) {
                    _el.addClass('loaded');
                    var _doEach = function(i) {
                        $(this).addClass('nth-child-' + (i + 1));
                    };
                    slide.$inner.children().each(_doEach);
                }
            };

            // deactive slide
            var deactiveSlide = function() {
                if ($before === null) $before = $('<div />');
                $before.find('.active').removeClass('active'); // reset
                $before.hide().children().css('margin', 0); // reset
            };

            start = (opts.randomActive === true) ? returnRandom(base.$wraps.length) : start; // for random actve
            _idx = (_idx === undefined || _idx === null) ? start : _idx;

            $after = base.$wraps.eq(_idx);

            if (base.$pager !== null && base.$pager.length) {
                base.$pager.fadeOut(base.tabDelay, function() {
                    base.$pager.fadeIn(base.tabDelay);
                    deactiveSlide();
                    activeSlide($after);
                    base.tabDelay = opts.tabDelay;
                });
            } else {
                deactiveSlide();
                activeSlide($after);
            }

            autoPlay();

        };

        // auto play
        var autoPlay = function() {
            clearTimeout(_timerAutoPlay);
            _delayAutoPlay = (_delayAutoPlay < opts.delay) ? opts.delay + returnRandom(1500) : _delayAutoPlay; // at first
            if (_isAuto === true) {
                _timerAutoPlay = setTimeout(function() {
                    console.log(_isHover);
                    if (_isHover !== true && base.isPause !== true) {
                        if (_isInfinite !== true && page.current >= page.total) {
                            doSlide('goto', 1); // or activateSlide(); // random 
                        } else {
                            doSlide('next');
                        }
                    }
                    _delayAutoPlay = opts.delay;
                    autoPlay();
                }, _delayAutoPlay);
            }
        };

        // destroy
        var destroy = function() {
            $([
                base.$el,
                base.$tabs,
                base.$wraps,
                base.$navPrev,
                base.$navNext,
                base.$pager,
                base.$counter
            ]).each(function() {
                $(this).off('.slide');
            });
            base = null;
            console.log('destroyed');
        };

        // set display
        var setDisplay = function() {

            var setElDisplay = (function(_el) {
                if (opts.sliderHeight !== 'auto') _el.css('height', _itemHeight);
            })(base.$el);

            var setWrapDisplay = (function(_el) {
                if (opts.sliderHeight !== 'auto') _el.css('height', _itemHeight);
                _el.hide();
            })(base.$wraps);

            var setNavigation = (function(_el) {
                doPullUp(_el);
            })([
                base.$navPrev,
                base.$navNext
            ]);

            var setTapPosition = (function(_el) {
                setAbsoluteTapPosition(_el, opts.tabsPosition); // set tabs position
            })(base.$tabs);

        };

        // set handler
        var setHandler = function() {
            base.$tabs.on('focusin.slide', function(e) {
                if ($(e.target).get(0).tagName.toLowerCase() == 'a') {
                    activateSlide(indexType($(this), '.subject'));
                }
            });
            if (base.$pager !== null && base.$pager.length) {
                base.$pager.on('click.slide', function(e) {
                    e.preventDefault();
                    var $this = $(e.target);
                    if ($this.get(0).tagName.toLowerCase() == 'a') {
                        doSlide('goto', $this.data('idx'), $this.data('page'));
                    }
                });
            }
            if (base.$navNext !== null && base.$navNext.length) {
                base.$navPrev.off('.slide').on('click.slide', function(e) {
                    e.preventDefault();
                    doSlide('prev');
                });
                base.$navNext.off('.slide').on('click.slide', function(e) {
                    e.preventDefault();
                    doSlide('next');
                });
            }
            base.$el.hover(function() {
                _isHover = (opts.pauseOnHover === true) ? true : false;
            }, function() {
                _isHover = false;
                autoPlay();
            });
        };

        // core
        var _core = function() {

            callFunc(opts.onInit);
            setDisplay(); // set display
            setHandler(); // set base handler
            onImgsLoaded(base.$el, function() {
                activateSlide();
                callFunc(opts.onComplete);
            });

            // on resized
            if (opts.resizable === true && slide.$inner !== null) {
                $(window).on('resize.slide orientationchange', function() {
                    throttle(function() {
                        fitSlidesWidth();
                        if (_isInfinite !== true) {
                            _isAnimating = false; // force slide
                            doSlide('goto', page.activeIdx + 1);
                        }
                    }, 0);
                });
            }

        };

        // add json items
        var addJsonItems = function(_el, _data, _options) {
            var _html = '',
                $group,
                $item;
            if (!_el.length) return false;
            for (var i = 0, len = _data.group.length; i < len; i++) {
                $group = _data.group[i];
                if (len > 1) { // 탭이 한개 이상일때만 추가
                    _html += '<h4 class="subject"><a href="#!"><span>' + $group.subject + '</span></a></h4>';
                }
                _html += '<div class="itemwrap"><ul class="tab"><!-- itemwrap -->';
                for (var j = 0, lenj = $group.items.length; j < lenj; j++) {
                    $item = $group.items[j];
                    _html += opts.formatter.call(null, $group.items[j], j);
                }
                _html += '</ul></div><!-- // itemswrap -->';
            }
            _el.append($(_html));

        };

        // init
        var init = function() {

            if (opts.json !== null) {
                addJsonItems(base.$el, opts.json);
            }

            base.$tabs = $(opts.tabEl, base.$el);
            base.$wraps = $(opts.wrapEl, base.$el);

            base.isMulti = true;
            base.tabDelay = 0;

            slide.$tab = null;
            slide.$el = null;
            slide.$inner = null;
            slide.$items = null;
            slide.$active = null;
            slide.itemsLength = 0;
            slide.itemWidth = 0;
            slide.itemHeight = 0;

            baseClass(null, 'multi no-infinite nav pager counter horizon');

            if (!base.$navNext.length) baseClass('pager', 'no-nav');
            if (!base.$pager.length) baseClass('pager', 'no-pager');
            if (!base.$counter.length) baseClass('pager', 'no-counter');
            if (_isVertical === true) baseClass('horizon', 'vertical');
            if (_isInfinite === true) baseClass('no-infinite', 'infinite');

            if (base.$tabs.length < 2) {
                baseClass('multi', 'single');
                base.isMulti = false;
            }

            for (var key in base) {
                if (typeof base[key] !== 'object') {
                    base.$el.addClass(key + '-' + base[key]);
                }
            }

            _core();

        };

        return this.each(function() {
            init();
        });

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
$(function() {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $container = $('#container');

    $('#header a').on('click', function(e) {

        $.showOverlay(true);
        $.perspectiveZoom('on');

        $html.addClass('smartsearch-open');

        $('#smartsearch').attr('tabindex', '0').focus();

    });

    $('#smartsearch a').on('click', function(e) {

        $html.removeClass('smartsearch-open');
        $.perspectiveZoom('off');

        $('#smartsearch').onTransitionEnd(function() {

            $.perspectiveZoom('clear');
            $.showOverlay(false, {
                onAfter: function() {
                    $('#header a').eq(0).focus();
                }
            });

        });
    });

});
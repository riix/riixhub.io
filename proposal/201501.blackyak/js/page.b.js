$(function() {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    $('.js-hover').hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

    $('#expertSlide').simpleSlider({
        pager: '#expertPager',
        css3animation: true,
        duration: 10,
        delay: 3500
    });

});
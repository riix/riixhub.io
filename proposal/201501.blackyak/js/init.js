// config
require.config({
    baseUrl: './js',
    paths: {
        'jquery': 'vendor/jquery-1.11.2.min',
        'modernizr': 'vendor/modernizr-2.6.2-respond-1.1.0.min',
        'imagesloaded': 'plugins/imagesloaded.3.1.8.pkgd.min',
        'stellar': 'plugins/jquery.stellar.0.6.2.min',
        'finger': 'plugins/jquery.finger.0.1.2.min',
        'mousetrap': 'plugins/moustrap.1.4.6.min',
        'riixBase': 'min/riix.base-0.1.min',
        // 'riixJsonModal': 'min/riix.jsonModal-0.1.min',
        'riixJsonModal': 'riix.jsonModal-0.1',
        'riixSimpleSlider': 'riix.simpleSlider-0.15'
    },
    shim: {
        'modernizr': {
            exports: 'Modernizr' // modernizr global var.
        }
    },
    urlArgs: 'timestamp=' + (new Date()).getTime() // cache buster
});

// init
require([

    'jquery',
    'modernizr'

], function($, modernizr) {

    'use strict';

    var _bodyClass = $('body').attr('class');

    require([

        'imagesloaded',
        'finger',
        'mousetrap',
        'riixBase'

    ], function() {

        $.appendPreloadImg();

        $('html').imagesLoaded(function() {
            $('img.preload').remove(); // preload 이미지 제거
        });

        if (_bodyClass.match('type-a')) {

            require([ // view

                'riixJsonModal',
                'riixSimpleSlider'

            ], function() {

                VIEW.init();
                ViewPortfolioControl.init();

                require([ // a

                    'page.a'

                ], function() {

                    // $.showOverlay(true, {
                    //     isIndicator: true
                    // });

                });

            });

        } else if (_bodyClass.match('type-b')) {

            require([

                'stellar',
                'riixSimpleSlider'

            ], function() {

                require([

                    'min/page.b.min'

                ], function() {

                    if (EPASS.support.transforms3d) {
                        $.stellar({
                            horizontalScrolling: false,
                            verticalOffset: 40
                        });
                    }

                });

            });

        } else if (_bodyClass.match('type-c')) {

            require([

                'riixSimpleSlider'

            ], function() {

                require([

                    'min/page.c.min'

                ], function() {

                    //

                });

            });

        }

    });

});
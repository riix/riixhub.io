// set control
var ProposalControl = null;

// set constructor PROPOSAL
var PROPOSAL = (function($, window, document, ua) {

    'use strict';

    var $document = $document || $(document), // DOM cache
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $$ = function(_selector, _parent) { // faster id selector
        return (_selector.match('#')) ? $(document.getElementById(_selector.replace('#', '')), _parent) : $($(_selector), _parent);
    };

    var Proposal = function(el, options) {

        // set base
        var base = [];

        // set settting
        var setting = {
            jsonUrl: './json/test.json',
            onAfter: null
        };

        // extend option
        base.opts = $.extend(setting, options);

        // global Var
        base.$el = $(el);

        // set custom data
        base.$el.data('list', base);

        // vars.
        base.jsonData = [],
        base.idx = 0,
        base.$item = null,
        base.$filter = null,
        base.BLANKIMGSRC = EPASS.INDICATORIMGSRC;


        /**
         * 핵심 실행 영역
         */
        var _core = function() {

            base.$el.css('opacity', '0');

            doCallback('onAfter'); // 생성 후 실행 함수 반환

            base.$el.on('destroy', function(e) { // destory 트리거 생성
                onDestroy();
            });

        };

        /**
         * 생성 후 실행 함수 반환
         */
        var doCallback = function(_when) {

            var _timerCallback = null;

            if (_when == 'onBefore' && typeof(base.opts.onBefore) == "function") {
                _timerCallback = setTimeout(function() {
                    clearTimeout(_timerCallback);
                    $.callFunc(base.opts.onBefore); // base.opts.onAfter(base);
                }, 50);
            } else if (_when == 'onAfter' && typeof(base.opts.onAfter) == "function") {
                _timerCallback = setTimeout(function() {
                    clearTimeout(_timerCallback);
                    $.callFunc(base.opts.onAfter); // base.opts.onAfter(base);
                }, 50);
            } else {
                console.log('needs when.');
            }

        };

        /**
         * Destory 트리거 생성
         */
        var onDestroy = function() {
            $.removeData(base.$el);
            $('a', base.$el).off();
            console.log('destroyed.');
        };

        return {

            init: function() {
                _core();
            },

            destroy: function() {
                _destroy();
            }

        };
    };

    // init
    return {

        init: function() {

            // set control
            ProposalControl = (new Proposal($('#stage'), {
                onAfter: function() {
                    console.log('done!');
                }
            }));

        }

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
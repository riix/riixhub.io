$(function() {

    var __animation = true;

    $('#jumbotronSlide').simpleSlider({
        pager: '#jumbotronPager',
        css3animation: __animation,
        duration: 300,
        delay: 3500
    });

    $('#jumbotronSlideB').simpleSlider({
        fx: 'fxLetMeIn',
        mode: 'inOut',
        pager: '#jumbotronPagerB',
        css3animation: __animation,
        duration: 500,
        delay: 3500,
        reverse: true
    });

    $('#issueSlide').simpleSlider({
        pager: '#issuePager',
        css3animation: __animation,
        duration: 300,
        delay: 5000,
        autoPlay: false
    });

    $('#newArrival .js-color').on('click mouseover', function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.currentActive().parents('li').find('.thumb').eq($this.index()).show().siblings('.thumb').hide();
    });


    $('#newArrivalSlide').simpleSlider({
        nav: '#newArrivalNav',
        css3animation: __animation,
        duration: 500,
        delay: 5200,
        autoPlay: false
    });

    $('#catalog').simpleSlider({
        nav: '#catalogNav',
        css3animation: __animation,
        duration: 250,
        delay: 3000,
        autoPlay: false
    });


    /*
    $('#newArrivalSlide .item').eq(0).addClass('active fx').show().css({});
    $('#catalog .item').eq(0).show();
    */

});
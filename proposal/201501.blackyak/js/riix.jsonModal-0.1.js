(function($, window, document, ua) {

    /**
     * 자식 객체의 크기를 부모 객체 크기에 맞게 리사이즈 함, 마진 옵션을을 이용해 세로 가운데 정렬
     */
    $.fn.fitResizeCenter = function(options) {

        var setting = {
            margin: 0,
            target: 'img',
            defaultX: 1004,
            defaultY: 711,
            verticalCenter: true
        };

        var opts = $.extend(setting, options);

        var $parentEl = $(this),
            $childrenEl = $(opts.target, $parentEl).eq(0);

        $childrenEl.removeAttr('style').css({
            'display': 'inline-block',
            'opacity': '0'
        });

        var _parentWidth = $parentEl.width(),
            _parentHeight = $parentEl.height(),
            _childrenWidth = ($childrenEl.width() > 100) ? $childrenEl.width() : opts.defaultX,
            _childrenHeight = ($childrenEl.height() > 100) ? $childrenEl.height() : opts.defaultY;

        var _ratioX = _childrenHeight / _childrenWidth,
            _ratioY = _childrenWidth / _childrenHeight;

        var _toWidth = Math.min(_parentWidth - opts.margin, _childrenWidth),
            _toHeight = _toWidth * _ratioX;

        if (_toHeight > _parentHeight - opts.margin) { // div 보다 넘칠 경우
            _toHeight = _parentHeight - opts.margin;
            _toWidth = _toHeight * _ratioY;
        }

        var _marginTop = (opts.verticalCenter) ? (_parentHeight - _toHeight) / 2 : 0;

        $childrenEl.css({
            'opacity': 1,
            'margin-top': _marginTop,
            'width': _toWidth,
            'height': _toHeight
        });

        return this;

    };
})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);



// set control
var ViewPortfolioControl = null;

// set constructor VIEW
var VIEW = (function($, window, document, ua) {

    'use strict';

    var $document = $document || $(document), // DOM cache
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $$ = function(_selector, _parent) { // faster id selector
        return (_selector.match('#')) ? $(document.getElementById(_selector.replace('#', '')), _parent) : $($(_selector), _parent);
    };

    var ViewPortfolio = function(el, options) { // view portfolio

        // set base
        var base = [];

        // set settting
        var setting = {

            jsonUrl: null,
            fx: 'fxFortuneWheel',

            animDuration: '0.5s',
            maxWidth: 1920,
            margin: 25,

            dragable: false,
            reverse: true,
            mouse: true,
            keyboard: true,
            onBefore: null,
            onAfter: null,

            SELECTOR_POPUP: '#popup',
            CLASS_CURSORGRAB: 'cursor-grab'

        };

        // extend option
        base.opts = $.extend(setting, options);

        // global Var
        base.$el = $(el).show();

        // set custom data
        base.$el.data('view', base);

        // vars.
        base.jsonData = [],
        base.supportTransforms3d = EPASS.support.transforms3d || true;
        base.isMobile = (EPASS.hasTouch === true) ? true : false,
        base.animEndEventName = EPASS.animEndEventName || 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd',
        base.countAnims = 0,
        base.isAnimating = false,
        base.$currentEl = null,
        base.$nextEl = null;
        base.$currentOpener = null;

        base.opts.mouse = (base.isMobile === true) ? false : base.opts.mouse;

        // mousetrap key map
        base.mouseTrap = (typeof window.Mousetrap == 'object' && base.isMobile !== true && base.opts.keyboard === true) ? true : false;
        base.KEYMAP = ['esc', 'left', 'right', 'up', 'down'];

        /**
         * 팝업 내 아이템 생성
         * @param  {[type]} _item      객체
         * @param  {[type]} _classname 덧붙일 클래스명
         */
        var _appendItem = function(_item, _classname) {

            _classname = (_classname !== null) ? _classname : '';

            var _html = '<li class="item ' + _item.classname + ' ' + _classname + '">';
            _html += '<div class="holder">';
            _html += '<div class="thumb"><img src="' + _item.src + '"/></div>';
            _html += '</div>';
            _html += '</li>';

            var $el = $(_html);
            return $el;
        };

        /**
         * 이벤트 바인딩
         */
        var _setEvent = function() {

            base.$popup.off('.popup'); // reset

            base.$popup.on('click.popup', function(e) {

                e.preventDefault();

                var $link = $(e.target).closest('a');

                if ($link[0] !== undefined && $link[0].tagName.toLowerCase() == 'a') {

                    var _class = $link.attr('class');

                    if (_class == 'close') {
                        _deactivePopup(e);
                    } else if (_class.match('nav')) {
                        _navigate(_class);
                    }

                }

            });

            // for mouse
            if (base.opts.mouse === true) {

                base.$popup.addClass(base.opts.CLASS_CURSORGRAB);

                base.$popup.on('drag.popup', function(e) {

                    e.preventDefault();

                    e.stopPropagation();
                    if (e.direction == 1) {
                        _navigate('prev');
                    } else if (e.direction == -1) {
                        _navigate('next');
                    }
                }).on('dragstart', function(e) {
                    return false;
                });

                if (base.opts.dragable !== true) {
                    base.$popup.css({
                        'user-select': 'none',
                        'user-drag': 'none'
                    });
                }
            }

            // for keyboard
            if (base.mouseTrap === true) {

                Mousetrap.unbind(base.KEYMAP).bind(base.KEYMAP, function(e, key) {

                    $.preventDefault(e);

                    switch (key) {
                        case 'esc':
                            _deactivePopup();
                            break;
                        case 'left':
                            _navigate('prev');
                            break;
                        case 'right':
                            _navigate('next');
                            break;
                        default:
                            return false;
                    }

                });
            }

            // for mobile
            if (base.isMobile === true) {

                base.$popup.on('flick.popup', function(e) {

                    e.preventDefault();
                    e.stopPropagation();

                    if (e.orientation == 'horizontal') {
                        if (e.direction == 1) {
                            _navigate('prev');
                        } else if (e.direction == -1) {
                            _navigate('next');
                        }
                    }

                }).trigger('flickr.popup');

            }

        };

        /**
         * 팝업 슬라이드 구성
         * @param {int} _idx 현재 슬라이드 인덱스
         * @param {int]} _max 마지막 장 제한
         */
        var _setItem = function(_idx, _max) {

            doCallback('onBefore');

            // 인덱스값 범위 내 반환
            base.currentIdx = $.idxProof(_idx, _max);

            base.$popupItemWrapper.empty()
                .append(_appendItem(base.jsonData.items[$.idxProof(base.currentIdx - 1, _max)], 'prev'))
                .append(_appendItem(base.jsonData.items[base.currentIdx], 'current'))
                .append(_appendItem(base.jsonData.items[$.idxProof(base.currentIdx + 1, _max)], 'next'));

            _setResize();
            doCallback('onAfter');

        };

        /**
         * 윈도우 리사이즈 대응
         */
        var _setResize = function() {

            $window.off('.popup').on('resize.popup orientationchange.popup', function() {

                var _winHeight = $window.height(),
                    _winWidth = $window.width(),
                    _width = Math.min(_winWidth, base.opts.maxWidth) - (base.opts.margin * 2),
                    _marginX = (_winWidth - _width) / 2;

                $('li.item', base.$popupItemWrapper).css({
                    'top': base.opts.margin,
                    'left': _marginX,
                    'width': _width,
                    'height': (_winHeight - base.opts.margin * 2)
                });

                $('.close', base.$popup).css({
                    'top': base.opts.margin,
                    'right': _marginX
                });

                $('.nav', base.$popup).eachQ(function() {
                    var $parentEl = $(this);
                    $parentEl.css({
                        'margin-left': _marginX,
                        'margin-right': _marginX,
                        'top': (_winHeight / 2 - $parentEl.height() / 2)
                    });
                });

                $('li.current', base.$popupItemWrapper).fitResizeCenter({
                    margin: 40,
                    target: '.thumb'
                });

            }).trigger('resize.popup');
        };

        /**
         * 현재 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimCurrentItem = function() {
            base.countAnims++;
            base.$currentEl.removeClass();
            base.isAnimating = (base.countAnims == 2) ? false : true;
        };

        /**
         * 다음 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimNextItem = function() {
            base.countAnims++;
            base.$nextEl.removeClass().addClass('current');
            base.isAnimating = (base.countAnims == 2) ? false : true;
            _setItem(base.currentIdx, base.jsonData.items.length);
        };

        /**
         * 슬라이드 이동
         * @param  {string} dir 방향
         */
        var _navigate = function(dir) {

            if (base.isAnimating) return false;

            $html.removeClass('indicator');

            base.countAnims = 0;
            base.isAnimating = true;

            base.$currentEl = $('li.current', base.$popupItemWrapper);

            var _classIn,
                _classOut;

            if (dir.match('prev')) {

                base.$nextEl = base.$currentEl.prev();
                base.currentIdx = base.currentIdx - 1;

                _classIn = (!base.opts.reverse) ? 'navInPrev' : 'navInNext';
                _classOut = (!base.opts.reverse) ? 'navOutPrev' : 'navOutNext';

            } else {

                base.$nextEl = base.$currentEl.next();
                base.currentIdx = base.currentIdx + 1;

                _classIn = (!base.opts.reverse) ? 'navInNext' : 'navInPrev';
                _classOut = (!base.opts.reverse) ? 'navOutNext' : 'navOutPrev';
            }

            if (base.supportTransforms3d === true) {

                base.$currentEl.addClass(_classOut)
                    .css('animation-duration', base.opts.animDuration)
                    .get(0).addEventListener(base.animEndEventName, _onEndAnimCurrentItem);

                base.$nextEl.addClass(_classIn)
                    .css('animation-duration', base.opts.animDuration)
                    .get(0).addEventListener(base.animEndEventName, _onEndAnimNextItem);

            } else {

                base.$currentEl.fadeOut();
                base.$nextEl.fadeIn();
                _onEndAnimCurrentItem();
                _onEndAnimNextItem();
                base.isAnimating = false;

            }

        };

        /**
         * 팝업 띄우기
         */
        var _activePopup = function(e) {

            base.currentIdx = 0;

            if (e !== undefined && $(e.target).length) {

                e.preventDefault();

                base.$currentOpener = $(e.target);

                if (base.$currentOpener.parents('.item').length) { // 리스트가 있을때

                    $('.item:visible', base.$el).eachQ(function(i) {
                        $(this).data('index', i);
                    });

                    base.currentIdx = base.$currentOpener.parents('.item').data('index');
                }

            }

            base.jsonData = {
                "items": []
            };

            var jqxhr = $.getJSON(base.opts.jsonUrl, function(data) {

                // filter
                if (EPASS.isotopeFilter !== undefined) {

                    $.each(data.items, function(i) {
                        var _item = data.items[i];
                        if (_item.classname == EPASS.isotopeFilter) {
                            base.jsonData.items.push(_item);
                        }
                    });

                } else {

                    base.jsonData = data;

                }

            }).done(function(data) {

                $html.addClass('popup-open');

                base.$popup.addClass(base.opts.fx).css({
                    'display': 'block',
                    'background': 'transparent'
                }).attr('tabIndex', '0').focus().promise().done(function() {
                    $html.removeClass('indicator');
                });

                _setItem(base.currentIdx, base.jsonData.items.length);
                _setEvent()

            }).fail(function() {

                base.jsonData = null;
                console.log("json load error");

            });

        };

        /**
         * 팝업 닫기
         */
        var _deactivePopup = function(e) {

            $.preventDefault(e);

            $.showOverlay(false);

            base.$popup.onTransitionEnd(function() {
                $html.removeClass('popup-close');
                base.$popup.css('display', 'none');
            });

            $html.removeClass('popup-open').addClass('popup-close');

            base.$popup.removeAttr('tabIndex').off('.popup').find('a').off('.popup');

            if (base.mouseTrap === true) {
                Mousetrap.unbind(base.KEYMAP);
            }

            if (base.$currentOpener !== null) {
                base.$currentOpener.focus();
            }

            console.log('popup deactived.');
        };

        /**
         * 핵심 영역
         */
        var _core = function() {

            // set element
            base.$popup = $$(base.opts.SELECTOR_POPUP);
            base.$popupItemWrapper = $('ul', base.$popup).eq(0);
            base.$opener = $('a.event', base.$el);

            // bind
            base.$opener.off('.popup').on('click.popup', _activePopup);

        };

        /**
         * 생성 후 실행 함수 반환
         */
        var doCallback = function(_when) {

            var _timerCallback = null;

            if (_when == 'onBefore' && typeof(base.opts.onBefore) == "function") {
                _timerCallback = setTimeout(function() {
                    clearTimeout(_timerCallback);
                    $.callFunc(base.opts.onBefore); // base.opts.onAfter(base);
                }, 50);
            } else if (_when == 'onAfter' && typeof(base.opts.onAfter) == "function") {
                _timerCallback = setTimeout(function() {
                    clearTimeout(_timerCallback);
                    $.callFunc(base.opts.onAfter); // base.opts.onAfter(base);
                }, 50);
            } else {
                console.log('needs when.');
            }

        };

        /**
         * Destory 트리거 생성
         */
        var onDestroy = function() {
            $.removeData(base.$el);
            $document.off('.popup');
            base.$popup.off('.popup');
            $('a', base.$popup).off('.popup');
        };

        // return public function
        return {

            init: function() {
                _core();
                console.log('popup initialized.');
            },

            destroy: function() {
                onDestroy();
            }

        };
    };

    // init
    return {

        init: function() {

            ViewPortfolioControl = (new ViewPortfolio($('#utility'), {
                jsonUrl: './json/test.json',
                duration: 300

                /*
                onBefore: function() {
                    InnerViewControl.deactive();
                },
                onAfter: function() {
                    InnerViewControl.active();
                }
                */

            }));

        }

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);

$(function() {

});
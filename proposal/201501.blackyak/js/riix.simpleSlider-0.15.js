(function($, window, document, ua) {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    /**
     * 심플 CSS3 슬라이더
     * @param  {[type]} options 옵션
     */
    $.fn.simpleSlider = function(options) {

        var base = [];

        var _animEndEventNames = EPASS.animEndEventName || 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd';

        var setting = {
            css3animation: true,
            mode: 'double', // 'fx', 'double'
            fx: 'fxPressAway',
            autoPlay: true,
            delay: 3000,
            baseZindex: 100,
            duration: 300,
            mouse: true,
            pager: null,
            nav: null,
            reverse: false,
            animEndEventNames: _animEndEventNames
        };

        // set opts
        base.opts = $.extend(setting, options);
        base.opts.css3animation = (EPASS.support.transforms3d !== true) ? false : base.opts.css3animation; // fix animation support

        // global Var
        base.$el = $(this);

        base.$beforeItem = null;
        base.$afterItem = null;

        var _timerAutoPlay = null,
            _timerDuration = null,
            _isAnimating = false,
            _isHover = false,
            _isPause = false,
            _beforeIdx = 0,
            _afterIdx = 0,
            _itemsLength = 0,
            _fxCountAnim = 0,
            _fxClassIn,
            _fxClassOut,
            _dir;

        /** 페이징 셋팅 */
        var _setPaginate = function() {
            if (base.opts.pager && _itemsLength !== $.getIndexedAnchor(base.$pager).length) {
                var $el = $.getIndexedAnchor(base.$pager, 0),
                    $pagerInner = $el.parent().empty();
                for (var i = 0; i < _itemsLength; i++) {
                    $el.clone().appendTo($pagerInner);
                }
            }
        };

        /** 페이징 */
        var _doPaginate = function() {
            if (base.opts.pager !== undefined) {
                $.getIndexedAnchor(base.$pager, _afterIdx).currentActive(); // 해당 페이지네이트  활성화
            }
        };

        /**
         * 현재 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimCurrentItem = function() {
            _fxCountAnim++;
            _isAnimating = (_fxCountAnim >= 2) ? false : true;
            base.$beforeItem.removeClass().addClass('item').off(base.opts.animEndEventNames);
        };

        /**
         * 다음 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimNextItem = function() {
            _fxCountAnim++;
            _isAnimating = (_fxCountAnim >= 2) ? false : true;
            base.$afterItem.removeClass().addClass('item active').off(base.opts.animEndEventNames);
        };

        /** 페이지 이동 */
        var _togglePage = function(_idx) {

            if (_isAnimating === true) return false;

            // set index
            _beforeIdx = (_beforeIdx !== _afterIdx) ? _afterIdx : _itemsLength - 1;
            _afterIdx = $.idxProof(_idx, _itemsLength);

            // set item and z-index, poiner events
            base.$beforeItem = base.$items.eq(_beforeIdx).setZindex(2, base.opts.baseZindex).css('pointer-events', 'none');
            base.$afterItem = base.$items.eq(_afterIdx).setZindex(3, base.opts.baseZindex).css('pointer-events', 'auto');

            // paginate
            _doPaginate();

            if (base.opts.css3animation === true) { // css3 지원시

                if (base.opts.mode == 'double') {

                    base.$beforeItem.removeClass('fx-second');
                    base.$afterItem.addClass('fx-first');

                    _timerDuration = setTimeout(function() {
                        clearTimeout(_timerDuration);
                        base.$beforeItem.removeClass('fx-first');
                        base.$afterItem.addClass('fx-second');
                        _isAnimating = false;
                    }, base.opts.duration);

                } else if (base.opts.mode == 'inOut') {

                    _fxCountAnim = 0;
                    _isAnimating = true;

                    if (_beforeIdx >= _afterIdx) {
                        _fxClassIn = (!base.opts.reverse) ? 'navInPrev' : 'navInNext';
                        _fxClassOut = (!base.opts.reverse) ? 'navOutPrev' : 'navOutNext';
                    } else {
                        _fxClassIn = (!base.opts.reverse) ? 'navInNext' : 'navInPrev';
                        _fxClassOut = (!base.opts.reverse) ? 'navOutNext' : 'navOutPrev';
                    }

                    base.$beforeItem.one(base.opts.animEndEventNames, _onEndAnimCurrentItem).addClass(_fxClassOut);
                    base.$afterItem.one(base.opts.animEndEventNames, _onEndAnimNextItem).addClass(_fxClassIn);

                }

            } else { // css3 미지원시

                base.$beforeItem.hide();
                base.$afterItem.fadeIn(200).promise().done(function() {
                    base.$afterItem.currentActive();
                    _isAnimating = false;
                });

            }

            // init autoplay
            _autoPlay();

        };

        /** 자동실행  */
        var _autoPlay = function() {
            clearTimeout(_timerAutoPlay);
            if (base.opts.autoPlay === true) {
                _timerAutoPlay = setTimeout(function() {
                    if (_isHover !== true) {
                        _navigate('next');
                    }
                }, base.opts.delay);
            }
        };

        /**
         * 이전, 다음 페이지 이동
         * @param  {[type]} _dir [description]
         */
        var _navigate = function(_dir) {
            if (_isAnimating !== true) {
                var _idx = _afterIdx;
                if (_dir.match('prev')) {
                    _idx--;
                } else {
                    _idx++;
                }
                _togglePage(_idx);
            }
        };

        /** 이벤트 핸들러  */
        var _setHandler = function() {

            base.$el.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
                _autoPlay();
            });

            $('a', base.$nav).on('click.slide', function(e) {
                e.preventDefault();
                _navigate($(this).attr('class'));
            });

            $('a', base.$pager).off().on('click.slide', function(e) {
                e.preventDefault();
                var _idx = $.getElemIdx($(this));
                if (_idx !== _afterIdx) {
                    _togglePage(_idx);
                } else {
                    return false;
                }
            });

            if (EPASS.hasTouch !== true && base.opts.mouse === true) { // no touchable device

                base.$items.on('drag.slide', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.direction == 1) {
                        _navigate('prev');
                    } else if (e.direction == -1) {
                        _navigate('next');
                    }
                }).on('dragstart', function(e) {
                    return false;
                });

                base.$items.css({
                    'user-select': 'none',
                    'user-drag': 'none'
                });

            } else if (EPASS.hasTouch === true) { // touchable device

                base.$items.on('flick.slide', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    if (e.orientation == 'horizontal') {
                        if (e.direction == 1) {
                            _navigate('prev');
                        } else if (e.direction == -1) {
                            _navigate('next');
                        }
                    }
                }).trigger('flickr.slide');

            }

        };

        /** 코어  */
        var _core = function() {

            // set animation class
            if (base.opts.css3animation === true) {

                base.$el.addClass(base.opts.mode);

                if (base.opts.mode == 'inOut') {
                    base.$el.addClass('component-fullwidth component itemwrap');
                    base.$el.parent().addClass(base.opts.fx);
                    base.$items.css('animation-duration', parseInt(base.opts.duration, 10) + 'ms');
                }

            } else {

                base.$el.addClass('no-animation');

            }

            _setHandler();
            _togglePage(0);
        };

        /** 초기화 */
        var _init = (function() {

            // sel elements
            base.$items = $('.item', base.$el),
            base.$pager = $(base.opts.pager),
            base.$nav = $(base.opts.nav);

            // set custom data
            base.$el.data('slider', base);

            // set z-index in order
            $.each([
                base.$el,
                base.$items,
                base.$items.eq(0),
                base.$nav,
                base.$pager
            ], function(i, _el) {
                _el.setZindex(i, base.opts.baseZindex);
            });

            // set vars.
            _itemsLength = base.$items.length;

            // set paginate
            _setPaginate();

            // do core
            _core();

        })();

        return this;

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
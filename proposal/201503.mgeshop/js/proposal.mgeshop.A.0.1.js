$(function() {

    var $html = $('html');

    // gnb
    $(document).on('click', '#gnb a.d1', function() {
        $(this).parents('li').toggleClass('active');
        $('#skyscraperB li').removeClass('active');
    });

    $(document).on('click', '#gnb .d2 a.close', function() {
        $(this).parents('li').toggleClass('active');
        $('#skyscraperB li').removeClass('active');
    });

    // skyscraperB
    $(document).on('click', '#skyscraperB a', function() {
        var $parent = $(this).parent();
        $parent.toggleClass('active');
        $parent.siblings().removeClass('active');
        $('#gnb li').removeClass('active');

    });

    $('#themeShopping a').hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

    var __animation = true;

    $('#jumbotronCore').fxSlider({
        // fx: 'random',
        // fx: 'fxCrossFade',
        // fx: 'fxCliffDiving',
        // fx: 'fxPushReveal'
        fx: 'fxCoverflow',
        autoPlay: true,
        pager: '#pager',
        css3animation: __animation,
        duration: 1000,
        delay: 3000,
        typho: {
            el: '.typho',
            duration: '.5s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        },
        reverse: true
    });

    // skyscraper
    var $skyscraper = $('#skyscraperB');
    $(window).on('resize scroll', function(e) {
        var _marginTop = $(window).scrollTop();
        var _start = 229;
        if (_marginTop >= _start) {
            $skyscraper.stop(true, false).animate({
                'margin-top': _marginTop - _start
            });
        } else {
            $skyscraper.stop(true, false).animate({
                'margin-top': 0
            });
        }
    }).trigger('resize');

});
$(function() {

    var $html = $('html');

    // gnb
    $(document).on('click', '#gnb a.d1', function() {
        var $this = $(this),
            _sub = $this.next();

        _sub.slideDown();
    });

    $(document).on('click', '#gnb .d2 a.close', function() {
        var $this = $(this),
            _sub = $this.parent();

        _sub.slideUp('300');
    });

    // skyscraperB
    $(document).on('click', '#skyscraperB a', function() {
        var $this = $(this),
            _sub = $this.next();

        if ($this.parent().hasClass('active')) {
            $this.parent().removeClass('active');
            _sub.hide();
        } else {

            $this.parent().addClass('active').siblings().removeClass('active');
            _sub.show().parent().siblings().children('.box').hide();

        }
    });

    $('#themeShopping a').hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

    var __animation = true;

    $('#jumbotronCore').fxSlider({
        // fx: 'random',
        // fx: 'fxCrossFade',
        // fx: 'fxCliffDiving',
        fx: 'fxPushReveal',
        // fx: 'fxCoverflow',
        autoPlay: true,
        pager: '#pager',
        css3animation: __animation,
        duration: 1000,
        delay: 3000,
        typho: {
            el: '.typho',
            duration: '.5s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        },
        reverse: true
    });

});
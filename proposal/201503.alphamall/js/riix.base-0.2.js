var EPASS = EPASS || [];
var noop = $.noop || function() {};

// console polyfill
window.console = window.console || {
    log: function() {}
};

(function($, window, document, ua) {

    'use strict';

    // DOM cache
    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    // set meta equiv for old ie to edge
    var setMetaIeEdge = (function() {
        var $meta = $('meta[http-equiv]');
        if ($meta.length) {
            $meta.attr('content', 'IE=edge');
        } else {
            $('head').prepend($('<meta http-equiv="X-UA-Compatible" content="IE=edge" />'));
        }
    }());

    // detect browser
    var detectBrowser = (function(ua) {
        var _vendor = {
            "android": "mobile android",
            "windows phone": "mobile windows",
            "iphone": "mobile iphone",
            "ipad": "mobile ipad",
            "ipod": "mobile ipod",
            "msie 6": "ie ie6 oldie lt-ie7 lt-ie8 lt-ie9",
            "msie 7": "ie ie7 oldie lt-ie8 lt-ie9",
            "msie 8": "ie ie8 oldie lt-ie9",
            "msie 9": "ie ie9 lt-ie10",
            "msie 10": "ie10",
            "chrome": "chrome",
            "webkit": "webkit",
            "safari": "safari",
            "firefox": "firefox"
        };
        $.each(_vendor, function(key, value) {
            if (ua.toLowerCase().match(key)) {
                EPASS.browser = _vendor[key];
                console.log('your browser is ' + EPASS.browser);
            }
        });
    })(ua);

    // detect device
    var isChrome = /chrome/i.exec(ua),
        isAndroid = /android/i.exec(ua);

    EPASS.hasTouch = 'ontouchstart' in window && !(isChrome && !isAndroid);
    EPASS.isMobile = ((/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(ua) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(ua.substr(0, 4)))) ? true : false; // EPASS.isMobile = (typeof window.orientation !== 'undefined') ? true : false;

    // modernizr option
    var support = {
        animations: Modernizr.cssanimations,
        transforms: Modernizr.csstransforms,
        transforms3d: Modernizr.csstransforms3d,
        transitions: Modernizr.csstransitions,
        rgba: Modernizr.rgba
    };

    var transitEndEventNames = {
        'WebkitTransition': 'webkitTransitionEnd',
        'OTransition': 'oTransitionEnd',
        'msTransition': 'MSTransitionEnd',
        'transition': 'transitionend'
    };

    var animEndEventNames = {
        'WebkitAnimation': 'webkitAnimationEnd',
        'OAnimation': 'oAnimationEnd',
        'msAnimation': 'MSAnimationEnd',
        'animation': 'animationend'
    };

    // get animation end event name
    EPASS.support = support;
    EPASS.transitEndEventName = transitEndEventNames[Modernizr.prefixed('transition')];
    EPASS.animEndEventName = animEndEventNames[Modernizr.prefixed('animation')];

    EPASS.easing = 'easeInOutQuad';
    EPASS.timeStamp = '?timeStamp=' + Math.floor(+new Date() / 100);

    // set html class in global array key
    for (var key in EPASS) {
        $html.addClass(EPASS[key]);
    }

    // set html class for device
    if (EPASS.isMobile) {
        $html.addClass('mobile');
    } else {
        $html.addClass('classic');
    }

    // set module
    EPASS.$overlay = $('#overlay').length ? $('#overlay') : $('<div id="overlay"></div>').appendTo($body);
    EPASS.$indicator = $('#indicator').length ? $('#indicator') : $('<div id="indicator"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>').appendTo($body);

    // image
    EPASS.BLANKIMGSRC = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';
    EPASS.INDICATORIMGSRC = './img/module/ajax-loader.gif';

    // easing
    var baseEasings = {};

    $.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(i, name) {
        baseEasings[name] = function(p) {
            return Math.pow(p, i + 2);
        };
    });

    $.each(baseEasings, function(name, easeIn) {
        $.easing["easeInOut" + name] = function(p) {
            return p < 0.5 ? easeIn(p * 2) / 2 : 1 - easeIn(p * -2 + 2) / 2;
        };
    });

    // set ajax
    $.ajaxSetup({
        cache: false
    });

    /**
     * faster Each for
     */
    $.fn.eachQ = (function() {
        var $this = $([1]);
        return function(_func) {
            var i = -1,
                len = this.length,
                $el;
            try {
                while (
                    ++i < len &&
                    ($el = $this[0] = this[i]) &&
                    _func.call($this, i, $el) !== false
                );
            } catch (e) {
                delete $this[0];
                throw e;
            }
            delete $this[0];
            return this;
        };
    }());

    /**
     * 콜백 함수 실행
     * @param  {obj} _el, 필요할 경우 객체
     * @param  {func} _func  실행할 함수
     * @param  {param} _param 파라미터(생략가능)
     * $.callFunc(foo, param);
     */
    $.callFunc = function(_el, _callback, _param, _delay) {
        var _timerCallback = null;
        _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
        if ($.isFunction(_callback)) {
            _param = (_param === null) ? '' : _param;
            _delay = _delay || 50;
            _timerCallback = setTimeout(function() {
                clearTimeout(_timerCallback);
                _callback.call(null, _param);
            }, 50);
        } else {
            return false;
        }
    };

    /**
     * css3 transition 시 실행
     * @param  {func} _callback 콜백
     * @param  {func} _pullback transition 적용이 안될 경우 우선 실행
     * $foo.onTransitionEnd(function(){
     *     todo();
     * });
     */
    $.fn.onTransitionEnd = function(_callback, _pullback) {

        var $this = $(this),
            _supportTransforms = EPASS.support.transforms || true,
            _eventName = EPASS.transitEndEventName || 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';

        var core = function() {
            $this.off(_eventName);
            try {
                _callback.call();
                $.callFunc(null, _callback);
            } catch (e) {
                _callback.call();
            }
        };

        if (_supportTransforms === true) { // 보강필요
            $this.off(_eventName).on(_eventName, core);
        } else if ($.isFunction(_pullback)) {
            $.callFunc(null, _pullback);
        } else {
            $.callFunc(null, _callback);
        }

    };

    /**
     * css3 animation end 시 실행
     * @param  {func} _callback 콜백
     * @param  {func} _pullback animation 적용이 안될 경우 우선 실행
     * $foo.onAnimationEnd(function(){
     *     todo();
     * });
     */
    $.fn.onAnimationEnd = function(_callback, _pullback) {

        var $this = $(this),
            _supportAnimations = EPASS.support.animations || true,
            _eventName = EPASS.animEndEventName || 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd';

        var core = function() {
            $this.off(_eventName);
            try {
                $.callFunc(null, _callback);
            } catch (e) {
                _callback.call();
            }
        };

        if (_supportAnimations === true) { // 보강필요
            $this.off(_eventName).on(_eventName, core);
        } else if ($.isFunction(_pullback)) {
            $.callFunc(null, _pullback);
        } else {
            $.callFunc(null, _callback);
        }
    };

    /**
     * 이미지 토글러
     * $('img').imgToggle('_on');
     * $('img').imgToggle('_off');
     * $('img').imgToggle('_off','_on');
     */
    $.fn.imgToggle = function(_first, _second) {

        var _asIs = '_on',
            _toBe = '_off';

        if (_second !== undefined) {
            _asIs = _first;
            _toBe = _second;
        } else {
            if (_first === undefined || _first == '_on') {
                _asIs = '_off';
                _toBe = '_on';
            }
        }

        this.each(function() {

            var $this = $(this);

            if ($this[0].tagName.toLowerCase() !== 'img') {
                $this = $this.find('img').eq(0);
            }

            $this.attr('src', $this.attr('src').replace(_asIs, _toBe));
        });

        return this;
    };

    /**
     * class, img 활성화
     * $foo.currentActive();
     */
    $.fn.currentActive = function(_className, _imgToggle) {

        _className = _className || 'active';

        var $this = $(this),
            $parent = $this.parent('li'),
            $el = ($parent.length) ? $parent : $this,
            $siblings = $el.siblings(),
            $img = $('img', $el).eq(0);

        $el.addClass('active').siblings().removeClass('active');

        if ($img.length && _imgToggle !== false) {
            $img.imgToggle('_on');
            $siblings.find('img').imgToggle('_off');
        }

        return this;

    };

    /**
     * 이미지 로드 후 콜백 실행
     * @param  {Function} callback 실행할 콜백
     * $('img', $('#foo')).imgsLoaded(function() {
     *     console.log('done');
     * });
     */
    $.fn.imgsLoaded = function(_callback, _delay) {
        var _items = this.filter('img'),
            _len = _items.length,
            _totalLen = 0,
            _timer = null,
            _blankImageSrc = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==';

        _delay = _delay || 0,

        _items.one('load.imgloaded', function() {
            _totalLen = --_len;
            if (_totalLen <= 0 && this.src !== _blankImageSrc) {
                _items.off('load.imgloaded');
                _timer = setTimeout(function() {
                    $.callFunc(_items, _callback); // _callback.call(_items, this);
                }, _delay);
            }
        });

        _items.each(function() {
            if (this.complete || this.complete === undefined) {
                var _src = this.src; // + '?time = ' + new Date().getTime();
                this.src = _blankImageSrc;
                this.src = _src;
            }
        });

        return this;
    };

    /**
     * Z-index 설정
     * @param {[type]} _num  반환할 z-index
     * @param {[type]} _base 더해질 기본값
     */
    $.fn.setZindex = function(_num, _base) {
        _num = (_base !== undefined) ? _num + _base : _num;
        $(this).css('z-index', _num);
        return this;
    };

    /**
     * 이벤트 타겟을 탐지하여 존재할 경우 preventDefault 적용
     * @param  {[type]} e 탐지할 대상
     * $.preventDefault(e);
     */
    $.preventDefault = function(e) {
        if (e !== null && e !== undefined && $(e.target).length) {
            e.preventDefault();
        }
        return this;
    };

    /**
     * lorempixel 랜덤 이미지 경로 구하기
     * @return {str} 이미지 경로 src
     */
    $.getRandomImgSrc = function() {
        var _size = Math.random() * 3 + 1,
            _width = Math.round((Math.random() * 110 + 100) * _size),
            _height = Math.round(140 * _size),
            _random = Math.ceil(Math.random() * 1000),
            _src = 'http://lorempixel.com/' + _width + '/' + _height + '/' + '?cacheBuster=' + _random;
        return _src;
    };

    /**
     * 스크롤 위치를 포함한 스크린 중앙 값을 구한다.
     * @param  {[type]} _height 계산 대상의 높이 크기
     * var _center = $.returnWinCenterPos($foo);
     */
    $.returnWinCenterPos = function(_el, _mode) {
        if (_el === undefined || !_el.length) return false;
        var _height = _el.height() || 0,
            _result = ($window.height() / 2) - (_height / 2);
        if (_el.css('position') !== 'absolute') {
            _result += $window.scrollTop();
        }
        return _result;
    };

    /**
     * 오버레이 + 인디케이터 출력
     * @param  {boolean} _overlay      오버레이 출력 여부
     * @param  {boolean} _indicator 인디케이터 출력 여부
     * @param  {func} _func      콜백함수
     * $.showOverlay(false);
     * $.showOverlay(true, {
     *     isIndicator: false,
     *     onAfter: function(){
     *         alert('done!');
     *     }
     * });
     *  $.showOverlay(false, {
     *     isIndicator: true
     *  });
     */
    $.showOverlay = function(_overlay, options) {

        var setting = {
            isIndicator: false,
            indicatorPositionMode: 'fixed',
            indicatorClassName: 'flip',
            onBefore: null,
            onAfter: null
        };

        var opts = $.extend(setting, options);

        var $overlay = EPASS.$overlay.attr('aria-hidden', true),
            $indicator = EPASS.$indicator.addClass(opts.indicatorClassName);

        var _timerOverlay = null,
            _delayHide = (opts.isIndicator === true) ? 1000 : 0;

        $.callFunc(null, opts.onBefore);

        if (_overlay === true) { // 보이기

            $overlay.css('display', 'block');

            _timerOverlay = setTimeout(function() {

                clearTimeout(_timerOverlay);

                $overlay.onTransitionEnd(function() {
                    $.callFunc(null, opts.onAfter); // opts.onAfter.call();
                });

                if (opts.isIndicator === true) {
                    if (opts.indicatorPositionMode == 'absolute') {
                        $indicator.removeAttr('style').css({
                            'position': 'absolute',
                            'top': $.returnWinCenterPos($indicator),
                            'margin-top': 0
                        });
                    } else {
                        $indicator.removeAttr('style').css({
                            'position': 'fixed',
                            'margin-top': -($indicator.height() / 2),
                            'top': '50%'
                        });
                    }
                    $html.addClass('indicator');
                }

                $html.addClass('overlay-open');

            }, 1);

        } else { // 감추기

            $overlay.onTransitionEnd(function() {
                $overlay.css('display', 'none');
                $html.removeClass('overlay-close indicator');
                $.callFunc(null, opts.onAfter);
            });

            _timerOverlay = setTimeout(function() {
                clearTimeout(_timerOverlay);
                $html.removeClass('overlay-open').addClass('overlay-close');
            }, _delayHide);

        }
    };

    /**
     * 인덱스값 범위 내 반환
     * @param  {[type]} _idx 받아올 인덱스
     * @param  {[type]} _max 제한범위
     */
    $.idxProof = function(_idx, _max) {
        _idx = (_idx < 0) ? _max - 1 : _idx;
        _idx = (_idx < _max) ? _idx : 0;
        return _idx;
    };


    /**
     * 해당 객체가 뷰포트 내에 있는지 불린값 반환
     * @param  {[type]} el 불러올 객체
     * @return {[type]}    불린 값
     * if ($.returnInView($el) === true) {
     *     todo()
     * };
     */
    $.returnInView = function(el) {
        var $this = $(el),
            _elHeight = parseInt($this.outerHeight() * 0.5, 10),
            _elTop = $this.offset().top,
            _scrollTop = document.documentElement.scrollTop || document.body.scrollTop,
            _winHeight = $window.height(),
            _winInnerHeight = (window.innerHeight && window.innerHeight < _winHeight) ? window.innerHeight : _winHeight,
            _result = (_elTop + _elHeight) > _scrollTop && (_elHeight + _elTop) < (_scrollTop + _winInnerHeight);
        return _result;
    };


    /**
     * 페이징 등 객체의 인덱스 순서 추출
     * @param  {[type]} _el 추출할 객체
     */
    $.getElemIdx = function(_el) {
        var $el = $(_el);
        var _result = ($el.length) ? $el.index() : 0;
        if ($el.parent('li').length) {
            _result = $el.parent('li').index();
        }
        return _result;
    };


    /**
     * 페이징등, 인덱싱된 앵커를 찾는 함수
     * @param  {[type]} _parent 탐지 대상
     * @param  {[type]} _idx    생략시 전체 반환
     * var $anchor = $.getIndexedAnchor($pager);
     * var $currentAnchor = $.getIndexedAnchor($pager, 0);
     */
    $.getIndexedAnchor = function(_parent, _idx) {

        var $parent = _parent,
            $el = $parent.find('a');

        if ($el.eq(0).parent('li').length) {

            $el = $parent.find('li');

            if (_idx === undefined) {
                $el = $el.find('a');
            } else {
                $el = $el.eq(_idx).find('a');
            }

        } else if (_idx !== undefined) {
            $el = $el.eq(_idx);
        }

        return $el;
    };

    /**
     * 경로를 포함한 파일명에서 경로를 제외한 파일명만 구하기
     * @param  {[type]} _src 추출할 경로
     * @return {[type]}      파일명
     */
    $.getFileName = function(_src) {
        var _result = _src.split('/');
        _result = _result[_result.length - 1];
        return _result;
    };

    /**
     * _off 확장자를 가진 이미지를 찾아 preload 이미지를 생성함
     * @param {[type]} _el 탐지할 부모 객체
     * $.appendPreloadImg();
     */
    $.appendPreloadImg = function(_el) {
        var _arrImgs = [], // 중복방지를 위한 캐싱
            _style = {
                'position': 'absolute',
                'visibility': 'hidden',
                'left': 0,
                'top': '-1000px',
                'width': 0,
                'height': 0,
                'z-index': -100
            };
        _el = (_el === undefined || !_el.length) ? $('body') : _el;

        var _timerAppendImg = setTimeout(function() {

            clearTimeout(_timerAppendImg);

            $('img[src*="_off"]', _el).each(function(i) {

                var _src = $(this).attr('src'),
                    _fileName = $.getFileName(_src);

                if (jQuery.inArray(_fileName, _arrImgs) < 0) { // 중복 방지
                    // if (_arrImgs.indexOf(_fileName) < 0) { // 중복 방지
                    _arrImgs.push(_fileName);
                    $('<img class="preload" aria-hidden="true" alt="Temporary Image" />')
                        .attr('src', _src.replace('_off', '_on'))
                        .css(_style).appendTo($('body'));
                }
            });

        }, 1000);
    };

    /**
     * 배열에서 랜덤 값 추출하기
     * var _color = [1, 2, 3, 4];
     * var _timestamp = '?rand=' + $.getRandInArray(_color);
     */
    $.getRandInArray = function(_arr) {
        var _result = ['blue', 'red', 'green', 'orange', 'pink'];
        if (typeof _arr === 'object') {
            _result = _arr;
        }
        _result = _result[Math.floor(Math.random() * _result.length)];
        return _result;
    };

    /**
     * json 등 배열 객체로 부터 html Dom List 생성
     * @param  {obj, str} _el       생성될 객체의 wrap 객체, $객체 또는 셀렉터
     * $.setListItems('#nav ul', data);
     * $.setListItems('.itemwrap', data, {
     *     filter: function(_item, _idx) {
     *         ...
     *     },
     *     onComplete: function() {
     *         ...
     *     }
     */
    $.setListItems = function(_el, _data, _options) {
        var _html = '',
            $el = (typeof _el == 'string') ? $(_el) : _el;
        var _setting = {
            filter: function(_item, _idx) {
                var _return = '',
                    _nthChild = 'nth-child-' + (_idx + 1);
                _return += '<li class="' + _nthChild + '"><a href="javascript:;"><span>' + _item.name + '</span></a></li>';
                return _return;
            },
            onComplete: function() {}
        };
        var opts = $.extend(_setting, _options);
        if (!$el.length) return false;
        for (var i = 0, len = _data.items.length; i < len; i++) {
            _html += opts.filter.call(null, _data.items[i], i);
        }
        $el.html(_html);
        $.callFunc(this, opts.onComplete);
    };

    /**
     * Perspective Zoom In Out 효과
     * @param  {[type]} _todo   on, off, clear
     */
    $.perspectiveZoom = function(_todo, options) {

        var supportTransforms3d = EPASS.support.transforms3d || true;

        if (supportTransforms3d !== true) return false;

        var setting = {
            $body: $('body'),
            $container: $('#container')
        };

        var opts = $.extend(setting, options);

        var _winHeight = $window.height();

        if (_todo == 'on') {

            opts.$body.css({
                'perspective': '2000px',
                'background-color': '#333'
            });

            opts.$container.css({
                'overflow': 'hidden',
                'height': _winHeight,
                'transition': 'all 0.5s',
                'transform': 'translate3d(0, 0, -120px)'
            });

        } else if (_todo == 'off') {

            opts.$container.css({
                'transform': 'translate3d(0, 0, 0)'
            });

        } else if (_todo == 'clear') {

            opts.$body.removeAttr('style');
            opts.$container.removeAttr('style');

        }

    };

    /**
     * AJAX 인디케이터 설정
     */
    var setAjaxIndicator = (function(el) {
        el.ajaxStart(function() {
            // alert('1');
            // $.showOverlay(false, {
            //     isIndicator: false
            // });
        });
    })($document);

    /**
     * throttle, 스크롤 반응 지연등에 쓰임
     * $.throttle(function() {
     *     console.log('throttled');
     * }, 50);
     */
    $.throttle = (function() {
        var _timerThrottle;
        return function(_fn, _delay) {
            clearTimeout(_timerThrottle);
            _timerThrottle = setTimeout(_fn, _delay);
        };
    })();

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
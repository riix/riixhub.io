$(function() {

    var $html = $('html');

    // pickup
    $('#pick .btn').on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            $img = $this.children(),
            $wrap = $this.parents('.container');
        $html.toggleClass('active-pick');
        if ($html.hasClass('active-pick')) {
            $img.imgToggle();
            $wrap.stop().animate({
                'margin-top': 0
            }, 'fast');
        } else {
            $img.imgToggle('_off');
            $wrap.stop().animate({
                'margin-top': '-408px'
            }, 'fast');
        }
    });

    var _isHover = false;

    $('#gnb').hover(function() {
        _isHover = true;
    }, function() {
        _isHover = false;
    });

    var _timerHover = null;

    var checkGnb = function() {
        _timerHover = setTimeout(function() {
            clearTimeout(_timerHover);
            if (_isHover !== true) {
                $('#gnb li.d1').find('div').stop().fadeOut(50);
                $('#gnb img').imgToggle('_off');
            }
            checkGnb();
        }, 1000);
    };
    checkGnb();

    $('#gnb li.d1').hover(function() {
        var $this = $(this);
        var $d2 = $this.find('div.d2');
        $img = $this.find('img').eq(0);
        if ($d2.length) {
            $img.imgToggle();
            $this.find('div.d2').fadeIn(100);
        }
        $this.siblings().find('div').stop().fadeOut(50);
        $this.siblings().find('img').imgToggle('_off');
    });

    $('#gnb li.d2').hover(function() {
        var $this = $(this),
            $img = $this.find('img').eq(0),
            $d3 = $this.find('div.d3');
        if ($d3.length) {
            $img.imgToggle();
            $this.find('div.d3').fadeIn(100);
        }
        $this.siblings().find('div').stop().fadeOut(50);
    });

    $('#themeShopping a').hover(function() {
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

    var __animation = true;

    $('#jumbotronCore').fxSlider({
        fx: 'fxCrossFade',
        // fx: 'random',
        autoPlay: true,
        prev: '#jumbotron .prev',
        next: '#jumbotron .next',
        pager: '#pager',
        css3animation: __animation,
        duration: 700,
        delay: 4000,
        typho: {
            el: '.typho',
            duration: '.4s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        },
        reverse: true
    });

});
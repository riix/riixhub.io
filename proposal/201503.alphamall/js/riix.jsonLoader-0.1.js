$(function() {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $container = $('#container'),
        $contents = $('#contents'),
        $sqaure = $('#square'),
        $nav = $('#nav');

    var initSlides = function(_url) {
        var jqxhr = $.getJSON(_url, function(data) {
            // console.log('ajax loading');
        }).done(function(data) {
            $.setListItems('#nav ul', data);
            $.setListItems('.itemwrap', data, {
                filter: function(_item, _idx) {
                    var _return = '',
                        _nthChild = 'nth-child-' + (_idx + 1),
                        _random = '?cachebuster=' + Math.ceil(Math.random() * 1000);
                    _return += '<li class="' + _item.itemClass + ' ' + _nthChild + '">';
                    _return += '<div class="pos-relative"><div class="bg"><img class="bg" src="' + _item.background + '"></div>';
                    _return += '<img class="' + _item.picClass + '" src="' + _item.pic + _random + '">';
                    _return += '<div class="core">';
                    _return += '<p class="typho" ><img src="' + _item.typho1 + '"></p>';
                    _return += '<p class="typho"><img src="' + _item.typho2 + '"></p>';
                    _return += '<p class="typho"><img src="' + _item.typho3 + '"></p>';
                    _return += '<p class="typho"><a href="' + _item.href + '"><img src="' + _item.link + '"></a></p>';
                    _return += '</div></div></li>';
                    return _return;
                },
                onComplete: function() {
                    $.initFullscreen();
                }
            });
        });
    };

    initSlides('./json/items.json');

});
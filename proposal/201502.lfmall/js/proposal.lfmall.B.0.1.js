$(function() {

    var supportCSS3 = $('html').hasClass('csstransforms3d');

    // jumbotron
    $.fn.sliderFade = function(options) {

        var _itemWidth = 980,
            _start = (supportCSS3) ? 0 : -(_itemWidth * 1.5),
            _idx = 0;

        var _isHover = false,
            _isAutoPlay = false,
            _timerAutoPlay = null;

        var $el = $(this);

        var $wrap = $el.parent(),
            $slider = $('.itemwrap', $el),
            $slides = $('.item', $slider),
            $pager = $('#jumbotronPager');

        var _setSlide = (function() {
            if (supportCSS3) {
                $slider.css({
                    'transition': 'all 0.4s'
                });
            }
        })();

        var _timerFx = null;

        var _slideTo = function() {

            clearTimeout(_timerFx);

            _idx = $.idxProof(_idx, 8);

            if (supportCSS3) {
                $slides.eq(_idx).currentActive();
            } else {
                $slides.fadeOut(300);
                $slides.eq(_idx).fadeIn(300).currentActive();
            }

            $('li', $pager).eq(_idx).currentActive();

            _timerFx = setTimeout(function() {
                $slides.removeClass('fx');
                $slides.eq(_idx).addClass('fx');
            }, 30);
        };

        var _setHandler = (function() {

            $('li', $pager).click(function() {
                _idx = $(this).index();
                _slideTo();
            });

            $wrap.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
            });

        })();

        var _autoPlay = function() {
            _timerAutoPlay = setTimeout(function() {
                if (_idx >= 4) {
                    _idx = 0;
                }
                if (_isHover !== true && _isAutoPlay === true) {
                    _slideTo();
                    _idx++;
                }
                _autoPlay();
            }, 4000);
        };

        _slideTo(0);
        _autoPlay();

    };

    $('#jumbotron').sliderFade();

    // js toggler
    $('.js-toggler').on('click', function(e) {
        e.preventDefault();

        var $this = $(this),
            $target = $($this.attr('href')),
            $img = ($('#' + $this.data('toggler')).find('img').length) ? $('#' + $this.data('toggler')).find('img') : $this.find('img');

        var _imgToggle = function() {
            if ($img.length && $img.attr('src').match('_off')) {
                $img.imgToggle('_on');
            } else if ($img.length) {
                $img.imgToggle('_off');
            }
        };

        if ($target.is(':hidden')) {
            _imgToggle();
            $target.slideToggle();
        } else {
            $target.slideToggle(_imgToggle);
        }

    });

    // event slider
    $.fn.slider = function(options) {

        var _itemHeight = 682,
            // _start = (supportCSS3) ? 0 : -(_itemHeight * 1.5),
            _start = 0;
        _idx = 0;

        var _isHover = false,
            _isAutoPlay = false,
            _timerAutoPlay = null;

        var $el = $(this);

        var $wrap = $el.parent(),
            $slider = $('.itemwrap', $el),
            $pager = $('#eventSliderPager');

        var _setSlide = (function() {
            if (supportCSS3) {
                $slider.css({
                    'transition': 'all 1s'
                });
            }
        })();

        var _slideTo = function() {

            _idx = $.idxProof(_idx, 5);

            var _to = _start - (_itemHeight * _idx);

            if (supportCSS3) {
                $slider.css({
                    'transform': 'translate3d(0, ' + _to + 'px, 0)'
                });
            } else {
                $slider.stop().animate({
                    'marginTop': _to
                });
            }

            $('.item', $el).eq(_idx + 1).currentActive();

            $('li', $pager).eq(_idx).addClass('active').siblings().removeClass('active');

        };

        var _setHandler = (function() {

            $('li:not(".not")', $pager).hover(function() {
                _idx = $(this).index();
                _slideTo();
            });

            $wrap.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
            });

        })();

        var _autoPlay = function() {
            _timerAutoPlay = setTimeout(function() {
                if (_idx >= 5) {
                    _idx = 0;
                }
                if (_isHover !== true && _isAutoPlay === true) {
                    _slideTo();
                    _idx++;
                }
                _autoPlay();
            }, 4000);
        };

        _slideTo(0);
        _autoPlay();

    };

    $('#eventSlider').slider();

    // news slider 
    var $newsSlider = $('#newsSlider');

    $('.nav a', $newsSlider).on('click', function(e) {
        e.preventDefault();
        var $this = $(this),
            _idx = $this.parent('li').index();
        $newsSlider.removeClass().addClass('grid active-' + (_idx + 1));
        $('.items', $newsSlider).eq(_idx).currentActive();
    }).eq(0).trigger('click');

    var _timerGnb = null;

    // gnb
    var $gnbWrap = $('#gnbWrap'),
        $gnbSub = $('#gnbSub');

    $gnbWrap.hover(function() {
        // clearTimeout(_timerGnb);
        $gnbWrap.addClass('active');
        if (!supportCSS3) {
            $gnbSub.css('opacity', '0').stop().animate({
                'opacity': '1',
                'marginTop': '0'
            }, 400);
        }
    }, function() {
        // _timerGnb = setTimeout(function() {
        $gnbWrap.removeClass('active');
        if (!supportCSS3) {
            $gnbSub.stop().animate({
                'opacity': '0',
                'marginTop': '-560px'
            }, 400);
        }
        // }, 0);
    });

});
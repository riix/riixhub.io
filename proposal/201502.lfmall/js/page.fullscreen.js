$(function() {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $container = $('#container'),
        $contents = $('#contents'),
        $sqaure = $('#square'),
        $nav = $('#nav');

    $.initFullscreen = function() {

        var navIdx = '0';

        $('li', $nav).on('click', function(e) {
            navIdx = $(this).index();
            $('#jumbotron').trigger('goPage');
        });

        $('li', $nav).each(function(i) {
            $(this).css({
                'transition-delay': +((i) / 30) + 's'
            });
        });

        var __animation = true;

        // random
        $('#jumbotron').fxSlider({
            fx: 'fxStickIt',
            autoPlay: true,
            prev: '#jumbotron a.prev',
            next: '#jumbotron a.next',
            pager: '#jumbotronPager',
            css3animation: __animation,
            infinite: false,
            hover: false,
            imagesLoaded: true,
            duration: 1000,
            delay: 4000,
            reverse: false,
            onResize: function(base) {
                var _height = $(window).height();
                $.each([
                    $('.js-fit-height'),
                    base.$el,
                    base.$items
                ], function(i, _el) {
                    _el.height(_height)
                });
                base.$items.find('div.bg').css({
                    'min-height': _height
                });
                base.$items.find('img.bg').css({
                    'width': 'auto',
                    'min-width': '100%',
                    'min-height': _height,
                    'margin-left': -(base.$el.width / 2)
                });
            },
            onAfter: function(base) {
                base.$el.on('goPage', function(e) {
                    var _idx = $.idxProof(navIdx, base.itemsLength);
                    if (_idx == base.afterIdx) return false;
                    base.togglePage(_idx);
                });
            },
            onSlide: function(base) {
                $('li', $nav).eq(base.afterIdx).currentActive();
            },
            typho: {
                el: '.typho',
                duration: '.7s',
                transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
            }
        });

        $('button').on('click', function(e) {
            e.preventDefault();

            var _height = $(window).height();

            $('body').toggleClass('nav-on');

            if ($body.hasClass('nav-on')) {

                $('body').toggleClass('nav-in');

                $contents.animate({
                    'margin-left': '200px'
                }, 300, function() {
                    $sqaure.animate({
                        'left': '0',
                        'top': '0',
                        'width': '200px',
                        'height': _height
                    }, 300, function() {
                        $body.addClass('nav-end');
                        $sqaure.addClass('js-fit-height');
                    })
                });

            } else {

                $body.removeClass('nav-end');

                $contents.animate({
                    'margin-left': '0'
                }, 300, function() {
                    $sqaure.removeClass('js-fit-height').animate({
                        'left': '60',
                        'top': '60',
                        'width': '60',
                        'height': '60'
                    }, 300, function() {
                        $body.removeClass('nav-in');
                    });
                });

            }
        });

    };

});
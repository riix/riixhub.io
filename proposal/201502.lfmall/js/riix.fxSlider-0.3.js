(function($, window, document, ua) {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    /**
     * 심플 CSS3 슬라이더
     * @param  {[type]} options 옵션
     */
    $.fn.fxSlider = function(options) {

        var base = [];

        var _animEndEventNames = EPASS.animEndEventName || 'animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd';

        var setting = {
            animEndEventNames: _animEndEventNames,
            fx: 'fxCrossFade',
            autoPlay: true,
            css3animation: true,
            hover: true,
            imagesLoaded: true,
            infinite: true,
            mouse: true,
            mousewheel: false,
            baseZindex: 1000,
            delay: 3000,
            duration: 300,
            mode: null,
            nav: null,
            next: null,
            pager: null,
            prev: null,
            reverse: false,
            onAfter: function() {},
            onResize: function() {},
            onSlide: function() {},
            typho: {
                el: null,
                duration: '.7s',
                transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
            }
        };

        // set opts
        base.opts = $.extend(setting, options);
        base.opts.css3animation = (EPASS.support.transforms3d !== true) ? false : base.opts.css3animation; // fix animation support

        // global Var
        base.$el = $(this);

        base.$beforeItem = null;
        base.$afterItem = null;
        base.itemsLength = 0,
        base.beforeIdx = 0;
        base.afterIdx = 0;

        var _CLASS_ITEMWRAP = 'itemwrap',
            _CLASS_ITEM = 'item',
            _CLASS_ANIMATING = 'animating',
            _CLASS_ANIMATED = 'animated',
            _CLASS_NO_ANIMATION = 'no-animation';

        var _timerAutoPlay = null,
            _timerDuration = null,
            _timerOnResize = null,
            _throttleResize = 30,
            _isAnimating = false,
            _isHover = false,
            _isPause = false,
            _isFirst = true,
            _elWidth = 0,
            _fxCountAnim = 0,
            _fxClassIn,
            _fxClassOut,
            _dir;

        var _arrFx = [
            'fxCrossFade',
            'fxSoftScale', // Full Width
            'fxPressAway',
            'fxSideSwing',
            'fxFortuneWheel',
            'fxSwipe',
            'fxPushReveal',
            'fxSnapIn',
            'fxLetMeIn',
            'fxStickIt',
            'fxArchiveMe',
            'fxVGrowth',
            'fxSlideBehind',
            'fxSoftPulse',
            'fxEarthquake',
            'fxCliffDiving',
            'fxSlideForward', // Transparent
            'fxTableDrop',
            'fxSlideIt',
            'fxBottleKick',
            'fxShelf',
            'fxCorner', // Small Component
            'fxVScale',
            'fxFall',
            'fxFPulse',
            'fxRPulse',
            'fxHearbeat',
            'fxCoverflow',
            'fxRotateSoftly',
            'fxDeal',
            'fxFerris',
            'fxShinkansen',
            'fxSnake',
            'fxShuffle',
            'fxPhotoBrowse',
            'fxSlideBehind',
            'fxVacuum',
            'fxHurl'
        ];

        var _arrFxTypho = base.opts.typho;

        // preloader
        var $preloader = $('<div class="js-fxSlider-preloader"><img src="./img/module/loading.gif" alt="loading..." /><p>0%</p></div>').css({
            'position': 'absolute',
            'left': '50%',
            'top': '50%',
            'margin': '-23px 0 0 -16px',
            'text-align': 'center'
        });

        /**
         * 콜백 함수가 참인지 판단하고 실행
         * @param  {[type]} _callback 실행할 함수
         * @param  {[type]} _param    파라미터
         * @param  {[type]} _delay    딜레이
         * callFunc(base.opts.onBefore);
         */
        var callFunc = function(_callback, _param, _delay) {
            var _timerCallback = null;
            _callback = (typeof _callback == 'string') ? window[_callback] : _callback;
            if ($.isFunction(_callback)) {
                _param = (_param === null) ? '' : _param;
                _delay = _delay || 50;
                _timerCallback = setTimeout(function() {
                    clearTimeout(_timerCallback);
                    _callback.call(null, base, _param);
                }, 50);
            } else {
                return false;
            }
        };

        /**
         * 타이포 그래피 Fx 효과
         * @param  {[type]} todo 'start' : 'end'
         */
        var _fxTypho = function(todo) {

            var _typhoEl = null;

            if (_arrFxTypho.el === null || base.$afterItem.children().get(0) === undefined) {
                return false;
            } else if (base.$afterItem.children().get(0).tagName.toLowerCase() == 'a') {
                return false;
            }

            if (todo == 'start') {

                _typhoEl = base.$afterItem.find(_arrFxTypho.el);
                _typhoEl.eachQ(function(i) {
                    var $this = $(this),
                        _index = ($this.data('index')) ? $this.data('index') : i,
                        _delay = (_index * 2) / 10 + 's';
                    var _setTyphoCss = function(_transform, _duration, _delay) {
                        $this.css({
                            'opacity': '0',
                            'transform': _arrFxTypho.transform,
                            'transition-duration': _arrFxTypho.duration,
                            'transition-delay': _delay
                        });
                    };
                    _setTyphoCss(_arrFxTypho.transform, _arrFxTypho.duration, _delay);
                });

            } else {

                _typhoEl = base.$afterItem.find(_arrFxTypho.el);
                _typhoEl.css({
                    'transform': 'translate3d(0, 0, 0) scale(1,1) rotate(0deg)'
                });
                _typhoEl.eachQ(function() {
                    var $this = $(this),
                        _opacity = $this.data('opacity') ? $this.data('opacity') : 1;
                    $this.css('opacity', _opacity);
                });
                _typhoEl = null;

            }
        };

        /** 페이징 */
        var _doPaginate = function() {
            var _activePager = function(el, todo) {
                if (todo === true) {
                    el.css('cursor', 'pointer').stop().animate({
                        'opacity': '1'
                    }, 50);
                } else {
                    el.css('cursor', 'default').stop().animate({
                        'opacity': '0.3'
                    }, 50);
                }
                el.show();
            };
            if (base.opts.pager !== undefined) {
                $.getIndexedAnchor(base.$pager, base.afterIdx).currentActive(); // 해당 페이지네이트  활성화
            }
            if (base.opts.infinite === true) {
                base.$prev.fadeIn('fast');
                base.$next.fadeIn('fast');
            } else {
                if (base.afterIdx <= 0) {
                    _activePager(base.$prev, false);
                } else {
                    _activePager(base.$prev, true);
                }
                if (base.afterIdx >= (base.itemsLength - 1)) {
                    _activePager(base.$next, false);
                } else {
                    _activePager(base.$next, true);
                }
            }
        };

        /**
         * 현재 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimCurrentItem = function() {
            _fxCountAnim++;
            _isAnimating = (_fxCountAnim >= 2) ? false : true;
            base.$beforeItem.removeClass(_fxClassOut).removeClass('active').off(base.opts.animEndEventNames);
        };

        /**
         * 다음 슬라이드 애니메이션 후 실행
         */
        var _onEndAnimNextItem = function() {
            _isFirst = false;
            _fxCountAnim++;
            _isAnimating = (_fxCountAnim >= 2) ? false : true;
            base.$afterItem.removeClass(_fxClassIn).addClass('active').off(base.opts.animEndEventNames);
            base.$el.removeClass(_CLASS_ANIMATING).addClass(_CLASS_ANIMATED);
            _fxTypho('end'); // 타이포 그래피 fx 효과
        };

        /** 페이지 이동 */
        base.togglePage = function(_idx) {

            if (_isAnimating === true) return false;

            // set index
            _isAnimating = true;
            _fxCountAnim = 0;
            base.beforeIdx = base.afterIdx;
            base.afterIdx = $.idxProof(_idx, base.itemsLength);

            // on first
            if (_isFirst === true) {
                _fxCountAnim = 1;
                base.beforeIdx = -100;
            }

            // set item and z-index, poiner events, fx 일때는 레벨이 동등해야함
            base.$beforeItem = base.$items.eq(base.beforeIdx);
            base.$afterItem = base.$items.eq(base.afterIdx);

            // paginate
            _doPaginate();

            // on Slide
            callFunc(base.opts.onSlide);

            if (base.opts.css3animation === true) { // css3 지원시

                base.$el.addClass(_CLASS_ANIMATING);

                if (base.beforeIdx >= base.afterIdx) {
                    _fxClassIn = (!base.opts.reverse) ? 'navInPrev' : 'navInNext';
                    _fxClassOut = (!base.opts.reverse) ? 'navOutPrev' : 'navOutNext';
                } else {
                    _fxClassIn = (!base.opts.reverse) ? 'navInNext' : 'navInPrev';
                    _fxClassOut = (!base.opts.reverse) ? 'navOutNext' : 'navOutPrev';
                }

                _fxTypho('start'); // typho effect

                base.$beforeItem.one(base.opts.animEndEventNames, _onEndAnimCurrentItem).addClass(_fxClassOut).css('pointer-events', 'none');
                base.$afterItem.one(base.opts.animEndEventNames, _onEndAnimNextItem).addClass(_fxClassIn).css('pointer-events', 'auto');

            } else { // css3 미지원시

                base.$beforeItem.show();
                base.$afterItem.show();

                var _slideEnd = function() {
                    base.$wrap.css('left', 0);
                    base.$beforeItem.hide();
                    base.$afterItem.currentActive();
                    _isAnimating = false;
                };

                if (_isFirst === true) {
                    _isAnimating = false;
                    _isFirst = false;
                } else {
                    if (base.beforeIdx >= base.afterIdx) {
                        base.$wrap.css('left', -_elWidth).animate({
                            'left': 0
                        }, base.opts.duration, _slideEnd);
                    } else {
                        base.$wrap.animate({
                            'left': -_elWidth
                        }, base.opts.duration, _slideEnd);
                    }
                }

            }

            // init autoplay
            autoPlay();

        };

        /** 자동실행  */
        var autoPlay = function() {
            clearTimeout(_timerAutoPlay);
            if (base.opts.autoPlay === true) {
                _timerAutoPlay = setTimeout(function() {
                    console.log(base.opts.hover);
                    if (_isHover !== true) {
                        navigate('next', true);
                    }
                }, base.opts.delay);
            }
        };

        /**
         * 이전, 다음 페이지 이동
         * @param  {[type]} _dir [description]
         */
        var navigate = function(_dir, _isAuto) {
            if (_isAnimating !== true) {
                var _idx = base.afterIdx;
                if (_dir.match('prev')) {
                    _idx--;
                } else {
                    _idx++;
                }
                if (base.opts.infinite !== true && _isAuto === undefined) {
                    if (_idx == base.itemsLength || _idx < 0) {
                        return false;
                    }
                }
                base.togglePage(_idx);
            }
            return false;
        };

        var _onResize = function() {

            var _height = Math.max(base.$el.height(), base.$items.eq(0).height());

            if (_isFirst === true) {
                base.$el.css('height', _height).delay(50).promise().done(function() {
                    base.$el.show();
                });
            } else {
                base.$el.css('height', _height);
            }

            // set animation class
            if (base.opts.css3animation !== true) {

                _elWidth = base.$el.width();

                base.$wrap.css('width', _elWidth * 2);
                base.$items.css({
                    'opacity': 1,
                    'width': _elWidth
                });

            }

            // on Resize
            callFunc(base.opts.onResize);
        };

        /** 코어  */
        var _core = function() {

            _onResize();

            // set z-index in order
            var _setZindex = (function() {
                $.each([
                    base.$el,
                    base.$prev,
                    base.$next,
                    base.$pager
                ], function(i, _el) {
                    _el.setZindex(i, base.opts.baseZindex);
                });
            })();

            /** 페이징 셋팅 */
            var _setPaginate = (function() {
                if (base.opts.pager && base.itemsLength !== $.getIndexedAnchor(base.$pager).length) {
                    var $el = $.getIndexedAnchor(base.$pager, 0),
                        $pagerInner = $el.parent().empty().show();
                    for (var i = 0; i < base.itemsLength; i++) {
                        $el.clone().appendTo($pagerInner);
                    }
                }
            })();

            // set animation class
            var _setStyle = (function() {
                if (base.opts.css3animation === true) {
                    base.opts.fx = (base.opts.fx == 'random') ? $.getRandInArray(_arrFx) : base.opts.fx;
                    base.$el.addClass(base.opts.fx);
                    base.$items.removeClass('active').css('animation-duration', parseInt(base.opts.duration, 10) + 'ms');
                } else {
                    base.$el.addClass(_CLASS_NO_ANIMATION);
                    base.$wrap.css({
                        'position': 'absolute',
                        'max-width': 'none'
                    });
                    base.$items.css({
                        'position': 'static',
                        'float': 'left'
                    });
                }
            })();

            /** 이벤트 핸들러  */
            var _setHandler = (function() {

                base.$el.hover(function() {
                    _isHover = base.opts.hover; // true : false
                }, function() {
                    _isHover = false;
                    autoPlay();
                });

                if (base.opts.prev !== null) {
                    $document.on('click.slide', base.opts.prev, function(e) {
                        e.preventDefault();
                        navigate('prev');
                    });
                }

                if (base.opts.next !== null) {
                    $document.on('click.slide', base.opts.next, function(e) {
                        e.preventDefault();
                        navigate('next');
                    });
                }

                base.$pager.on('click.slide', 'a', function(e) {
                    e.preventDefault();
                    var _idx = $.getElemIdx($(this));
                    if (_idx == base.afterIdx) return false;
                    base.togglePage(_idx);
                });

                if (EPASS.hasTouch !== true) { // no touchable device

                    if (base.opts.mouse === true) {

                        base.$items.on('drag.slide', function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (e.direction == 1) {
                                navigate('prev');
                            } else if (e.direction == -1) {
                                navigate('next');
                            }
                        }).on('dragstart', function(e) {
                            return false;
                        });

                        base.$items.css({
                            'user-select': 'none',
                            'user-drag': 'none'
                        });
                    }

                    if (base.opts.mousewheel === true) {
                        base.$el.on('mousewheel.slide', function(e) {
                            e.preventDefault();
                            if (e.deltaY > 0) {
                                navigate('prev');
                            } else if (e.deltaY < 0) {
                                navigate('next');
                            }
                        });
                    }

                } else if (EPASS.hasTouch === true) { // touchable device

                    base.$items.on('flick.slide', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if (e.orientation == 'horizontal') {
                            if (e.direction == 1) {
                                navigate('prev');
                            } else if (e.direction == -1) {
                                navigate('next');
                            }
                        }
                    }).trigger('flickr.slide');

                }

                $window.on('resize orientationchange', function() {
                    clearTimeout(_timerOnResize);
                    _timerOnResize = setTimeout(_onResize, _throttleResize);
                });

            })();

            var _onReady = function() {

                $preloader.remove();
                base.$pager.fadeIn();
                base.togglePage(0);

            };

            // imagesLoaded Core
            var _imagesLoadedCore = function(imgLoad) {

                var _loaded = 0;

                $preloader.hide().appendTo(base.$el).fadeIn();

                imgLoad.on('progress', function(instance, image) {
                    _loaded++;
                    $preloader.find('p').text(parseInt(_loaded * 100 / imgLoad.images.length, 10) + '%');
                }).on('done', function() {
                    setTimeout(function() {
                        _onReady();
                    }, 300);
                }).on('fail', function() {
                    _onReady();
                });
            };

            if (base.opts.imagesLoaded && typeof require === "function" && require.defined('imagesLoaded')) { // requireJs
                requirejs([
                    'imagesLoaded'
                ], function(imagesLoaded) {
                    _imagesLoadedCore(imagesLoaded(base.$el));
                });
            } else if (base.opts.imagesLoaded && typeof imagesLoaded == 'function') { // imagesLoaded
                _imagesLoadedCore(imagesLoaded(base.$el));
            } else {
                _onReady();
            }

            // on after
            callFunc(base.opts.onAfter);
        };

        /** 초기화 */
        var _init = (function() {

            // sel elements
            base.$wrap = $('.' + _CLASS_ITEMWRAP, base.$el);
            base.$items = $('.' + _CLASS_ITEM, base.$wrap),
            base.$pager = $(base.opts.pager),
            base.$prev = $(base.opts.prev),
            base.$next = $(base.opts.next);

            // set custom data
            base.$el.data('slider', base);

            // set vars.
            base.itemsLength = base.$items.length;

            // do core
            _core();

        })();

        return this;

    };

})(jQuery, window, document, navigator.userAgent || navigator.vendor || window.opera);
$(function() {

    var idxProof = function(_idx, _max) { // 인덱스값 범위 내 반환
        _idx = (_idx < 0) ? _max - 1 : _idx;
        _idx = (_idx < _max) ? _idx : 0;
        return _idx;
    };

    var currentActive = function(_el, _className, _imgToggle) { // active current item
        var $li = _el.parent('li'),
            $siblings,
            $img;
        _className = _className || 'active';
        _el = ($li.length) ? $li : _el;
        $siblings = _el.siblings();
        $img = $('img', _el).eq(0);
        _el.addClass(_className).siblings().removeClass(_className);
        if ($img.length && _imgToggle !== false) {
            imgToggle($img, '_on');
            imgToggle($siblings.find('img'), '_off');
        }
    };

    var imgToggle = function(_el, _first, _second) { // img toggler
        var _asIs = '_on',
            _toBe = '_off';
        if (_second !== undefined) {
            _asIs = _first;
            _toBe = _second;
        } else {
            if (_first === undefined || _first == '_on') {
                _asIs = '_off';
                _toBe = '_on';
            }
        }
        for (var i = 0, len = _el.length; i < len; i++) {
            var $el = _el.eq(i);
            if ($el[0].tagName.toLowerCase() !== 'img') {
                $el = $el.find('img').eq(0);
            }
            $el.attr('src', $el.attr('src').replace(_asIs, _toBe));
        }
    };

    $.fn.simpleSlidr = function(options) {

        var setting = {
            width: '980px',
            innerEl: '.itemwrap',
            pagerEl: '#jumbotronPager',
            autoPlay: true,
            delay: 4000
        }

        // set opts
        var opts = $.extend(setting, options);

        var isSupport3d = function(_is) {
            var _result = false;
            if (_is === undefined || _is === true) {
                _result = (document.body && document.body.style.perspective !== undefined) ? true : _result;
                var _tempDiv = document.createElement("div"),
                    style = _tempDiv.style,
                    a = ["Webkit", "Moz", "O", "Ms", "ms"],
                    i = a.length;
                _result = (_tempDiv.style.perspective !== undefined) ? true : _result;
                while (--i > -1) {
                    _result = (style[a[i] + "Perspective"] !== undefined) ? true : _result;
                }
            }
            return _result;
        };

        var $el = $(this),
            $wrap = $el.parent(),
            $inner = $(opts.innerEl, $el),
            $pager = $(opts.pagerEl);

        var _isCSS3 = isSupport3d(),
            _itemWidth = parseInt(opts.width, 10),
            _start = (_isCSS3) ? 0 : -(_itemWidth * 1.5),
            _idx = 0,
            _isHover = false,
            _timerAutoPlay = null;

        var _itemsLength = 4;

        var _setSlide = (function() {
            if (_isCSS3) {
                $inner.css({
                    'transition': 'all 0.4s'
                });
            }
        })();

        var _slideTo = function() {

            _idx = idxProof(_idx, _itemsLength);

            var _to = _start - (_itemWidth * _idx);

            if (_isCSS3) {
                $inner.css({
                    'transform': 'translate3d(' + _to + 'px, 0, 0)'
                });
            } else {
                $inner.stop().animate({
                    'marginLeft': _to
                });
            }

            currentActive($('.item', $el).eq(_idx + 1));

        }

        var _setHandler = (function() {

            $('li', $pager).on('mouseover', function() {
                _idx = $(this).index();
                _slideTo();
            });

            $wrap.hover(function() {
                _isHover = true;
            }, function() {
                _isHover = false;
            });

        })();

        var _autoPlay = function() {
            _timerAutoPlay = setTimeout(function() {
                if (_idx >= _itemsLength) {
                    _idx = 0;
                }
                if (_isHover == false && opts.autoPlay == true) {
                    _slideTo();
                    _idx++;
                }
                _autoPlay();
            }, opts.delay);
        }

        _slideTo(0);
        _autoPlay();

    }

    $('#jumbotron').simpleSlidr();

    var __animation = true;

    $('#slideA').fxSlider({
        fx: 'fxFortuneWheel',
        autoPlay: false,
        prev: '#slideA a.prev',
        next: '#slideA a.next',
        // pager: '#jumbotronPagerC',
        infinite: false,
        css3animation: __animation,
        imagesLoaded: true,
        duration: 1000,
        delay: 3500,
        reverse: false,
        typho: {
            el: '.typho',
            duration: '.7s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        },
        onSlide: function(base) {
            currentActive($('#slideAsub ul').eq(base.afterIdx));
        }
    });

});
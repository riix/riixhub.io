$(function() {

    var __animation = true;

    $('#jumbotron').fxSlider({
        fx: 'fxPressAway',
        autoPlay: true,
        pager: '#jumbotronPager',
        infinite: false,
        css3animation: __animation,
        imagesLoaded: true,
        duration: 1000,
        delay: 3500,
        reverse: false
        /* typho: {
            el: '.typho',
            duration: '.7s',
            transform: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        }*/
    });

    // Mega Drop Down Menu
    var $menu = $('#navigation ul.core').eq(0),
        $menuWrapper = $('#navigation').eq(0);
    var _menuHover = false;

    var activateSubmenu = function(row) { // activate
        var $row = $(row),
            $submenu = $('#' + $row.data('submenuId'));
        $submenu.css({
            display: 'block'
        });
        $row.addClass('active');
    };

    var deactivateSubmenu = function(row) { // deactivate
        var $row = $(row),
            $submenu = $('#' + $row.data('submenuId'));
        $submenu.css('display', 'none');
        $row.removeClass('active');
    };

    $menuWrapper.hover(function() { // mouse
        if (_menuHover !== true) {
            _menuHover = true;
            $menu.menuAim({
                activate: activateSubmenu,
                deactivate: deactivateSubmenu
            });
        }
    }, function() {
        _menuHover = false;
        setTimeout(function() {
            if (_menuHover !== true) {
                $menu.find('li.active').eq(0).find('.sub-box').eq(0).fadeOut('fast');
                $menu.find('li').removeClass('active');
            }
        }, 1000);
    });

});
$(function() {

    var $window = $(window),
        $html = $('html');

    // preloaded
    if (typeof $.appendPreloadImg === 'function' && typeof imagesLoaded === 'function') {
        $.appendPreloadImg();
        $('body').imagesLoaded(function() {
            console.log('preloaded.');
            $('img.preload').remove(); // preload 이미지 제거
        });
    };

    EPASS.css3animation = true;
    EPASS.autoPlay = true;

    var fxAlpha = 'fxCliffDiving';

    if ($('body').hasClass('page-b')) fxAlpha = 'fxStickIt';

    // fxSlidr
    $('#jumbotron').fxSlider({
        fx: fxAlpha,
        // fx: 'random',
        // fx: 'fxStickIt',
        // fx: 'fxCrossFade',
        // fx: 'fxCliffDiving',
        // fx: 'fxPushReveal',
        animEndEventNames: EPASS.animEndEventName,
        css3animation: EPASS.css3animation,
        autoPlay: EPASS.autoPlay,
        prev: '#jumbotron a.prev',
        next: '#jumbotron a.next',
        pager: '#jumbotronPager',
        nav: null,
        hover: false,
        infinite: true,
        reverse: true,
        mouse: true,
        mousewheel: false,
        imagesLoaded: true,
        width: '100%',
        height: '434px',
        baseZindex: 1000,
        delay: 3500,
        duration: 1000,
        mode: null,
        onAfter: function() {},
        onResize: function() {},
        onSlide: function() {},
        kenBurns: false,
        typho: {
            el: '.typho',
            duration: '.7s',
            delay: 400,
            transformFrom: 'translate3d(0, 0, 0) scale(1, 1) rotate(0deg)',
            transformTo: 'translate3d(-20px, 0, 0) scale(1, 1) rotate(0deg)'
        }
    });

    // productSlidr
    $('#sliderD').productSlidr({
        infinite: true,
        autoPlay: EPASS.autoPlay,
        visibleLength: 4,
        sliderHeight: '298px'
    });

    // toggleSlidr
    $('#toggleSlidr').toggleSlidr({
        nav: '#toggleSlidrNav',
        css3animation: EPASS.css3animation,
        duration: 100,
        delay: 5200,
        autoPlay: EPASS.autoPlay
    });

    // to the top
    $.fn.toTheTop = function() {
        $(this).on('click', function(e) {
            e.preventDefault();
            $('html, body').stop().animate({
                'scrollTop': '0'
            }, 500);
        });
        return this;
    };

    $('#skyscraperB area[href="#top"]').toTheTop();

    var openpop = function() {
        var _delay = ($window.scrollTop() < 10) ? 0 : 200;
        $('html, body').stop().animate({
            'scrollTop': '0'
        }, _delay, function() {
            $.perspectiveZoom('on');
            $('html').addClass('overlay-open popup-open');
        });
    };

    var closepop = function() {
        $.perspectiveZoom('off');
        $('html').addClass('overlay-close popup-close');
        setTimeout(function() {
            $.perspectiveZoom('clear');
            $('html').removeClass('overlay-open popup-open overlay-close popup-close');
        }, 300);
    };

    $('#skyscraperA area[href="#popup"]').on('click', function(e) {
        e.preventDefault();
        openpop();
    });

    $('#popup, #overlay').on('click', function(e) {
        e.preventDefault();
        closepop();
    });

    $.fn.toggler = function() {
        var $this = $(this),
            $target,
            _fx = 'toggle',
            _speed = 200;
        $this.on('click', function(e) {
            $target = $($this.attr('href'));
            _fx = ($this.data('fx')) ? $this.data('fx') : _fx;
            _speed = ($this.data('speed')) ? $this.data('speed') : _speed;
            if (!$target.length) return false;
            e.preventDefault();
            if (_fx == 'slideToggle') {
                $target.slideToggle(_speed);
            } else {
                $target.toggle();
            }
            $window.trigger('resize');
        });
    };

    $('.js-toggler').toggler();

    // skyscraper
    // $('.skyscraper').skyscraper($('#sectionA')).init();
    $.fn.skyscraper = function(_options) {

        var $window = $(window),
            $this = $(this);

        var _setting = {
            targeEl: null,
            start: 0,
            marginOffset: 0,
            margin: 0
        }
        var opts = $.extend(_setting, _options);

        var core = function() {
            var $target = $(opts.targetEl);
            var _scroll = $window.scrollTop(),
                _start = ($target !== undefined && $target.length) ? $target.offset().top + opts.marginOffset : opts.start,
                _margin = opts.margin,
                _top = $this.css('top'),
                _mode = $this.css('position');
            if (_mode == 'fixed') {
                _top = (_scroll < _start - _margin) ? _start - _scroll : _margin;
            } else if (_mode == 'absolute') {
                _top = (_scroll < _start - _margin) ? _start : _scroll + _margin;
            }
            $this.css('top', _top);
            $this.fadeIn(300);
        }

        var init = function() {
            $window.on('resize scroll', core);
            setTimeout(core, 100);
        }

        return {
            init: function() {
                init();
            },
            core: function() {
                core();
            }
        }
    };

    // skyscraper options
    var _optsSkyscraper = {
        targetEl: '#wrapper',
        start: 0,
        marginOffset: 144,
        margin: 100
    }

    if ($('body').hasClass('main')) {
        _optsSkyscraper = {
            targetEl: '#sectionA',
            start: 0,
            marginOffset: 0,
            margin: 100
        }
    }

    $('.skyscraper').skyscraper(_optsSkyscraper).init();

    $('#topBan').on('click', function(e) {
        e.preventDefault();
        $('.skyscraper').fadeOut(100);
        setTimeout(function() {
            $('.skyscraper').skyscraper(_optsSkyscraper).core();
        }, 400);
    });

});
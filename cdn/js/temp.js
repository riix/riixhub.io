// core do slide
var _coreDoSlide = function(_todo, _idx, _page, $inner, _itemWidth, _itemsLength) {

    if (_isAnimating === true) return false;

    _isAnimating = true;

    var first = 0,
        last = 0;

    // on slide end
    var _onSlideEnd = function(_el) {
        _isAnimating = false;
        slide.$active = slide.$inner.find('.active') || slide.$inner.eq(0);
        doNavigation();
        callFunc(opts.onSlideEnd);
    };

    if (_isInfinite === true) { // infinite mode

        var $items = $inner.children();
        var _prev;
        var _next;
        var posInfinite = -(_slideLength * slide.itemWidth);
        var pageMargin = 0;

        if (_isVertical === true) {

            posInfinite = -_itemHeight;

            // prev
            _prev = function(_count) {
                moveChildItems('prepend', $inner, $items, (_slideLength * _count), _itemsLength);
                $inner.stop(true, true).css('margin-top', -(_itemHeight * _count)).animate({
                    'margin-top': 0
                }, opts.duration, function() {
                    setClassSlide($inner, _visibleLength);
                    _onSlideEnd($inner);
                });
            };

            // next
            _next = function(_count) {
                console.log(_count);
                $inner.stop(true, true).animate({
                    'margin-top': -(_itemHeight * _count)
                }, opts.duration, function() {
                    moveChildItems('append', $inner, $items, (_slideLength * _count), _itemsLength);
                    $inner.stop().css('margin-top', 0);
                    setClassSlide($inner, _visibleLength);
                    _onSlideEnd($inner);
                });
            };
        } else {
            // prev
            _prev = function(_count) {
                moveChildItems('prepend', $inner, $items, (_slideLength * _count), _itemsLength);
                $inner.stop(true, true).css('margin-left', posInfinite * _count).animate({
                    'margin-left': 0
                }, opts.duration, function() {
                    setClassSlide($inner, _visibleLength);
                    _onSlideEnd($inner);
                });
            };

            // next
            _next = function(_count) {
                $inner.stop(true, true).animate({
                    'margin-left': posInfinite * _count
                }, opts.duration, function() {
                    moveChildItems('append', $inner, $items, (_slideLength * _count), _itemsLength);
                    $inner.stop().css('margin-left', 0);
                    setClassSlide($inner, _visibleLength);
                    _onSlideEnd($inner);
                });
            };

        }

        // set page margin
        if (_todo == 'prev') {
            pageMargin = -1;
        } else if (_todo == 'next') {
            pageMargin = 1;
        } else {
            pageMargin = _page - page.current;
        }

        // do
        if (pageMargin < 0) {
            pageMargin = Math.abs(pageMargin);
            page.current = returnLoopPaging(page.current - pageMargin, page.total);
            _prev(pageMargin); // 1...
        } else if (pageMargin > 0) {
            page.current = returnLoopPaging(page.current + pageMargin, page.total);
            _next(pageMargin); // 1...
        }

    } else { // no infinite mode

        var posNoInfinte = 0;
        var firstIdx = 0;
        var beforeIdx = $inner.find('.active').index(); // get before item index
        var _doGo = function(_goto) {

            first = (_goto < 0 || _goto === undefined) ? 0 : parseInt(_goto, 10);
            first = (first >= _itemsLength) ? (_itemsLength) : first;
            first = (first >= _itemsLength - _visibleLength) ? _itemsLength - _visibleLength : first;
            last = first + (_visibleLength - 1);

            posNoInfinte = -(first * _itemWidth);

            if (_isVertical === true) {

                var firstIdx = (Math.ceil((first + _visibleLength) / _visibleLength) - 1);
                posNoInfinte = -(firstIdx * _itemHeight);

                // do animate
                $inner.stop(true, false).animate({
                    'margin-top': posNoInfinte
                }, opts.duration, function() {
                    setClassSlide($inner, _visibleLength, first, last);
                    _onSlideEnd($inner);
                });

            } else { // vertical

                // do animate
                $inner.stop(true, false).animate({
                    'margin-left': posNoInfinte
                }, opts.duration, function() {
                    setClassSlide($inner, _visibleLength, first, last);
                    _onSlideEnd($inner);
                });

            }

        };



        if (_todo == 'prev') {
            _doGo(beforeIdx - _slideLength);
        } else if (_todo == 'next') {
            _doGo(beforeIdx + _slideLength);
        } else {
            _doGo(parseInt(_idx, 10) - 1);
        }

    }
};
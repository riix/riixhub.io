$(function() {

    $.fn.underScor = function(options) {

        var base = [];

        var setting = {
            foo: 'null';
        };

        base.opts = $.extend(setting, options);
        base.$el = $(this);
        base.idx = 0;

        var _CLASS_ACTIVE = 'active';

        var _core = function() {};

        var _init = (function() {
            _core();
        })();

        return this;

    };

    $('#underscore').underScor({
        indicator: '#underscoreIndicator'
    });

});
$(function() {

    'use strict';

    var $document = $document || $(document),
        $window = $window || $(window),
        $html = $html || $('html'),
        $body = $body || $('body');

    var $container = $('#container'),
        $contents = $('#contents'),
        $sqaure = $('#square'),
        $nav = $('#nav');

    /**
     * json 등 배열 객체로 부터 html Dom List 생성
     * @param  {[type]} _el       생성될 객체의 wrap 객체, $객체 또는 셀렉터
     * $.setListItems('#nav ul', data);
     * $.setListItems('.itemwrap', data, {
     *     filter: function(_item, _idx) {
     *         ...
     *     },
     *     onComplete: function() {
     *         ...
     *     }
     */
    $.setListItems = function(_el, _data, _options) {
        var _html = '',
            $el = (typeof _el == 'string') ? $(_el) : _el;
        var _setting = {
            filter: function(_item, _idx) {
                var _return = '',
                    _nthChild = 'nth-child-' + (_idx + 1);
                _return += '<li class="' + _nthChild + '"><a href="javascript:;"><span>' + _item.name + '</span></a></li>';
                return _return;
            },
            onComplete: function() {}
        };
        var opts = $.extend(_setting, _options);
        if (!$el.length) return false;
        for (var i = 0, len = _data.items.length; i < len; i++) {
            _html += opts.filter.call(null, _data.items[i], i);
        }
        $el.html(_html);
        $.callFunc(opts.onComplete);
    };

    var initSlides = function(_url) {
        var jqxhr = $.getJSON(_url, function(data) {
            // console.log('ajax loading');
        }).done(function(data) {
            $.setListItems('#nav ul', data);
            $.setListItems('.itemwrap', data, {
                filter: function(_item, _idx) {
                    var _return = '',
                        _nthChild = 'nth-child-' + (_idx + 1),
                        _random = '?cachebuster=' + Math.ceil(Math.random() * 1000);
                    _return += '<li class="' + _item.itemClass + ' ' + _nthChild + '">';
                    _return += '<div class="pos-relative"><div class="bg"><img class="bg" src="' + _item.background + '"></div>';
                    _return += '<img class="' + _item.picClass + '" src="' + _item.pic + _random + '">';
                    _return += '<div class="core">';
                    _return += '<p class="typho" ><img src="' + _item.typho1 + '"></p>';
                    _return += '<p class="typho"><img src="' + _item.typho2 + '"></p>';
                    _return += '<p class="typho"><img src="' + _item.typho3 + '"></p>';
                    _return += '<p class="typho"><a href="' + _item.href + '"><img src="' + _item.link + '"></a></p>';
                    _return += '</div></div></li>';
                    return _return;
                },
                onComplete: function() {
                    $.initFullscreen();
                }
            });
        });
    };

    initSlides('./json/items.json');

});